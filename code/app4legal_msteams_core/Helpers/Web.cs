﻿using System;
using System.Text;
using System.IO;
using System.Net;
// To use NameValueCollection
using System.Collections.Specialized;
// To use Krystalware.UploadHelper 
using Krystalware.UploadHelper;
// To use MIMEType class and its method
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using app4legal_msteams_core.Controllers;
using NLog;
using System.Threading.Tasks;
using app4legal_msteams_core.Services;
using app4legal_msteams_core.Models;

namespace Microsoft_Teams_Graph_RESTAPIs_Connect.Helpers
{
    public class Web
    {
        private static NLog.Logger logger = LogManager.GetCurrentClassLogger();

        public static string sentRequestToPostFormDataANDMultipleFiles(string urlApp4Legal, string pathAttachmentsFiles, string dataToPOST)
        {
            try
            {
                string[] filesArray = Directory.GetFiles(pathAttachmentsFiles);
                return PostFormDataANDMultipleFilesProcedure(urlApp4Legal, filesArray, dataToPOST);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public static string PostFormDataANDMultipleFilesProcedure(string urlApp4Legal, string[] filesArray, string dataToPOST)
        {
            try
            {
                UploadFile[] files = new UploadFile[filesArray.Length];

                for (int i = 0; i < filesArray.Length; i++)
                {
                    FileInfo fileInfo = new FileInfo(filesArray[i]);
                    files[i] = new UploadFile(fileInfo.FullName,GetMIMETypeClass.GetMimeType(fileInfo.Extension));
                }

                NameValueCollection form = new NameValueCollection();

                string[] ArrayForSeparatePOSTData = dataToPOST.Split('&');

                for (int j = 0; j < ArrayForSeparatePOSTData.Length; j++)
                {
                    string[] ArrayForSeparateKeyANDValueForEachData = ArrayForSeparatePOSTData[j].Split('=');

                    if (ArrayForSeparateKeyANDValueForEachData.Length == 2)
                    {
                        //Replace("\"", string.Empty) in order to replace " character "&createdOn=" + createdBy;
                        form[ArrayForSeparateKeyANDValueForEachData[0].Trim().Replace("\"", string.Empty).Replace("+", string.Empty)] = ArrayForSeparateKeyANDValueForEachData[1].Trim().Replace("\"", string.Empty).Replace("+", string.Empty);
                    }
                }

                return HttpUploadHelper.Upload(urlApp4Legal, files, form, LoginController.credentials?.apiKey);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public static string getdata(string url)
        {
            string valueOriginal = string.Empty;
            using (WebClient webClient = new System.Net.WebClient())
            {
                WebClient n = new WebClient();
                //this is how you authenticate to jira
                System.Net.ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => { return true; };
                n.Headers.Add("Content-Type", "application/json");
                n.Headers.Add("x-api-channel", "microsoft-teams");
                //to get arabic values
                n.Encoding = System.Text.Encoding.UTF8;
                var json = n.DownloadString(url);
                valueOriginal = Convert.ToString(json);
                return valueOriginal;
            }
        }
        public static async Task<String> sendRequestToPostData(string url, string dataToPost)
        {
            string valueToReturnFromRequest = string.Empty;
            try
            {
                byte[] buffer = Encoding.UTF8.GetBytes(dataToPost);
                HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(url);
                WebReq.Method = "POST";
                System.Net.ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => { return true; };
                WebReq.ContentType = "application/x-www-form-urlencoded";
                WebReq.Headers["X-api-key"] = (LoginController.credentials != null)? LoginController.credentials.apiKey : "";
                WebReq.Headers["x-api-channel"] = "microsoft-teams";
                WebReq.ContentLength = buffer.Length;
                Stream PostData = WebReq.GetRequestStream();
                PostData.Write(buffer, 0, buffer.Length);
                PostData.Close();
                HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();
                if (WebResp != null)
                {
                    string responseContent = "";
                    using (StreamReader r = new StreamReader(WebResp.GetResponseStream()))
                    {
                        responseContent = r.ReadToEnd();
                    }
                    logger.Info("The server at ResponseUri :" + WebResp.ResponseUri);
                    logger.Info("returned StatusCode:" + WebResp.StatusCode);
                    logger.Info("With headers: ");
                    foreach (string key in WebResp.Headers.AllKeys)
                    {
                        logger.Info(key + ": " + WebResp.Headers[key]);
                    }
                    logger.Info("responseContent: " + responseContent);
                    valueToReturnFromRequest = responseContent;
                    bool isExpiredToken = isRefreshTokenExpired(valueToReturnFromRequest);
                    if (isExpiredToken)
                    {
                        bool retrieveNewRefreshToken = await refreshToken();
                        if (retrieveNewRefreshToken)
                        {
                            return sendRequestToPostData(url, dataToPost).Result;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("web.cs - sendRequestToPostData: ERROR Message: " + ex.Message.ToString());
                logger.Error("web.cs - sendRequestToPostData - StackTrace" + ex.StackTrace.ToString());
                valueToReturnFromRequest = ex.Message;
            }
            logger.Info("web.cs - sendRequestToPostData: END");
            return valueToReturnFromRequest;
        }
        public static async Task<String> sendRequestToGETtData(string url)
        {
            string valueToReturnFromRequest = string.Empty;
            try
            {
                WebRequest WebReq = WebRequest.Create(url);
                System.Net.ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => { return true; };
                WebReq.ContentType = "application/x-www-form-urlencoded";
                WebReq.Headers["X-api-key"] = (LoginController.credentials != null) ? LoginController.credentials.apiKey : "";
                WebReq.Headers["x-api-channel"] = "microsoft-teams";
                Stream PostData = WebReq.GetResponse().GetResponseStream();
                StreamReader _Answer = new StreamReader(PostData);
                valueToReturnFromRequest = _Answer.ReadToEnd().ToString();
                bool isExpiredToken = isRefreshTokenExpired(valueToReturnFromRequest);
                if (isExpiredToken)
                {
                    bool retrieveNewRefreshToken = await refreshToken();
                    if (retrieveNewRefreshToken)
                    {
                        return sendRequestToGETtData(url).Result;
                    }
                }
            }
            catch (Exception ex)
            {
                valueToReturnFromRequest = ex.Message;
            }
            return valueToReturnFromRequest;
        }
        public static String sendGenericRequestToGETtData(string url, string accessToken = "")
        {
            string valueToReturnFromRequest = string.Empty;
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                if(accessToken.Length > 0)
                    req.Headers.Add("Authorization", "Bearer " + accessToken);
                ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => { return true; };
                Stream PostData = req.GetResponse().GetResponseStream();
                StreamReader _Answer = new StreamReader(PostData);
                valueToReturnFromRequest = _Answer.ReadToEnd().ToString();
                
            }
            catch (Exception ex)
            {
                logger.Error("Web - sendGenericRequestToGETtData - Message: " + ex.Message);
                valueToReturnFromRequest = "Forbidden";
            }
            return valueToReturnFromRequest;
        }
        public static String sendGenericRequestToPostData(string url, string dataToPost, string accessToken = "")
        {
            logger.Info("web.cs - sendRequestToPostData: Start");
            string valueToReturnFromRequest = string.Empty;
            logger.Info("Web.cs - sendRequestToPostData - URL : " + url);
            try
            {
                byte[] buffer = Encoding.UTF8.GetBytes(dataToPost);
                HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(url);
                if (accessToken.Length > 0)
                    WebReq.Headers.Add("Authorization", "Bearer " + accessToken);
                WebReq.Method = "POST";
                WebReq.ContentType = "application/json";
                ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => { return true; };
                WebReq.ContentLength = buffer.Length;
                Stream PostData = WebReq.GetRequestStream();
                PostData.Write(buffer, 0, buffer.Length);
                PostData.Close();
                HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();
                if (WebResp != null)
                {
                    string responseContent = "";
                    using (StreamReader r = new StreamReader(WebResp.GetResponseStream()))
                    {
                        responseContent = r.ReadToEnd();
                    }
                    logger.Info("The server at ResponseUri :" + WebResp.ResponseUri);
                    logger.Info("returned StatusCode:" + WebResp.StatusCode);
                    logger.Info("With headers: ");
                    foreach (string key in WebResp.Headers.AllKeys)
                    {
                        logger.Info(key + ": " + WebResp.Headers[key]);
                    }
                    logger.Info("responseContent: " + responseContent);
                    valueToReturnFromRequest = responseContent;                   
                }
            }
            catch (Exception ex)
            {
                logger.Error("web.cs - sendRequestToPostData: ERROR Message: " + ex.Message.ToString());
                logger.Error("web.cs - sendRequestToPostData - StackTrace" + ex.StackTrace.ToString());
                valueToReturnFromRequest = ex.Message;
            }
            logger.Info("web.cs - sendRequestToPostData: END");
            return valueToReturnFromRequest;
        }
        public static string sendRequestToGetDataWithData(string url, Dictionary<object,object> data)
        {
            string valueToReturnFromRequest = string.Empty;
            try
            {
                WebClient webClient = new WebClient();
                foreach(KeyValuePair<object,object> pair in data)
                {
                    webClient.QueryString.Add(pair.Key.ToString(), pair.Value.ToString());
                }
                webClient.Headers["x-api-key"] = (LoginController.credentials != null) ? LoginController.credentials.apiKey : "";
                webClient.Headers["x-api-channel"] = "microsoft-teams";
                string resp = webClient.DownloadString(url);
                bool isExpiredToken = isRefreshTokenExpired(valueToReturnFromRequest);
                //if (isExpiredToken)
                //{
                //    bool retrieveNewRefreshToken = refreshToken();
                //    if (retrieveNewRefreshToken)
                //    {
                //        return sendRequestToGETtData(url);
                //    }
                //}
            }
            catch (Exception ex)
            {
                valueToReturnFromRequest = ex.Message;
            }
            return valueToReturnFromRequest;
        }
        public static string serializeJson(string json)
        {
            var dyn = JsonConvert.DeserializeObject<dynamic>(json);
            var p = dyn.success;
            var jObject = (JObject)p;
            return jObject.ToString();


        }
        public static string deserializeJson(string json)
        {
            var dyn = JsonConvert.DeserializeObject<dynamic>(json);
            var p = dyn.error;
            var jObject = (JObject)p;
            return jObject.ToString();
        }
        public static bool isValidConnection(string url, string user, string password)
        {
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(url);
                request.Method = WebRequestMethods.Ftp.ListDirectory;
                request.Credentials = new NetworkCredential(user, password);
                request.GetResponse();
            }
            catch (WebException ex)
            {
                return false;
            }
            return true;
        }
        public static Boolean isRefreshTokenExpired(string response)
        {
            Boolean isExpired = false;
            try
            {
                dynamic details = JObject.Parse(response);
                dynamic error = details["error"];
                if (error != null)
                {
                    string errorCode = error["code"];
                    string errorMsg = error["msg"];
                    if (errorCode == "refresh_key_required")
                    {
                        isExpired = true;
                    }
                    logger.Info(Convert.ToString(error));
                }
                logger.Info("Web cs - checkErrorMessage - END");
            }
            catch (Exception ex)
            {
                logger.Error("Web cs - checkErrorMessage - Error:" + ex.Message);
            }
            return isExpired;
        }
        static async Task<bool> refreshToken()
        {
            bool retrieveRefreshToken = false;
            string valueToReturnFromRequest = string.Empty;
            logger.Info("Web cs - refreshToken - START");
            try
            {
                string Url = LoginController.credentials?.userUrl + @"modules/api/users/refresh_api_key";
                HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(Url);
                WebReq.Method = "POST";
                ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => { return true; };
                WebReq.ContentType = "application/x-www-form-urlencoded";
                WebReq.Headers["X-api-key"] = LoginController.credentials?.apiKey;
                WebReq.Headers["x-api-channel"] = "microsoft-teams";
                HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();
                if (WebResp != null)
                {
                    string responseContent = "";
                    using (StreamReader r = new StreamReader(WebResp.GetResponseStream()))
                    {
                        responseContent = r.ReadToEnd();
                    }
                    logger.Info("responseContent: " + responseContent);
                    valueToReturnFromRequest = responseContent;
                    dynamic details = JObject.Parse(valueToReturnFromRequest);
                    string error = details["error"];
                    if (error == "")
                    {
                        dynamic success = details["success"]["data"];
                        string newUserKey = success["key"];
                        LoginController.credentials.apiKey = newUserKey;
                        var graphClient = GraphServiceClientFactory.GetStaticAuthenticatedClient();
                        await GraphService.saveUserExtension(graphClient, newUserKey);
                        retrieveRefreshToken = true;
                    }
                    else
                    {
                        logger.Error("web.cs - refreshToken: ERROR API -responseContent : " + responseContent);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("web.cs - refreshToken: ERROR Message: " + ex.Message.ToString());
                logger.Error("web.cs - refreshToken - StackTrace" + ex.StackTrace.ToString());
                valueToReturnFromRequest = ex.Message;
            }
            logger.Info("web.cs - refreshToken: END");
            return retrieveRefreshToken;
        }

    }
}