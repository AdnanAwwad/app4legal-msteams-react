﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace app4legal_msteams_core.Helpers
{
        namespace Classes
        {
            public class errors
            {
                public string error { get; set; }
            }
            public class IdName
            {
                public string id { get; set; }
                public string name { get; set; }
                public string firstName { get; set; }
                public string lastName { get; set; }
            }
            public class Versioning
            {
                public string status { get; set; }
                public string version { get; set; }
                public string error { get; set; }
            }
            public class jdata
            {
                public string error { get; set; }
                public success success { get; set; }

            }
            public class IPdata
            {
                public string error { get; set; }
                public IPsuccess success { get; set; }

            }
            public class IPsuccess
            {
                public string msg { get; set; }
                public IPInData data { get; set; }
            }
            public class success
            {
                public string msg { get; set; }
                public IPCase data { get; set; }
            }
            public class data
            {
                public List<IdName> case_statuses { get; set; }
                public DefaultValues default_values { get; set; }

            }
            public class DefaultValues
            {
                public string case_status_id { get; set; }
                public string provider_group_id { get; set; }
                public dynamic provider_group_users { get; set; }
                public string assignedTo { get; set; }
                public string caseValueCurrency { get; set; }
                public string case_type_id { get; set; }

            }
            public class DefaultValuesNewtask
            {
                public string task_status_id { get; set; }
                public string task_type_id { get; set; }
                //public dynamic provider_group_users { get; set; }
                //public string assignedTo { get; set; }
                //public string caseValueCurrency { get; set; }
                //public string case_type_id { get; set; }

            }
            public class IPCase
            {
                public ClientTypesClass clientTypes { get; set; }
                public List<IdName> IPR { get; set; }
                public List<IdName> categories { get; set; }
                public List<IdName> subcategories { get; set; }
                public List<IdName> usersProviderGroup { get; set; }
                public List<IdName> Provider_Groups { get; set; }
                public RegistrationStatusClass registrationStatus { get; set; }
            }

            public class IPInData
            {
                public ClientTypesClass clientTypes { get; set; }
                // public ClientTypesClass agentTypes { get; set; }
                public List<IdName> IPR { get; set; }
                public List<IdName> ip_classes { get; set; }
                public List<IdName> ip_subcategories { get; set; }
                public List<IdName> Provider_Groups { get; set; }
                public IPDefaultValues default_values { get; set; }
                public List<IdName> ip_statuses { get; set; }
                public List<IdName> ip_names { get; set; }
                public List<IdName> usersProviderGroup { get; set; }
                public Dictionary<string, string> countries { get; set; }
                public RegistrationStatusClass registrationStatus { get; set; }
            }
            public class IPDefaultValues
            {
                public string provider_group_id { get; set; }

            }
            public class successLitCase
            {
                public string msg { get; set; }
                public Case data { get; set; }

            }
            public class Case

            {
                public string status { get; set; }
                public List<IdName> case_statuses { get; set; }
                public List<IdNameSLA> case_types { get; set; }
                public List<IdName> provider_groups { get; set; }
                public List<IdName> caseStages { get; set; }
                public PrioritiesClass priorities { get; set; }
                public ClientTypesClass clientTypes { get; set; }
                public List<IdName> clientPositions { get; set; }
                public DefaultValues default_values { get; set; }
                public Assignments assignments { get; set; }

            }
            public class Assignments
            {
                public string category { get; set; }
                public string type { get; set; }
                public string assigned_team { get; set; }
                public string visible_assignee { get; set; }
                public string visible_assigned_team { get; set; }
                public string assignment_relation { get; set; }
                public string assignment_id { get; set; }
                public string user_relation { get; set; }
            }
            public class IdNameSLA
            {
                public string id { get; set; }
                public string name { get; set; }
                public string litigationSLA { get; set; }
            }
            public class PrioritiesClass
            {
                public string critical { get; set; }
                public string high { get; set; }
                public string medium { get; set; }
                public string low { get; set; }
            }
            public class ClientTypesClass
            {
                public string company { get; set; }
                public string contact { get; set; }
            }
            public class IdNameType
            {
                public string id { get; set; }
                public string name { get; set; }
                public string type { get; set; }
            }
            public class getClients
            {
                public List<IdName> data { get; set; }
                public string msg { get; set; }
            }
            public class RegistrationStatusClass
            {
                public string Active { get; set; }
                public string Inactive { get; set; }
            }
            public class getProviderGroups
            {
                public List<ResultsClass> data { get; set; }
                public string msg { get; set; }
            }
            public class ResultsClass
            {
                public string id { get; set; }
                public string user_group_id { get; set; }
                public string email { get; set; }
                public string status { get; set; }
                public string gender { get; set; }
                public string title { get; set; }
                public string firstName { get; set; }
                public string lastName { get; set; }
                public string jobTitle { get; set; }
                public string isLawyer { get; set; }
            }
            public class addedCase
            {
                public caseAdd data { get; set; }
                public string msg { get; set; }
            }
            public class addedIP
            {
                public IPAdd data { get; set; }
                public string msg { get; set; }
            }
            public class IPAdd
            {
                public string id { get; set; }
            }
            public class errorsSave
            {
                public object error { get; set; }
            }
            public class caseAdd
            {
                public string case_id { get; set; }
            }
            public class Cases
            {
                public List<UrlName> data { get; set; }

            }
            public class UrlName
            {
                public string url { get; set; }
                public string name { get; set; }
                public string type { get; set; }
                public string id { get; set; }
                public string subject { get; set; }
            }
            public class VA
            {
                public string val { get; set; }
                public string attach { get; set; }
            }
            public class GetUrl
            {
                public string data { get; set; }

            }
            public class ProductVersion
            {
                public VerData data;
            }
            public class VerData
            {
                public string productVersion { get; set; }
                public string apiVersion { get; set; }
            }
            public class defaultsCheck
            {
                public datacheck data { get; set; }
            }
            public class CaseLookup
            {
                public string id { get; set; }
                public string name { get; set; }
                public string comingFromCP { get; set; }

            }
            public class datacheck
            {
                public groupUserscheck default_values { get; set; }
            }
            public class groupUserscheck
            {
                public List<ResultsClass> provider_group_users { get; set; }
            }
            public class getCompanies
            {
                public List<Company> data { get; set; }
                public string msg { get; set; }
            }
            public class Company
            {
                public string id { get; set; }
                public string name { get; set; }
                public string shortName { get; set; }
            }
            public class jdataCase
            {
                public string error { get; set; }
                public successCase success { get; set; }

            }
            public class successCase
            {

                public string msg { get; set; }
                public Case data { get; set; }
            }
            public class getAssignee
            {
                public string msg { get; set; }
                public List<ResultsClass> data { get; set; }
            }

            //add case success

            //Read Data after submit


            // Definition of class for use in deserialize<T>(String) (received JSONData data from App4Legal)
            public class AssignToClass2
            {
                public string status { get; set; }
                public List<IdName> results { get; set; }
            }
            public class ClientNameAutoComplitClass
            {
                public string status { get; set; }
                public List<IdName> results { get; set; }
            }
            public class refferedBy
            {
                public List<IdName> data { get; set; }
                public string msg { get; set; }
            }
            public class IdNamelst
            {
                public string id { get; set; }
                public string name { get; set; }
            }
            public class errorSave
            {
                public object error { get; set; }

            }
            public class AssignToClass1
            {
                public string status { get; set; }
                public List<ResultsClass> results { get; set; }
            }
            public class ValidatioErrosFields
            {
                public string description { get; set; }
                public string internalReference { get; set; }
                public string estimatedEffort { get; set; }
            }
            public class CaseAdd
            {
                public string status { get; set; }
                public string case_id { get; set; }
                public ValidatioErrosFields validationErrors { get; set; }
            }

            public class SUCCESS
            {
                public string msg { get; set; }
                public DATA data { get; set; }
                public Dictionary<string, string> categories { get; set; }

            }
            public class CompanySuccess
            {
                public string msg { get; set; }
                public CompanyData data { get; set; }

            }
            public class DATA
            {
                public Dictionary<string, string> isLawyerValues { get; set; }
                public Dictionary<string, string> statusesValues { get; set; }
                public FORMDATA formData { get; set; }
                public string contact_id { get; set; }
                public List<money_accounts> money_accounts { get; set; }
                public bool flag_to_change_category { get; set; }
            }
            public class CompanyData
            {
                public Dictionary<string, string> Companies { get; set; }
                public Dictionary<string, string> Company_Legal_Types { get; set; }
                public Dictionary<string, string> Countries { get; set; }
                public Dictionary<string, string> Currencies { get; set; }
                public Dictionary<string, string> statuses { get; set; }
                public CompanyDefaultValues default_values { get; set; }
                //public Dictionary<string, string> companyUsers { get; set; }
                //public Dictionary<string, string> companyWatchersUsersStatus { get; set; }
                public Dictionary<string, string> categories { get; set; }
                public Dictionary<string, string> company_categories { get; set; }
                public Dictionary<string, string> company_sub_categories { get; set; }

            }
            public class CompanyResponseSuccess
            {
                public string msg { get; set; }
                public ResponseData data { get; set; }
                public Dictionary<string, string> categories { get; set; }

            }
            public class ResponseData
            {
                public string company_id { get; set; }
                public List<money_accounts> money_accounts { get; set; }
                public bool flag_to_change_category { get; set; }
            }
            public class money_accounts
            {
                public string editable { get; set; }
                public string organization_name { get; set; }
                public string organization_id { get; set; }
                public string currency { get; set; }
                public string currency_id { get; set; }
                public string model { get; set; }
                public string model_id { get; set; }
                public string account_name { get; set; }
            }
            public class CompanyDefaultValues
            {
                public string company_legal_type_id { get; set; }
                public string status { get; set; }
                public string category { get; set; }
                public string capitalVisualizeDecimals { get; set; }


            }
            public class FORMDATA
            {
                public string id { get; set; }
                public Dictionary<string, string> status { get; set; }
                public Dictionary<string, string> genders { get; set; }
                public Dictionary<string, string> titles { get; set; }
                public Dictionary<string, string> categories { get; set; }
                public Dictionary<string, string> sub_categories { get; set; }

                public Dictionary<string, string> countries { get; set; }

            }
            public class ContactData
            {
                public string Email1Address { get; set; }
                public string FatherName { get; set; }
                public string FirstName { get; set; }
                public string LastName { get; set; }
                public string Birthday { get; set; }
                public string MobileTelephoneNumber { get; set; }
                public string BusinessTelephoneNumber { get; set; }
                public string fax { get; set; }
                public string WebPage { get; set; }
                public string JobTitle { get; set; }
                public string FullName { get; set; }
                public string street { get; set; }
                public string zip { get; set; }
                public string country { get; set; }
                public string city { get; set; }
                public string notes { get; set; }
                public string state { get; set; }
            }
            public class Hearing
            {
                public List<IdName> clientPositions { get; set; }
                public Dictionary<string, string> courtTypes { get; set; }
                public Dictionary<string, string> degreesList { get; set; }
                public Dictionary<string, string> regionsList { get; set; }
                public Dictionary<string, string> courtsList { get; set; }

            }

            public class SpecialHearing
            {
                public List<IdName> clientPositions { get; set; }
                public List<IdName> courtTypes { get; set; }
                public Dictionary<string, string> degreesList { get; set; }
                public Dictionary<string, string> regionsList { get; set; }
                public Dictionary<string, string> courtsList { get; set; }

            }
            public class loadTask
            {
                public string msg { get; set; }
                public Hearing data { get; set; }

            }
            public class SpecialloadHearing
            {
                public string msg { get; set; }
                public SpecialHearing data { get; set; }

            }

            public class loadNewTask
            {
                public string msg { get; set; }
                public Task data { get; set; }
            }
            public class Task
            {
                public string status { get; set; }
                public List<IdName> taskTypes { get; set; }
                public List<IdName> taskStatuses { get; set; }
                public string toMeId { get; set; }
                public string toMeFullName { get; set; }
                public PrioritiesClass taskPriorities { get; set; }
                public DefaultValuesNewtask default_values { get; set; }
                public string userLoggedInFullName { get; set; }
            }
            public class loadCaseData
            {
                public string msg { get; set; }
                public HearingCase data { get; set; }
            }
            public class HearingCase
            {
                public string hearingCourtId { get; set; }
                public string hearingCourtTypeId { get; set; }
                public string hearingCourtDegreeId { get; set; }
                public string hearingCourtRegionId { get; set; }
                public string hearingClientPositionId { get; set; }
                public Dictionary<string, string> hearingLawyers { get; set; }
                public dynamic hearingOpponents { get; set; }
                public Dictionary<string, string> hearingJudges { get; set; }
                public Dictionary<string, string> hearingOpponentLawyers { get; set; }
                public Dictionary<string, string> hearingExternalLawyers { get; set; }
                public Dictionary<string, string> hearingClients { get; set; }
                public Dictionary<string, string> courtDegrees { get; set; }
                public Dictionary<string, string> courtRegions { get; set; }
                public Dictionary<string, string> courts { get; set; }
                public dynamic stage { get; set; }
                public bool showStageEditLink { get; set; }
                //public Dictionary<string, string> case_reference { get; set; }
                public dynamic case_reference { get; set; }
            }

            public class loadCaseDataCheck
            {
                public CaseCheck data { get; set; }
            }
            public class CaseCheck
            {
                public Dictionary<string, string> hearingOpponents { get; set; }
            }
            public class LoadMeeting
            {
                public string msg { get; set; }
                public MeetingData data { get; set; }
            }
            public class loadHeadring
            {
                public string msg { get; set; }
                public HearingNew data { get; set; }
            }
            public class HearingNew
            {
                public Dictionary<string, string> types { get; set; }
                public Dictionary<string, string> stage_statuses { get; set; }

                public Boolean hijri_calendar_enabled { get; set; }
            }
            public class StageData
            {
                public Stage data { get; set; }
            }
            public class Stage
            {
                public string case_id { get; set; }
                public string id { get; set; }
                public string legal_case_stage { get; set; }
                public string court_type { get; set; }
                public string court_degree { get; set; }
                public string court_region { get; set; }
                public string court { get; set; }
                public string status { get; set; }
                public string client_position { get; set; }
                public string client_name { get; set; }
                public string stage { get; set; }
                public string ext_references { get; set; }
                public string sentenceDate { get; set; }
                public string opponents { get; set; }
                public string constitutionDate { get; set; }
                public string modifiedBy { get; set; }
                public string modifiedOn { get; set; }
                public string modifiedByName { get; set; }
            }
            //public class Stage
            //{
            //    public string stage_id { get; set; }
            //    public string legal_case_stage { get; set; }
            //    public string court_type { get; set; }
            //    public string court_degree { get; set; }
            //    public string court_region { get; set; }
            //    public string court { get; set; }
            //    public string status { get; set; }
            //    public string client_position { get; set; }
            //    public string client_name { get; set; }
            //    public string legal_case_stage_name { get; set; }
            //    public string ext_references { get; set; }
            //    public string opponents { get; set; }
            //}

            public class MeetingData
            {
                public MeetingFormData formData { get; set; }
            }
            public class MeetingFormData
            {
                public Dictionary<string, string> priorities { get; set; }
                public IdName[] meeting_types { get; set; }

            }
       
        }
    }