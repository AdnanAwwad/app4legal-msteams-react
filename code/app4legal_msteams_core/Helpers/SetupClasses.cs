﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace app4legal_msteams_core.Helpers
{
    namespace SetupClasses
    {
        public class Success
        {
            public data data { get; set; }
            public string msg { get; set; }
        }
        public class data
        {
            public string key { get; set; }
            public string userId { get; set; }
            public ProfilePicture profilePicture { get; set; }
        }
        public class ProfilePicture
        {
            public string type { get; set; }
            public string content { get; set; }
        }
        public class errors
        {
            public string error { get; set; }
        }
        public class CheckAPISuccess
        {
            public CheckAPIdata data { get; set; }
            public string msg { get; set; }
        }
        public class CheckAPIdata
        {
            public string user_id { get; set; }
            public string username { get; set; }
            public string profileName { get; set; }
        }
    }
}