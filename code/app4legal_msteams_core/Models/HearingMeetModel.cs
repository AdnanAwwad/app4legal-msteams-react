﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace app4legal_msteams_core.Models
{
    public class HearingMeetModel
    {
        public string HearingId { get; set; }
        public string[] Members { get; set; }
        public string CaseId { get; set; }
        public string CaseCategory { get; set; }
        public string CaseName { get; set; }
        public string CaseURL { get; set; }
        public string ChannelName { get; set; }
    }
}