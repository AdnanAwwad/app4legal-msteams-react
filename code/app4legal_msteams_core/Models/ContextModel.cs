﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace app4legal_msteams_core.Models
{
    public class ContextModel
    {
        public string controllerName { get; set; }
        public string actionName { get; set; }
    }
}