﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace app4legal_msteams_core.Models
{
    public class ContractMeetModel
    {
        public string ContractId { get; set; }
        public string CaseName { get; set; }
        public String[] Members { get; set; }
        public string ChannelName { get; set; }
        public string CaseCategory { get; set; }
        public string CaseURL { get; set; }

    }
}