﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace app4legal_msteams_core.Models
{
    public class TeamContextModel
    {
        public string teamId { get; set; }
        public string caseId { get; set; }
        public string teamName { get; set; }
        public string channelId { get; set; }
        public string channelName { get; set; }
        public string noteTitle { get; set; }
        public List<string> filesID { get; set; }
        public bool isFullConversation { get; set; }
        public bool isContract { get; set; }
    }
}