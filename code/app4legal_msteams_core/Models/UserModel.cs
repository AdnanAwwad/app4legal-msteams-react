﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using app4legal_msteams_core.Models;

namespace app4legal_msteams_core.Models
{
    public class UserModel
    {
        public string displayName { get; set; }
        public CredentialsModel credentials { get; set; }
        public string organizationName { get; set; }
        public string imagePath { get; set; }
    }
}