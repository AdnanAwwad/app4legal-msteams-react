﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace app4legal_msteams_core.Models
{
    public class LoginModel
    {
            public string cloudOrServer { get; set; }
            public string url { get; set; }
            public string userName { get; set; }
            public string password { get; set; }
            public string instanceId { get; set; }

    }
    public class CredentialsModel
    {
        public string apiKey { get; set; }
        public string userUrl { get; set; }
    }
}