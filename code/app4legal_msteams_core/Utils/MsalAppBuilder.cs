﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Identity.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.Graph;
using app4legal_msteams_core;

namespace Microsoft_Teams_Graph_RESTAPIs_Connect
{
    public static class MsalAppBuilder
    {
        public static IConfidentialClientApplication publicClient;
        public static IConfidentialClientApplication publicApplication;
        public static IConfidentialClientApplication BuildConfidentialClientApplication()
        {
            IConfidentialClientApplication clientapp = ConfidentialClientApplicationBuilder.Create(Globals.ClientId)
                  .WithClientSecret(Globals.ClientSecret)
                  .WithRedirectUri(Globals.RedirectUri)
                  .WithAuthority(new Uri(Globals.Authority))
                  .Build();
            publicClient = clientapp;

            return clientapp;
        }
        public static IConfidentialClientApplication BuildConfidentialClientApplicationForAppLevel(string tenantId)
        {
            string authority = "https://login.microsoftonline.com/" + tenantId;
            IConfidentialClientApplication clientapp = ConfidentialClientApplicationBuilder.Create(Globals.ClientId)
                    .WithClientSecret(Globals.ClientSecret)
                    .WithAuthority(new Uri(authority))
                    .Build();
            publicApplication = clientapp;
            return clientapp;
        }
        public static ITokenCache GetUserTokenCache()
        {
            IConfidentialClientApplication clientapp = ConfidentialClientApplicationBuilder.Create(Globals.ClientId)
                  .WithClientSecret(Globals.ClientSecret)
                  .WithRedirectUri(Globals.RedirectUri)
                  .WithAuthority(new Uri(Globals.Authority))
                  .Build();

            // We only clear the user's tokens.
            // MSALPerUserMemoryTokenCache userTokenCache = new MSALPerUserMemoryTokenCache(clientapp.UserTokenCache);
            return clientapp.UserTokenCache;
        }


    }
}