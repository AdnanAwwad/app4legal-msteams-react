﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using app4legal_msteams_core.Helpers;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using NLog;
using app4legal_msteams_core.Helpers.SetupClasses;
using static System.Net.Mime.MediaTypeNames;
using System.Drawing;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace app4legal_msteams_core
{
    public class Globals
    {
        private static NLog.Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        public Globals(IWebHostEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _configuration = configuration;
        }
        public const string ConsumerTenantId = "9188040d-6c67-4c5b-b112-36a304b66dad";
        public const string IssuerClaim = "iss";
        public const string Authority = "https://login.microsoftonline.com/common/v2.0/";
        public const string TenantIdClaimType = "http://schemas.microsoft.com/identity/claims/tenantid";
        public const string MicrosoftGraphGroupsApi = "https://graph.microsoft.com/v1.0/groups";
        public const string MicrosoftGraphUsersApi = "https://graph.microsoft.com/v1.0/users";
        public const string AdminConsentFormat = "https://login.microsoftonline.com/{0}/adminconsent?client_id={1}&state={2}&redirect_uri={3}";
        public const string BasicSignInScopes = "openid profile email offline_access user.readbasic.all";
        public const string NameClaimType = "name";
        public const string ExtensionName = "com.app4legal.roamingSettings";
        public static string imageFormat = string.Empty;
        /// <summary>
        /// The Client ID is used by the application to uniquely identify itself to Azure AD.
        /// </summary>
        public static string ClientId { get; } = Startup.StaticConfig.GetSection("AzureAd").GetSection("ClientId").Value;

        /// <summary>
        /// The ClientSecret is a credential used to authenticate the application to Azure AD.  Azure AD supports password and certificate credentials.
        /// </summary>
        public static string ClientSecret { get; } = Startup.StaticConfig.GetSection("AzureAd").GetSection("ClientSecret").Value;

        /// <summary>
        /// Redirect URI
        /// </summary>
        public static string RedirectUri { get; } = Startup.StaticConfig.GetSection("AzureAd").GetSection("BaseUrl").Value + "/";
        /// <summary>
        /// Domain URI
        /// </summary>
        public static string DomainUri { get; } = Startup.StaticConfig.GetSection("AzureAd").GetSection("DomainUrl").Value;

        /// <summary>
        /// Base URI
        /// </summary>
        public static string BaseUri { get; } = Startup.StaticConfig.GetSection("AzureAd").GetSection("BaseUrl").Value;
        /// <summary>
        /// Get Id of the installed App4Legal add-on
        /// </summary>
        public static string TeamsAppId { get; } = Startup.StaticConfig.GetSection("AzureAd").GetSection("TeamsAppId").Value;

        /// <summary>
        /// The Post Logout Redirect Uri is the URL where the user will be redirected after they sign out.
        /// </summary>
        public static string PostLogoutRedirectUri { get; } = Startup.StaticConfig.GetSection("ida:PostLogoutRedirectUri").Value;

        /// <summary>
        /// The TenantId is the DirectoryId of the Azure AD tenant being used in the sample
        /// </summary>
        public static string TenantId { get; } = Startup.StaticConfig.GetSection("ida:TenantId").Value;

        public void SaveProfileImage(Success res)
        {
            try
            {
                int width = 480;
                var height = UInt16.MaxValue - 36; //succeeds at 65499, 65500
                string ImageString = res.data.profilePicture.content;
                string Imageformat = res.data.profilePicture.type;
                imageFormat = Imageformat;
                string webRootPath = _hostingEnvironment.WebRootPath;
                var path = Path.Combine(webRootPath, "Images", "ProfileImage." + Imageformat);
                if (System.IO.File.Exists(path))
                        System.IO.File.Delete(path);
                byte[] bytes = Convert.FromBase64String(ImageString);
                System.Drawing.Image image = new Bitmap(width, height);
                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    image = System.Drawing.Image.FromStream(ms);
                    image.Save(path);
                }
            }
            catch (Exception ex)
            {
                logger.Error("Globals - SaveProfileImage Method - Message :" + ex.Message);
            }
        }
        public static string EncryptString(string key, string plainText)
        {
            try
            {
                byte[] iv = new byte[16];
                byte[] array;

                using (Aes aes = Aes.Create())
                {
                    aes.Key = Encoding.UTF8.GetBytes(key);
                    aes.IV = iv;

                    ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
                        {
                            using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                            {
                                streamWriter.Write(plainText);
                            }

                            array = memoryStream.ToArray();
                        }
                    }
                }

                return Convert.ToBase64String(array);
            }
            catch(Exception ex)
            {
                logger.Error("Globals - EncryptString Method - Message :" + ex.Message);
                return plainText;
            }
        }

        public static string DecryptString(string key, string cipherText)
        {
            try
            {
                byte[] iv = new byte[16];
                byte[] buffer = Convert.FromBase64String(cipherText);

                using (Aes aes = Aes.Create())
                {
                    aes.Key = Encoding.UTF8.GetBytes(key);
                    aes.IV = iv;
                    ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                    using (MemoryStream memoryStream = new MemoryStream(buffer))
                    {
                        using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
                        {
                            using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
                            {
                                return streamReader.ReadToEnd();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Globals - DecryptString Method - Message :" + ex.Message);
                return cipherText;
            }
        }
        public static string extractingCaseIdFromChannelDescription(string channelDescription, string teamName = "")
        {
            try
            {
                if (channelDescription != null)
                {
                    var hearingIdString = "Hearing ID: ";
                    var caseIdString = "Case ID: ";
                    var contractIdString = "Contract ID: ";
                    var keyString = caseIdString;
                    if(teamName.Length > 0)
                    {
                        switch (teamName)
                        {
                            case "App4Legal-Hearing":
                                {
                                    keyString = hearingIdString;
                                }
                                break;                            
                            case "App4Legal-Contract":
                                {
                                    keyString = contractIdString;
                                }
                                break;
                        }
                    }
                    int index = channelDescription.IndexOf(keyString);
                    if (index == -1)
                    {
                        index = channelDescription.IndexOf(contractIdString);
                        keyString = contractIdString;
                    }
                    if (index >= 0)
                    {
                        int indexOfComma = channelDescription.IndexOfAny(new char[] { ',' }, index);
                        if (indexOfComma > 0)
                        {
                            var caseId = channelDescription.Slice(index + keyString.Length, indexOfComma);
                            return caseId;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                logger.Error("Globals - extractingCaseIdFromChannelDescription Method - Message :" + ex.Message);
            }
            return "";
        }
    }
    public static class ExtraExtensions
    {
        /// <summary>
        /// Get the string slice between the two indexes.
        /// Inclusive for start index, exclusive for end index.
        /// </summary>
        public static string Slice(this string source, int start, int end)
        {
            if (end < 0) // Keep this for negative end support
            {
                end = source.Length + end;
            }
            int len = end - start;               // Calculate length
            return source.Substring(start, len); // Return Substring of length
        }
    }
}