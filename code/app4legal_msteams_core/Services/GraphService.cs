﻿/* 
*  Copyright (c) Microsoft. All rights reserved. Licensed under the MIT license. 
*  See LICENSE in the source repository root for complete license information. 
*/

using app4legal_msteams_core.Controllers;
using app4legal_msteams_core.Models;
using app4legal_msteams_core.Utils;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Graph;
using Microsoft_Teams_Graph_RESTAPIs_Connect.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using NLog.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace app4legal_msteams_core.Services
{
    public static class GraphService
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private const string Pattern = "~#%&*{}+/\\:<>?|'\".";
        public static string myId = string.Empty;
        public static string myMail = string.Empty;
        public static bool isThreadFinished = true;
        public delegate void addingMemberCallBack(bool isForbidden, string memberMail, Exception ex);
        public delegate void addingGuestCallBack(bool isForbidden, Exception ex);
        private static string createdTeamId = "";
        public static string RemoveSpecialChars(string str)
        {
            // Create  a string array and add the special characters you want to remove
            string[] chars = new string[] { "~", ".", "#", "%", "&", "*", "{", "}", "+", "/", "'", "\"", "\\", ":", "<", ">", "?", "|" };
            //Iterate the number of times based on the String array length.
            for (int i = 0; i < chars.Length; i++)
            {
                if (str.Contains(chars[i]))
                {
                    str = str.Replace(chars[i], "");
                }
            }
            return str;
        }
        public static async void sendChannelMessages(GraphServiceClient graphClient, string teamId, string channelId, dynamic model)
        {
            if (model != null)
            {
                try
                {
                    var channelName = await fixChannelName(graphClient, model, true);
                    string category = model.CaseCategory;
                    var url = "";
                    var messageString = "";
                    if (model.CaseCategory.Equals("Contract"))
                    {
                        url = (LoginController.credentials != null) ? (LoginController.credentials.userUrl + @"modules/contract/contracts/view/" + model.ContractId) : "";
                    }
                    else
                    {
                        url = (LoginController.credentials != null) ? (LoginController.credentials.userUrl + @"cases/edit/" + model.CaseId) : "";
                        messageString = " Matter ";
                    }
                    if ((model.CaseCategory as string).Equals("Hearing"))
                        messageString = " with ID: " + model.HearingId + " related to the Litigation Matter ";
                    var setupChatMessage = new ChatMessage
                    {
                        Body = new ItemBody
                        {
                            ContentType = BodyType.Html,
                            Content = "This channel is to discuss the " + (category.Equals("Matter") ? "Corporate" : category) + messageString + " <a href='" + url + "'>" + channelName + "</a>"
                        }
                    };
                    await graphClient.Teams[teamId].Channels[channelId].Messages
                                                   .Request()
                                                   .AddAsync(setupChatMessage);
                    var archiveChatMessage = new ChatMessage
                    {
                        Body = new ItemBody
                        {
                            ContentType = BodyType.Html,
                            Content = "This channel will be automatically deleted if it was detected idle, which means no messages are sent on it, for more than two days. Note: It can be restored within 30 days from the team settings."
                        }
                    };
                    await graphClient.Teams[teamId].Channels[channelId].Messages
                                                   .Request()
                                                   .AddAsync(archiveChatMessage);

                }
                catch (Exception ex)
                {
                    logger.Error("sendChannelMessages method - Message: " + ex.Message);
                }
            }
        }

        public static async Task<string> fixChannelName(GraphServiceClient graphClient, dynamic model, bool isCustomized = false, bool duplicateChannel = false, string teamId = "", int extraDuplicate = 0)
        {
            try
            {
                string finalCaseName = string.Empty;
                string category = model.CaseCategory;
                string caseName = model.CaseName;
                int channelDuplicationNumber = 0;
                if (duplicateChannel)
                {
                    //Check the number of created channels for same matter/hearing/contract
                    if (teamId.Length > 0)
                    {
                        var channels = await graphClient.Teams[teamId].Channels.Request().GetAsync();
                        var channelCaseId = "";
                        foreach(var channel in channels)
                        {
                            try
                            {
                                channelCaseId = "";

                                //Extract the case id from description, get first number:
                                var numbers = Regex.Split(channel.Description, @"\D+");
                                foreach (var value in numbers)
                                {
                                    if (value.Length > 0)
                                    {
                                        channelCaseId = value;
                                        break;
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                logger.Warn("fixChannelName Method - Extracting Channel name from description. Message :" + e.Message);
                            }
                            switch (category)
                            {
                                case "Hearing":
                                    {
                                        if (channelCaseId.Equals(model.HearingId))
                                        {
                                            channelDuplicationNumber++;
                                        }
                                    }
                                    break;
                                case "Contract":
                                    {
                                        if (channelCaseId.Equals(model.ContractId.ToString()))
                                        {
                                            channelDuplicationNumber++;
                                        }
                                    }
                                    break;
                                default:
                                    {
                                        if (channelCaseId.Equals(model.CaseId.ToString()))
                                        {
                                            channelDuplicationNumber++;
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                    channelDuplicationNumber+= extraDuplicate;
                }
                if (isCustomized)
                {
                    switch (category)
                    {
                        case "Hearing":
                            {
                                finalCaseName = "M" + model.CaseId + " - " + caseName;
                            }
                            break;
                        case "Contract":
                            {
                                finalCaseName = caseName;
                            }
                            break;
                        default:
                            {
                                finalCaseName = "M" + model.CaseId + " - " + caseName.Substring(11, caseName.Length - 11);
                            }
                            break;
                    }
                }
                else
                {

                    caseName = caseName.Replace(':', '-');
                    var removedSpecialCharacterCaseName = RemoveSpecialChars(caseName);
                    finalCaseName = removedSpecialCharacterCaseName;
                    switch (category)
                    {
                        case "Corporate Matter":
                        case "Litigation Case":
                            finalCaseName = "M" + model.CaseId + "-" + ((duplicateChannel && channelDuplicationNumber > 0) ? (channelDuplicationNumber.ToString() + "-") : "");
                            finalCaseName += removedSpecialCharacterCaseName.Substring(11, removedSpecialCharacterCaseName.Length - 11);
                            break;
                        case "Hearing":
                            finalCaseName = "H" + model.HearingId + "-" + ((duplicateChannel && channelDuplicationNumber > 0) ? (channelDuplicationNumber.ToString() + "-") : "");
                            finalCaseName += removedSpecialCharacterCaseName;
                            break;
                        case "Contract":
                            removedSpecialCharacterCaseName = removedSpecialCharacterCaseName.Substring(removedSpecialCharacterCaseName.IndexOf("-") + 2);
                            finalCaseName = "CT" + model.ContractId + "-" + ((duplicateChannel && channelDuplicationNumber > 0) ? (channelDuplicationNumber.ToString() + "-") : "");
                            finalCaseName += removedSpecialCharacterCaseName;
                            break;
                    }
                }
                return (finalCaseName.Length > 50 && !isCustomized) ?
                    finalCaseName.Substring(0, 50) : finalCaseName;
            }
            catch (Exception ex)
            {
                logger.Error("fixChannelName method - Message: " + ex.Message);
                return "Generic Channel";
            }
        }
        public static async Task<bool> checkIfUserIsAChannelMember(GraphServiceClient graphClient, GraphServiceClient graphAppClient, string teamId, string channelId)
        {
            try
            {
                var membersTask = graphAppClient.Teams[teamId].Channels[channelId].Members.Request().GetAsync();
                var myId = await getMyUserId(graphClient);
                var members = await membersTask;
                foreach (AadUserConversationMember member in members)
                {
                    if (member.UserId.Equals(myId))
                    {

                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("checkIfUserIsAChannelMember method - Message: " + ex.Message);
            }
            return false;
        }
        public static dynamic getTeamsIdFromString(string teamsIdString)
        {
            if (teamsIdString.Contains(";"))
            {
                var arrayOfTeams = teamsIdString.Split(new char[] { ';' });
                return arrayOfTeams;
            }
            return teamsIdString;
        }
        public static async Task<List<string>> checkIfTeamExistsAsync(GraphServiceClient graphClient, GraphServiceClient graphApplicationClient, List<string> teamIds, string teamName = "", bool checkExtension = false)
        {
            var teamsId = new List<string>();
            try
            {

                var teamsFound = new List<Microsoft.Graph.Group>();
                var teamsPage = await graphClient.Groups.Request().GetAsync();
                teamsFound.AddRange(teamsPage);
                while (teamsPage.NextPageRequest != null)
                {
                    teamsPage = await teamsPage.NextPageRequest.GetAsync();
                    teamsFound.AddRange(teamsPage);
                }
                foreach (var t in teamIds)
                {
                    foreach (var team in teamsFound)
                    {
                        if (team.Id.Equals(t))
                        {
                            teamsId.Add(t);
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("checkIfTeamExistsAsync method - Message: " + ex.Message);
            }
            try
            {
                if (checkExtension)
                {
                    if (!teamIds.All(teamsId.Contains) || (teamsId.Count != teamIds.Count))
                        await updateOrganizationApp4LegalExtension(graphApplicationClient, teamName, teamsId, true).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                logger.Error("checkIfTeamExistsAsync method - updateOrganizationApp4LegalExtension -Message: " + ex.Message);
            }
            return teamsId;
        }
        public static async Task<string> getTeamIdByNameAsync(GraphServiceClient graphClient, string teamName)
        {
            try
            {

                var teamsFound = new List<Microsoft.Graph.Group>();
                var teamsPage = await graphClient.Groups.Request().GetAsync();
                teamsFound.AddRange(teamsPage);
                while (teamsPage.NextPageRequest != null)
                {
                    teamsPage = await teamsPage.NextPageRequest.GetAsync();
                    teamsFound.AddRange(teamsPage);
                }
                foreach (var team in teamsFound)
                {
                    if (team.DisplayName.Equals(teamName))

                    {
                        return team.Id;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("getTeamIdByNameAsync method - Message: " + ex.Message);
            }
            return "";
        }
        public static async Task<object> getTeamIdByNameFromOrgExtensionsAsync(GraphServiceClient graphAppClient, string teamName)
        {
            var app4LegalExtension = await checkIfOrgExtensionExist(graphAppClient);
            if (app4LegalExtension != null)
            {
                //Extension of App4Legal exists in the organization, check if the team exists
                try
                {
                    if (app4LegalExtension.AdditionalData != null)
                    {
                        var teamId = app4LegalExtension.AdditionalData["App4Legal-" + teamName];
                        if (((JArray)teamId).Count > 0)
                            return teamId;
                    }
                }
                catch (Exception e)
                {
                    logger.Error("getTeamIdByNameFromOrgExtensionsAsync method - Check if team exists in organization extension- Message: " + e.Message);
                }
                return null;
            }
            else
            {
                try
                {
                    //Extension not found, create new one
                    OrganizationExtension app4LegalExt = new OrganizationExtension()
                    {
                        Id = Globals.ExtensionName,
                        AdditionalData = new Dictionary<string, object>()
                                    {
                                        { "@odata.type", "#microsoft.graph.openTypeExtension"}
                                    }
                    };
                    var tenantId = await getOrganizationId(graphAppClient);
                    await graphAppClient.Organization[tenantId].Extensions.Request().AddAsync(app4LegalExt);
                }
                catch (Exception e)
                {
                    logger.Error("getTeamIdByNameFromOrgExtensionsAsync method - Adding App4Legal extension to the organization - Message: " + e.Message);
                }
                return null;
            }
        }
        public static async Task<List<Dictionary<string, object>>> getDataFromOrganizationExtension(GraphServiceClient graphAppClient, string keyName = "")
        {
            //In case of a team newly created, return the created one instead of fetching the extension
            if (createdTeamId.Length > 0)
            {
                var localTeamId = createdTeamId;
                createdTeamId = string.Empty;
                return new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object>()
                    {
                        {"TeamCreated",new JArray(localTeamId) }
                    }
                };
            }
            var app4LegalExtension = await checkIfOrgExtensionExist(graphAppClient);
            var dataDic = new List<Dictionary<string, object>>();
            if (app4LegalExtension != null)
            {
                //Extension of App4Legal exists in the organization, check if the team exists
                try
                {
                    if (app4LegalExtension.AdditionalData != null)
                    {
                        foreach (var keyValue in app4LegalExtension.AdditionalData)
                        {
                            if (keyName.Length > 0 && keyName.Equals(keyValue.Key.ToString()))
                            {
                                return new List<Dictionary<string, object>>()
                                {
                                    new Dictionary<string, object>()
                                        {
                                            {keyValue.Key.ToString(), keyValue.Value}
                                        }
                                };
                            }
                            dataDic.Add(new Dictionary<string, object>()
                            {
                                {keyValue.Key.ToString(), keyValue.Value}
                            });
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.Error("getTeamsFromOrganizationExtension method - Message: " + e.Message);
                }
            }
            return dataDic;
        }
        public static async Task<List<String>> getApp4LegalAllUsers()
        {
            List<string> users = new List<string>();
            try
            {
                string response = await Web.sendRequestToGETtData(LoginController.credentials.userUrl + @"modules/api/auth_users/autocomplete");
                var jsonObject = JsonConvert.DeserializeObject<dynamic>(response);
                if (jsonObject["error"] != "")
                {
                    Helpers.Classes.errors data = JsonConvert.DeserializeObject<Helpers.Classes.errors>(response);
                    logger.Error(data.error);
                }
                else
                {
                    var p = jsonObject["success"];
                    var usersArray = p["data"];
                    foreach (dynamic user in usersArray)
                    {
                        users.Add(user.email.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error("getApp4LegalAllUsers method - Message: " + e.Message);
            }
            return users;
        }
        public async static Task<List<string>> checkIFApp4LegalUsersExistInTeamsAsync(GraphServiceClient graphClient)
        {
            try
            {
                var app4LegalUsers = await getApp4LegalAllUsers();
                var orgMembers = await GetOrganizationMembers(graphClient);
                var memberExistList = new List<string>();
                foreach (var user in app4LegalUsers)
                {
                    foreach (var member in orgMembers)
                    {
                        if (member["mail"] != null)
                        {
                            if (member["mail"].ToLower().Equals(user.ToLower()))
                            {
                                memberExistList.Add(member["id"]);
                                orgMembers.Remove(member);
                                break;
                            }
                        }
                    }
                }
                return memberExistList;
            }
            catch (Exception ex)
            {
                logger.Error("checkIFApp4LegalUsersExistInTeamsAsync method - Message: " + ex.Message);
                return new List<string>();
            }
        }
        /// <summary>
        ///Add existed members in organization to the team by using its ID
        ///
        /// </summary>
        /// <param name="teamId">Id of the created team</param>
        /// <param name="members">Members to be joined in the team</param>
        public static async Task<bool> addMembersToTeamAsync(GraphServiceClient graphClient, string teamId, List<string> members)
        {
            var conversationMembers = new ChannelMembersCollectionPage();
            members.ForEach(element =>
            {
                conversationMembers.Add(new AadUserConversationMember()
                {
                    Roles = new List<String>()
                    {
                        "owner"
                    },
                    AdditionalData = new Dictionary<string, object>()
                           {
                                { "user@odata.bind",ClaimConstants.graphApiBaseURL + "/users('" + element +"')" },
                           },
                });
            });
            foreach (var member in conversationMembers)
            {
                //Add user to the the team
                int attempts = 0;
                bool isMemberAdded = false;
                while (!isMemberAdded && attempts < 10)
                {
                    try
                    {
                        var result = await graphClient.Teams[teamId].Members.Request().AddAsync(member);
                        isMemberAdded = true;
                    }
                    catch(Exception ex)
                    {
                        attempts++;
                        Thread.Sleep(1000);
                        logger.Error("addMembersToTeamAsync method - Message: " + ex.Message);
                    }
                }
                Thread.Sleep(1000);
            }
            return true;
        }
        private static void ProcessAllResultPages<TResult, TItem>(IBaseRequest request,
                                                   Action<TResult> processorDelegate)
                                                   where TResult : ICollectionPage<TItem>
        {
            do
            {
                Task<TResult> task = ((dynamic)request).GetAsync();
                processorDelegate(task.Result); // This will implicitly call Wait() on the task.
                request = ((dynamic)task.Result).NextPageRequest;
            } while (request != null);
        }
        public static async Task<List<Microsoft.Graph.User>> getUsersAllPages(GraphServiceClient graphClient)
        {
            try
            {
                var usersFound = new List<Microsoft.Graph.User>();
                var usersPage = await graphClient.Users.Request().GetAsync();
                usersFound.AddRange(usersPage);
                while (usersPage.NextPageRequest != null)
                {
                    usersPage = await usersPage.NextPageRequest.GetAsync();
                    usersFound.AddRange(usersPage);
                }
                return usersFound;
            }
            catch (Exception ex)
            {
                logger.Error("getUsersAllPages method - Message: " + ex.Message);
                return new List<Microsoft.Graph.User>();
            }
        }
        public static async Task<List<Dictionary<string, string>>> GetOrganizationMembers(GraphServiceClient graphClient)
        {
            var users = await getUsersAllPages(graphClient);
            var myId = await getMyUserId(graphClient);
            List<Dictionary<string, string>> members = new List<Dictionary<string, string>>();
            foreach (var user in users)
            {
                if (user.UserPrincipalName != null)
                {
                    if (!user.UserPrincipalName.Contains("#EXT#"))
                    {
                        if (user.Id != null)
                        {
                            if (!user.Id.Equals(myId))
                            {
                                members.Add(new Dictionary<string, string>()
                                {
                                    {"name", user.DisplayName },
                                    {"mail", user.Mail },
                                    {"id", user.Id }
                                });
                            }
                        }
                    }
                }
            }
            return members;
        }
        public static async Task<Dictionary<String, List<object>>> CheckMembersAndGuestsAsync(GraphServiceClient graphClient, string[] emails)
        {
            var users = await getUsersAllPages(graphClient);
            List<object> usersIdAndMail = new List<object>();
            List<object> guestMail = new List<object>();
            try
            {
                for (int i = 0; i < emails.Count(); i++)
                {
                    bool isMember = false;
                    foreach (var user in users)
                    {
                        if (user.Mail != null)
                        {
                            if (user.Mail.Equals(emails[i]))
                            {
                                //is Member
                                var singleUser = new Dictionary<string, string>()
                            {
                                {"id", user.Id },
                                {"mail", emails[i] }
                            };
                                usersIdAndMail.Add(singleUser);
                                users.Remove(user);
                                isMember = true;
                                break;
                            }
                        }
                    }
                    if (!isMember)
                    {
                        //is Guest
                        guestMail.Add(emails[i]);
                    }
                }
            }
            catch(Exception ex)
            {
                logger.Error("CheckMembersAndGuestsAsync - Message: " + ex.Message);
            }
            return new Dictionary<string, List<object>>()
            {
                { "AddMemberIdAndMail" , usersIdAndMail },
                { "InviteGuestMail" , guestMail }
            };

        }
        public static async Task<string> getUserMailById(GraphServiceClient graphClient, string id)
        {
            var users = await getUsersAllPages(graphClient);
            try
            {
                foreach (var user in users)
                {
                    if (user.Id.Equals(id))
                    {
                        return user.Mail;
                    }
                }
            }
            catch(Exception ex)
            {
                logger.Error("getUserMailById - Message: " + ex.Message);
            }
            return "";
        }
        public static async Task<ITeamMembersCollectionPage> getTeamMembers(GraphServiceClient graphClient, string teamId)
        {
            return await graphClient.Teams[teamId].Members.Request().GetAsync();
        }
        public static string getCaseCategory(string category)
        {
            switch (category)
            {
                case "Matter": return "Corporate Matter";
                case "Litigation": return "Litigation Case";
                case "Hearing": return "Hearing";
                default: return category;
            }
        }
        public static async Task<CredentialsModel> checkIfLoggedIn(GraphServiceClient graphClient)
        {
            try
            {
                var extensions = await graphClient.Users[myId].Extensions.Request().GetAsync();
                //Check if extension exists
                if (extensions.Count > 0)
                {
                    foreach (var temp in extensions)
                    {
                        if (temp.Id.Contains("app4legal"))
                        {
                            var apiKey = Globals.DecryptString(LoginController.getExtensionSecretKey(), temp.AdditionalData["x-api-key"].ToString());
                            var userUrl = Globals.DecryptString(LoginController.getExtensionSecretKey(), temp.AdditionalData["url"].ToString());
                            return new CredentialsModel()
                            {
                                apiKey = apiKey,
                                userUrl = userUrl
                            };
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error("checkIfLoggedIn method - Message: " + e.Message);
            }
            return null;
        }
        public static async Task<Extension> checkIfOrgExtensionExist(GraphServiceClient graphAppClient)
        {
            var extensions = await getOrgExtensions(graphAppClient);
            if (extensions != null)
            {
                foreach (var extension in extensions)
                {
                    var extId = getExtensionId(new List<object>() { extension });
                    if (extId != "")
                        return extension;
                }
            }
            return null;
        }
        public static async Task updateOrganizationApp4LegalExtension(GraphServiceClient graphAppClient, string key, object updatedValue, bool isRemoving = false)
        {
            var app4LegalExtension = await checkIfOrgExtensionExist(graphAppClient);
            var tenantId = await getOrganizationId(graphAppClient);
            try
            {
                if (app4LegalExtension != null)
                {
                    if (app4LegalExtension.AdditionalData == null)
                    {
                        app4LegalExtension.AdditionalData = new Dictionary<string, object>()
                        {
                            {key, updatedValue }
                        };
                    }
                    else
                    {
                        if (app4LegalExtension.AdditionalData.ContainsKey(key))
                        {
                            var oldValue = app4LegalExtension.AdditionalData[key];
                            if (isRemoving)
                            {
                                oldValue = JArray.FromObject(updatedValue);
                            }
                            else
                            {
                                if (updatedValue is List<string>)
                                {
                                    var objectToList = updatedValue as List<string>;
                                    objectToList.ForEach(item => ((JArray)oldValue).Add(item));
                                    updatedValue = objectToList;
                                }
                                else
                                {
                                    if (key.Equals("archiveFiles"))
                                        oldValue = !Boolean.Parse(oldValue.ToString());
                                    else
                                        oldValue = updatedValue;
                                }
                            }
                            app4LegalExtension.AdditionalData[key] = oldValue;
                        }
                        else
                            app4LegalExtension.AdditionalData.Add(key, updatedValue);
                    }
                    var updatedExtension = await graphAppClient.Organization[tenantId]
                                            .Extensions[app4LegalExtension.Id].Request()
                                            .UpdateAsync(app4LegalExtension).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                logger.Error("updateOrganizationApp4LegalExtension method - Message: " + e.Message);
                throw e;
            }
        }
        public static async Task<string> createTeamAsync(GraphServiceClient graphClient, GraphServiceClient graphApplicationClient, string teamName, dynamic controller)
        {
            createdTeamId = string.Empty;
            var teamId = string.Empty;

            try
            {
                var createTeamTask = createTeamAndGetId(graphClient, teamName);
                Task<List<string>> existedMembersTask = checkIFApp4LegalUsersExistInTeamsAsync(graphClient);
                teamId = await createTeamTask;
                if (teamId.Length > 0)
                {
                    //Add the created team as an extension on Organization Level
                    Task updateOrgExtensionThread = updateOrganizationApp4LegalExtension(graphApplicationClient, teamName, new List<string>() { teamId });
                    var existedMembers = await existedMembersTask;
                    await addMembersToTeamAsync(graphClient, teamId, existedMembers);
                    try
                    {
                        await updateOrgExtensionThread;
                    }
                    catch
                    {
                        logger.Error("createTeamAsync - updateOrgExtension");
                    }
                    if (controller is CasesController)
                        CasesController.teamId = teamId;
                    if (controller is HearingController)
                        HearingController.teamId = teamId;

                    //Install the app to the team
                    try
                    {
                        int attempts = 0;
                        bool isAppCreated = false;
                        while(!isAppCreated && attempts < 10)
                        {
                            isAppCreated = await addAppToTeam(graphClient, teamId);
                            attempts++;
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error("createTeamAsync - Installing teamsApp - Message : " + ex.Message);
                    }
                    createdTeamId = teamId;
                    return "{\"code\":\"CreateChannel\"}";
                }
                else
                {
                    return "{\"code\":\"Error\", \"message\":\"Internal Server Error\"}";
                }
            }
            catch (Exception e)
            {
                logger.Error("createTeamAsync method - Message: " + e.Message);
                logger.Error("createTeamAsync method - Message: " + e.StackTrace);

                return "{\"code\":\"Error\", \"message\":\"" + e.Message + "\"}";
            }
        }
        public static async Task<bool> addAppToTeam(GraphServiceClient graphClient, string teamId)
        {
            var appCatalogApps = await graphClient.AppCatalogs.TeamsApps.Request().GetAsync();
            var teamsAppId = Globals.TeamsAppId;
            for (int i = 0; i < appCatalogApps.Count; i++)
            {
                var app = appCatalogApps[i];
                if (app.DisplayName.Equals("App4Legal"))
                {
                    teamsAppId = app.Id;
                    break;
                }
            }
            var teamsApp = new Microsoft.Graph.TeamsAppInstallation()
            {
                AdditionalData = new Dictionary<string, object>()
                    {
                        {"teamsApp@odata.bind","https://graph.microsoft.com/v1.0/appCatalogs/teamsApps/" + teamsAppId }
                    }
            };
            var message = graphClient.Teams[teamId].InstalledApps.Request().GetHttpRequestMessage();
            message.Method = HttpMethod.Post;
            message.Content = new StringContent(graphClient.HttpProvider.Serializer.SerializeObject(teamsApp));
            message.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            try
            {
                var response = await graphClient.HttpProvider.SendAsync(message);
                return response.StatusCode.ToString().Equals("Created");
            }
            catch (Exception ex)
            {
                logger.Error("addAppToTeam method - Message: " + ex.Message);
            }
            return false;
        }
        public static async Task<string> createTeamAndGetId(GraphServiceClient graphClient, string teamName)
        {

            //Create the team
            var message = graphClient.Teams.Request().GetHttpRequestMessage();
            message.Method = HttpMethod.Post;
            var team = new Microsoft.Graph.Team
            {
                DisplayName = teamName,
                Description = "App4Legal sub-Team",
                AdditionalData = new Dictionary<string, object>()
                    {
                        {"template@odata.bind", "https://graph.microsoft.com/v1.0/teamsTemplates('standard')"}
                    }
            };
            message.Content = new StringContent(graphClient.HttpProvider.Serializer.SerializeObject(team));
            message.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            try
            {
                var response = await graphClient.HttpProvider.SendAsync(message);
                var teamIdString = response.Content.Headers.ContentLocation.OriginalString;
                var extractedTeamId = teamIdString.Substring(8, teamIdString.Length - 10);
                return extractedTeamId;
            }
            catch (Exception ex)
            {
                logger.Error("createTeamAndGetId method - Message: " + ex.Message);
            }
            return "";
        }
        public static async void checkTeamsChannelsValidity(GraphServiceClient graphClient, GraphServiceClient graphApplicationClient, string tempFilesPath)
        {
            //Setting Reset event for waiting all threads to terminate
            var resetEvents = new List<AutoResetEvent>();

            //Set datetime variable relative to the current GMT time - 2 days
            DateTime now = DateTime.UtcNow;
            DateTime twoDaysAgo = now.AddDays(-2);
            string formattedDate = twoDaysAgo.ToString("yyyy-MM-ddTHH:mm:ssZ");


            //Get 3 Teams (Corporate, Litigation, and Hearing)
            var teams = await getDataFromOrganizationExtension(graphApplicationClient);
            foreach (var team in teams)
            {
                var re = new AutoResetEvent(false);
                resetEvents.Add(re);

                ThreadPool.QueueUserWorkItem(async w =>
                {
                    var threadReset = w as AutoResetEvent;
                    var random = new Random();
                    try
                    {
                        var teamIds = ((JArray)(team.Values.First())).ToObject<List<string>>();
                        //Check if the team not deleted between current teams
                        var teamExistsInTeams = await checkIfTeamExistsAsync(graphClient, graphApplicationClient, teamIds, team.Keys.First(), true);
                        if (teamExistsInTeams.Count > 0)
                        {
                            foreach (var teamId in teamExistsInTeams)
                            {
                                var innerResetEvents = new List<AutoResetEvent>();
                                //Get channels
                                var channels = await graphClient.Teams[teamId].Channels.Request().GetAsync();
                                foreach (var channel in channels)
                                {
                                    var re = new AutoResetEvent(false);
                                    innerResetEvents.Add(re);

                                    ThreadPool.QueueUserWorkItem(async w =>
                                    {
                                        var threadReset = w as AutoResetEvent;
                                        var random = new Random();
                                        try
                                        {
                                            //Check channel messages modification date, if > 2 days, delete it and archive its content
                                            if (channel.MembershipType == ChannelMembershipType.Private)
                                            {
                                                //Distinguish between general channel and other private channels

                                                // Set queury string for checking if the last modified message date is greater than two days.                                
                                                //string singInUrl = "https://graph.microsoft.com/beta/teams/" + teamId + "/channels/"
                                                //                    + channel.Id +
                                                //                    "/messages/delta?$filter=lastModifiedDateTime gt " + formattedDate;                                                
                                                //var responses = Web.sendGenericRequestToGETtData(singInUrl, HomeController.tempToken);
                                                graphClient.BaseUrl = ClaimConstants.graphApiBaseURLBETA;
                                                var queryOptions = new List<QueryOption>()
                                                {
                                                    new QueryOption("$filter", "lastModifiedDateTime gt " + formattedDate),
                                                    new QueryOption("$top", "2")
                                                };
                                                var responses = await graphClient.Teams[teamId].Channels[channel.Id].Messages["delta"].Request(queryOptions).GetAsync();         
                                                if (responses != null)
                                                {
                                                    logger.Info("checkTeamsChannelsValidity - Check Channel Validity - Channel Name : " + channel.DisplayName);
                                                    if ((responses.AdditionalData?["value"] as JArray).Count > 0)
                                                    {
                                                        //Messages found in the last two days
                                                        logger.Info("checkTeamsChannelsValidity - Channel Active - Channel Name : " + channel.DisplayName);
                                                    }
                                                    else
                                                    {
                                                        //Channel is Idle, retrieve its messages, files and delete it.
                                                        logger.Info("checkTeamsChannelsValidity - Channel archiving - Channel Name : " + channel.DisplayName + " - Begin");
                                                        graphClient.BaseUrl = ClaimConstants.graphApiBaseURL;
                                                        var caseId = Globals.extractingCaseIdFromChannelDescription(channel.Description);
                                                        if (caseId.Length > 0)
                                                        {
                                                            try
                                                            {
                                                                logger.Info("checkTeamsChannelsValidity - Channel archiving - Channel Name : " + channel.DisplayName + " - Archive messages");
                                                                string htmlPageString = "";
                                                                var token = getTokenFromChannelDescription(channel.Description);
                                                                var synchronizedMessages = await retrieveChannelMessagesSynchronized(graphClient, teamId, channel.Id, team.Keys.First().ToString(), channel.DisplayName, "", token);
                                                                if (synchronizedMessages != null && synchronizedMessages.Count > 0)
                                                                    htmlPageString = getChannelsMessagesString(synchronizedMessages, "", channel.DisplayName, "");

                                                                //string htmlPageString = await retrieveChannelMessages(teamId, channel.Id, team.Keys.First().ToString(), channel.DisplayName);
                                                                if (htmlPageString.Length > 0)
                                                                {
                                                                    //Send it to App4Legal server
                                                                    if (caseId.Length > 0)
                                                                    {
                                                                        logger.Info("checkTeamsChannelsValidity - Channel archiving - Channel Name : " + channel.DisplayName + " - Send Messages to Server - Begin");
                                                                        sendCaseNoteAsync(caseId, htmlPageString);
                                                                        logger.Info("checkTeamsChannelsValidity - Channel archiving - Channel Name : " + channel.DisplayName + " - Send Messages to Server - End");

                                                                    }
                                                                }
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                logger.Error("checkTeamsChannelsValidity - Get channel messages - Messsage: " + ex.Message);
                                                            }
                                                            //Archive Channel files
                                                            logger.Info("checkTeamsChannelsValidity - Channel archiving - Channel Name : " + channel.DisplayName + " - Check archive file status");
                                                            var isArchiveFilesCheckedList = await getDataFromOrganizationExtension(graphApplicationClient, "archiveFiles");
                                                            bool isChecked = Boolean.Parse(isArchiveFilesCheckedList[0].First().Value.ToString());                                                            
                                                            if (isChecked)
                                                            {
                                                                logger.Info("checkTeamsChannelsValidity - Channel archiving - Channel Name : " + channel.DisplayName + " - Archive Files");
                                                                string filesPath = "";
                                                                try
                                                                {
                                                                    var generator = new RandomGenerator();
                                                                    var randomFileName = generator.RandomString(10);
                                                                    var teamFilesPath = Path.Combine(tempFilesPath, team.Keys.First());
                                                                    filesPath = Path.Combine(teamFilesPath, randomFileName);
                                                                    logger.Info("checkTeamsChannelsValidity - Channel archiving - Channel Name : " + channel.DisplayName + " - IO - File Path : " + filesPath);
                                                                    if (!System.IO.Directory.Exists(teamFilesPath))
                                                                        System.IO.Directory.CreateDirectory(tempFilesPath);
                                                                    if (!System.IO.Directory.Exists(filesPath))
                                                                        System.IO.Directory.CreateDirectory(filesPath);
                                                                }
                                                                catch(Exception ex)
                                                                {
                                                                    logger.Error("checkTeamsChannelsValidity - Channel archiving - Channel Name : " + channel.DisplayName + " - IO - Message: " + ex.Message);
                                                                }
                                                                
                                                                var listoffiles = await getChannelFiles(graphClient, teamId, channel.DisplayName);

                                                                if (listoffiles.Count > 0)
                                                                {
                                                                    //Download files
                                                                    var urlTasks = listoffiles.Select((currentFile) =>
                                                                    {
                                                                        WebClient webClient = new WebClient();
                                                                        string url = currentFile.AdditionalData?.Values.First().ToString();
                                                                        var pathName = Path.Combine(filesPath, currentFile.Name);
                                                                        var downloadTask = webClient.DownloadFileTaskAsync(new Uri(url), pathName);
                                                                        return downloadTask;
                                                                    });
                                                                    try
                                                                    {
                                                                        await Task.WhenAll(urlTasks);
                                                                    }
                                                                    catch (Exception ex)
                                                                    {
                                                                        logger.Error("checkTeamsChannelsValidity - Waiting all tasks to download - Message: " + ex.Message);
                                                                    }
                                                                }
                                                                try
                                                                {
                                                                    //Attachments to send
                                                                    var filesToSend = System.IO.Directory.GetFiles(filesPath);
                                                                    if (filesToSend.Length > 0)
                                                                    {

                                                                        var url = "";
                                                                        var caseString = "";
                                                                        if (team.Keys.First().Contains("Contract"))
                                                                        {
                                                                            url = LoginController.credentials?.userUrl + @"modules/api/documents/contract_upload_file";
                                                                            caseString = "module_record_id=" + caseId;
                                                                        }
                                                                        else
                                                                        {
                                                                            url = LoginController.credentials?.userUrl + @"modules/api/cases/attachment_add";
                                                                            caseString = "legal_case_id=" + caseId;
                                                                        }
                                                                        logger.Info("checkTeamsChannelsValidity - Channel archiving - Channel Name : " + channel.DisplayName + " - Send files - Begin");
                                                                        var response = Web.sentRequestToPostFormDataANDMultipleFiles(url, filesPath, caseString);
                                                                        logger.Info("checkTeamsChannelsValidity - Channel archiving - Channel Name : " + channel.DisplayName + " - Send files - End");
                                                                    }
                                                                }
                                                                catch (Exception ex)
                                                                {
                                                                    logger.Error("checkTeamsChannelsValidity - uploadFilesToApp4Legal - Message: " + ex.Message);
                                                                }
                                                                try
                                                                {
                                                                    //Delete the folder created
                                                                    logger.Info("checkTeamsChannelsValidity - Channel archiving - Channel Name : " + channel.DisplayName + " - Delete files folder");
                                                                    System.IO.Directory.Delete(filesPath, true);
                                                                }
                                                                catch (IOException ex)
                                                                {
                                                                    logger.Error("checkTeamsChannelsValidity - delete folder - Message: " + ex.Message);
                                                                }
                                                            }
                                                        }
                                                        logger.Info("checkTeamsChannelsValidity - Channel archiving - Channel Name : " + channel.DisplayName + " - Delete channel");
                                                        //Delete the channel even not created by the App
                                                        await graphClient.Teams[teamId].Channels[channel.Id].Request().DeleteAsync();
                                                    }
                                                }
                                            }
                                            Thread.Sleep(random.Next(100, 2000));
                                        }
                                        catch (Exception ex)
                                        {
                                            logger.Error("checkTeamsChannelsValidity method - Inner catch - Message: " + ex.Message);
                                            // make sure you catch exceptions and release the lock.
                                            // otherwise you will get into deadlocks
                                            threadReset.Set();
                                        }
                                        threadReset.Set();
                                    }, re);
                                }
                                // this bit will wait for all threads to set
                                foreach (AutoResetEvent resetEvent in innerResetEvents)
                                {
                                    resetEvent.WaitOne();
                                }
                            }
                        }
                        Thread.Sleep(random.Next(100, 2000));
                    }
                    catch (Exception ex)
                    {
                        logger.Error("checkTeamsChannelsValidity method - outer catch - Message: " + ex.Message);
                        // make sure you catch exceptions and release the lock.
                        // otherwise you will get into deadlocks
                        threadReset.Set();
                    }
                    threadReset.Set();
                }, re);
            }
            // this bit will wait for all threads to set
            foreach (AutoResetEvent resetEvent in resetEvents)
            {
                resetEvent.WaitOne();
            }
            isThreadFinished = true;
        }
        public static bool sendCaseNoteAsync(string caseId, string htmlDocument)
        {
            try
            {
                string url = ((LoginController.credentials != null) ? LoginController.credentials.userUrl : "") + @"modules/api/cases/note_add";
                string dataToPost = "&comment=" + Environment.NewLine + htmlDocument + Environment.NewLine + Environment.NewLine
                    + "Added By App4Legal for Teams." + "&isVisibleToCP=no&case_id=" + caseId;
                var confirmUpload = Web.sendRequestToPostData(url, dataToPost);
                return true;
            }
            catch (Exception ex)
            {
                logger.Error("sendCaseNoteAsync method - Message: " + ex.Message);
            }
            return false;
        }
        public static async Task<string> retrieveChannelMessages(GraphServiceClient graphClient, string teamId, string channelId, string teamName, string channelName, string noteTitle = "")
        {
            try
            {

                var messagesPage = await graphClient.Teams[teamId].Channels[channelId].Messages.Request().GetAsync();
                var htmlPage = (noteTitle.Length > 0) ? ("<p><b>Note Title:  " + noteTitle + "</b></p> " + Environment.NewLine + Environment.NewLine) : "";
                htmlPage += "<p><b>This is what has been said in the " + channelName + " channel: </b></p> " + Environment.NewLine;
                if (messagesPage.Count > 0)
                {
                    while (messagesPage.NextPageRequest != null || messagesPage.Count > 0)
                    {
                        foreach (var message in messagesPage)
                        {
                            if (message.From != null && message.Body.Content != null)
                            {
                                htmlPage += Environment.NewLine + "<p>" + message.CreatedDateTime.Value.UtcDateTime + ", " + message.From.User.DisplayName + " " + message.Subject
                                    + ":" + ((message.DeletedDateTime == null) ? message.Body.Content : "This message has been deleted") + "</p>" + Environment.NewLine;
                                var messagesReplies = await graphClient.Teams[teamId]
                                                          .Channels[channelId]
                                                          .Messages[message.Id]
                                                          .Replies.Request().GetAsync();
                                while (messagesReplies.NextPageRequest != null || messagesReplies.Count > 0)
                                {
                                    foreach (var reply in messagesReplies)
                                    {
                                        if (reply.From != null && reply.Body.Content != null)
                                        {
                                            htmlPage += Environment.NewLine + "<p>*******************" + reply.CreatedDateTime.Value.UtcDateTime + ", reply: "
                                                + reply.From.User.DisplayName + " " + reply.Subject + ":" + ((reply.DeletedDateTime == null) ? reply.Body.Content : "This message has been deleted") + "</p>" + Environment.NewLine;
                                        }
                                    }
                                    if (messagesReplies.NextPageRequest != null)
                                        messagesReplies = await messagesReplies.NextPageRequest.GetAsync();
                                    else
                                        break;
                                }
                                htmlPage += Environment.NewLine + "<p>-----------------------------------------------------------------------------------------</p>" + Environment.NewLine;
                            }
                        }
                        if (messagesPage.NextPageRequest != null)
                            messagesPage = await messagesPage.NextPageRequest.GetAsync();
                        else
                            break;
                    }
                }
                return htmlPage;
            }
            catch (Exception ex)
            {
                logger.Error("retrieveChannelMessages method - Message: " + ex.Message);
                return null;
            }
        }
        public static string getChannelsMessagesString(IChannelMessagesCollectionPage messages, string teamName, string channelName, string noteTitle = "")
        {
            try
            {
                var htmlPage = (noteTitle.Length > 0) ? ("<p><b>Note Title:  " + noteTitle + "</b></p> " + Environment.NewLine + Environment.NewLine) : "";
                htmlPage += "<p><b>This is what has been said in the " + channelName + " channel [NEW AND UPDATED MESSAGES ONLY]: </b></p> " + Environment.NewLine;
                if (messages?.Count > 0)
                {

                    foreach (var message in messages)
                    {
                        if (message.From != null && message.Body.Content != null)
                        {
                            htmlPage += Environment.NewLine + "<p>" + message.CreatedDateTime.Value.UtcDateTime + ", " + message.From?.User?.DisplayName + " " + message.Subject
                                + ":" + ((message.DeletedDateTime == null) ? message.Body?.Content : "This message has been deleted") + "</p>" + Environment.NewLine;


                            foreach (var reply in message.Replies)
                            {
                                if (reply.From != null && reply.Body.Content != null)
                                {
                                    htmlPage += Environment.NewLine + "<p>*******************" + reply.CreatedDateTime.Value.UtcDateTime + ", reply: "
                                        + reply.From?.User?.DisplayName + " " + reply.Subject + ":" + ((reply.DeletedDateTime == null) ? reply.Body?.Content : "This message has been deleted") + "</p>" + Environment.NewLine;
                                }
                            }

                            htmlPage += Environment.NewLine + "<p>-----------------------------------------------------------------------------------------</p>" + Environment.NewLine;
                        }
                    }
                }
                return htmlPage;
            }
            catch (Exception ex)
            {
                logger.Error("getChannelsMessagesString method - Message: " + ex.Message);
                return null;
            }
        }
        public static async Task<IChannelMessagesCollectionPage> retrieveChannelMessagesSynchronized(GraphServiceClient graphClient, string teamId, string channelId, string teamName, string channelName, string noteTitle = "", string deltaToken = "")
        {
            IChannelMessagesCollectionPage totalMessages = new ChannelMessagesCollectionPage();
            IChannelMessagesCollectionPage syncMessages = new ChannelMessagesCollectionPage();
            var token = "";
            bool isDeltaTokenRetrieved = false;
            try
            {
                graphClient.BaseUrl = ClaimConstants.graphApiBaseURLBETA;
                var queryOptions = new List<QueryOption>()
                                    {
                                        new QueryOption("top", "2")
                                    };
                if (deltaToken.Length > 0)
                {
                    queryOptions.Add(new QueryOption("$deltatoken", deltaToken));
                }
                var chatMessages = await graphClient.Teams[teamId].Channels[channelId].Messages["delta"].Request(queryOptions).GetAsync();
                if (chatMessages != null)
                {
                    //Getting skip token from nextLink
                    var nextLink = "";
                    if (chatMessages.AdditionalData.ContainsKey("@odata.nextLink"))
                        nextLink = chatMessages.AdditionalData?["@odata.nextLink"].ToString();
                    if (nextLink.Length > 0)
                    {
                        token = nextLink.Substring(nextLink.IndexOf("skiptoken=") + 10);
                        if (token.Length > 0)
                        {
                            while (nextLink.Length > 0)
                            {
                                var syncMessagesArray = chatMessages.AdditionalData["value"].ToString();
                                syncMessages = JsonConvert.DeserializeObject<IChannelMessagesCollectionPage>(syncMessagesArray);
                                syncMessages = await getMessagesReplies(graphClient, syncMessages, teamId, channelId);
                                foreach(var message in syncMessages)
                                {
                                    totalMessages.Add(message);
                                }

                                queryOptions = new List<QueryOption>()
                                    {
                                        new QueryOption("$skiptoken", token),
                                        new QueryOption("$top", "2")
                                    };
                                chatMessages = await graphClient.Teams[teamId].Channels[channelId].Messages["delta"].Request(queryOptions).GetAsync();
                                object nextlink;
                                if (chatMessages.AdditionalData.TryGetValue("@odata.nextLink", out nextlink))
                                {
                                    nextLink = nextlink.ToString();
                                    token = nextLink.Substring(nextLink.IndexOf("skiptoken=") + 10);
                                }
                                else
                                {
                                    object deltalink;
                                    if (chatMessages.AdditionalData.TryGetValue("@odata.deltaLink", out deltalink))
                                    {
                                        token = deltalink.ToString().Substring(deltalink.ToString().IndexOf("deltatoken=") + 11);
                                        isDeltaTokenRetrieved = true;
                                    }
                                    var lastSyncMessagesArray = chatMessages.AdditionalData["value"].ToString();
                                    syncMessages = JsonConvert.DeserializeObject<IChannelMessagesCollectionPage>(lastSyncMessagesArray);
                                    syncMessages = await getMessagesReplies(graphClient, syncMessages, teamId, channelId);
                                    foreach (var message in syncMessages)
                                    {
                                        totalMessages.Add(message);
                                    }
                                    nextLink = "";
                                }
                            }
                        }
                    }
                    else
                    {
                        //Getting delta token from deltaLink
                        object deltalink;
                        if (chatMessages.AdditionalData.TryGetValue("@odata.deltaLink", out deltalink))
                        {
                            token = deltalink.ToString().Substring(deltalink.ToString().IndexOf("deltatoken=") + 11);
                            isDeltaTokenRetrieved = true;
                        }
                        var messagesArray = chatMessages.AdditionalData["value"].ToString();
                        syncMessages = JsonConvert.DeserializeObject<IChannelMessagesCollectionPage>(messagesArray);
                        syncMessages = await getMessagesReplies(graphClient, syncMessages, teamId, channelId);
                        foreach (var message in syncMessages)
                        {
                            totalMessages.Add(message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("retrieveChannelMessagesSynchronized - Message: " + ex.Message);
                return null;
            }
            //Save token to the current channel description
            if (isDeltaTokenRetrieved)
            {
                var channel = await graphClient.Teams[teamId].Channels[channelId].Request().GetAsync();
                var newChannel = new Channel();
                var channelDescription = channel.Description;
                if (channelDescription != null)
                {
                    channelDescription = channelDescription.Trim();
                    var tokenStr = "Token:\"";
                    if (channelDescription.Contains(tokenStr))
                    {
                        var tokenString = channelDescription.Substring(channelDescription.IndexOf(tokenStr));
                        var newTokenString = "Token:\"" + token + "\"";
                        channelDescription = channelDescription.Replace(tokenString, newTokenString);
                    }
                    else
                        channelDescription += ", Token:\"" + token + "\"";
                }
                newChannel.Description = channelDescription;
                try
                {
                    await graphClient.Teams[teamId].Channels[channelId].Request().UpdateAsync(newChannel);
                }
                catch (Exception ex)
                {
                    logger.Error("retrieveChannelMessagesSynchronized - SavingToken - Message: " + ex.Message);
                }
            }
            return totalMessages;
        }
        public static string getTokenFromChannelDescription(string description)
        {
            try
            {
                if (description != null || description?.Length > 0)
                {
                    var indexOfToken = description.IndexOf("Token:\"");
                    var indexOfLastDoubleQuote = description.IndexOf("\"", indexOfToken + 8);
                    if (indexOfToken != -1)
                        return description.Slice(indexOfToken + 7, indexOfLastDoubleQuote);
                }
                return "";
            }
            catch (Exception ex)
            {
                logger.Error("getTokenFromChannelDescription - Message: " + ex.Message);
                return "";
            }
        }
        private static async Task<IChannelMessagesCollectionPage> getMessagesReplies(GraphServiceClient graphClient, IChannelMessagesCollectionPage chatMessages, string teamId, string channelId)
        {
            foreach (var message in chatMessages)
            {

                var messagesReplies = await graphClient.Teams[teamId]
                                                          .Channels[channelId]
                                                          .Messages[message.Id]
                                                          .Replies.Request().GetAsync();
                message.Replies = new ChatMessageRepliesCollectionPage();
                while (messagesReplies.NextPageRequest != null || messagesReplies.Count > 0)
                {
                    foreach (var reply in messagesReplies)
                    {
                        message.Replies.Add(reply);
                    }
                    if (messagesReplies.NextPageRequest != null)
                        messagesReplies = await messagesReplies.NextPageRequest.GetAsync();
                    else
                        break;
                }
            }
            return chatMessages;
        }
        public static async Task<string> getChannelDescription(GraphServiceClient graphClient, string teamId, string channelId)
        {
            try
            {
                var channel = await graphClient.Teams[teamId].Channels[channelId].Request().GetAsync();
                if (channel != null)
                {
                    return channel.Description;
                }
            }
            catch (Exception ex)
            {
                logger.Error("getChannelDescription method - Message: " + ex.Message);
            }
            return null;
        }
        public static async Task<Dictionary<string,string>> fetchTeamIdByChannelId(GraphServiceClient graphClient, GraphServiceClient graphAppClient, string channelId, bool isExportConversations = false)
        {
            var teamIdThreaded = "";
            var teamNameThreaded = "";
            try
            {
                var app4LegalExtension = await checkIfOrgExtensionExist(graphAppClient);
                var teams = await graphClient.Groups.Request().GetAsync();
                var resetEvents = new List<AutoResetEvent>();
                if (app4LegalExtension != null)
                {
                    if (app4LegalExtension.AdditionalData != null)
                    {
                        foreach (var teamName in app4LegalExtension.AdditionalData.Keys)
                        {
                            var re = new AutoResetEvent(false);
                            resetEvents.Add(re);

                            ThreadPool.QueueUserWorkItem(async w =>
                            {
                                var threadReset = w as AutoResetEvent;
                                var random = new Random();
                                try
                                {
                                    if (teamName.Contains("App4Legal"))
                                    {
                                        var teamsId = app4LegalExtension.AdditionalData[teamName] as JArray;
                                        foreach (var team in teamsId)
                                        {
                                            foreach (var t in teams)
                                            {
                                                if (t.Id.Equals(team.ToString()))
                                                {
                                                    var channels = await graphClient.Teams[t.Id].Channels.Request().GetAsync();
                                                    foreach (var channel in channels)
                                                    {
                                                        if (channel.Id.Equals(channelId))
                                                        {
                                                            logger.Info("fetchTeamIdByChannelId - Channel found");
                                                            if (isExportConversations && teamName.Contains("Contract"))
                                                                teamIdThreaded = "notSupported";
                                                            else
                                                                teamIdThreaded = t.Id;
                                                            teamNameThreaded = teamName;
                                                            break;
                                                        }
                                                    }
                                                }
                                                if (teamIdThreaded.Length > 0) break;
                                            }
                                            if (teamIdThreaded.Length > 0) break;
                                        }
                                        Thread.Sleep(random.Next(100, 1000));
                                    }
                                }
                                catch (Exception ex)
                                {
                                    logger.Error("checkTeamsChannelsValidity method - outer catch - Message: " + ex.Message);
                                    //make sure you catch exceptions and release the lock.
                                    // otherwise you will get into deadlocks
                                    threadReset.Set();
                                }

                                threadReset.Set();
                            }, re);
                        }
                        foreach (AutoResetEvent resetEvent in resetEvents)
                        {
                            resetEvent.WaitOne();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("fetchTeamIdByChannelId - Message: " + ex.Message);
            }
            return new Dictionary<string, string>()
            {
                {"id", teamIdThreaded },
                {"name", teamNameThreaded }
            };
        }
        
        public static async Task<bool> addTabToChannel(GraphServiceClient graphClient, string teamId, string channelId, bool isContract = false)
        {
            Thread.Sleep(1000);
            var appCatalogApps = await graphClient.AppCatalogs.TeamsApps.Request().GetAsync();
            var teamsAppId = Globals.TeamsAppId;
            for (int i = 0; i < appCatalogApps.Count; i++)
            {
                var app = appCatalogApps[i];
                if (app.DisplayName.Equals("App4Legal"))
                {
                    teamsAppId = app.Id;
                    break;
                }
            }
            TeamsTab createdTab = null;
            try
            {
                var channelTab = new TeamsTab()
                {
                    DisplayName = "App4Legal",
                    Configuration = new TeamsTabConfiguration()
                    {
                        ODataType = null,
                        EntityId = null,
                        ContentUrl = Globals.RedirectUri + "Setup/ChannelTab" + (isContract ? "?contract=true" : ""),
                        WebsiteUrl = Globals.DomainUri,
                        RemoveUrl = null
                    },
                    ODataBind = "https://graph.microsoft.com/v1.0/appCatalogs/teamsApps/" + Globals.TeamsAppId
                };
                bool success = false;
                int attempts = 0;
                int maxAttempts = 10;
                while (!success && attempts <= maxAttempts)
                {
                    try
                    {
                        createdTab = await graphClient.Teams[teamId].Channels[channelId].Tabs.Request().AddAsync(channelTab);
                        success = true;
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("NotFound"))
                        {
                            logger.Error("addTabToChannel - Fail to create the tab for the " + (attempts + 1).ToString() + "time");
                            if (attempts >= maxAttempts)
                            {
                                logger.Error("addTabToChannel - Fail to create the tab for max attempts");
                                throw;
                            }
                            Thread.Sleep(5000);
                        }
                        else
                        {
                            logger.Error("addTabToChannel - Message: " + ex.Message);
                            success = true;
                        }
                    }
                    attempts++;
                }

                if (createdTab != null)
                {
                    //Send Channel Message
                    var app4LegalTabMessage = new ChatMessage
                    {
                        Body = new ItemBody
                        {
                            ContentType = BodyType.Html,
                            Content = "<div>Added a new tab \"App4Legal\" at the top of this channel.</div><attachment id=\"tab::" + createdTab.Id + "\"></attachment>",
                           
                        }
                    };

                    //string sendMessageRequest = ClaimConstants.graphApiBaseURL + "/teams/" + teamId + "/channels/" + channelId + "/messages";
                    //Dictionary<string, object> messageDic = new Dictionary<string, object>()
                    //    {
                    //        { "body", new Dictionary<string, string>(){
                    //            {"contentType", "html" },
                    //            {"content", "<div>Added a new tab \"App4Legal\" at the top of this channel.</div><attachment id=\"tab::" + createdTab.Id + "\"></attachment>" }
                    //        }},
                    //        {"attachments", new List<Dictionary<string,string>>()
                    //        {
                    //            new Dictionary<string, string>()
                    //            {
                    //                {"id", "tab::" + createdTab.Id },
                    //                {"contentType", "tabReference" },
                    //                {"name", "App4Legal" }
                    //            }
                    //        } }
                    //    };
                    //var dataToPost = JsonConvert.SerializeObject(messageDic);
                    //var responses = Web.sendGenericRequestToPostData(sendMessageRequest, dataToPost, HomeController.tempToken);
                    var createdMessage = await graphClient.Teams[teamId].Channels[channelId].Messages
                                                   .Request()
                                                   .AddAsync(app4LegalTabMessage);
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Error("addTabToChannel method - Message: " + ex.Message);
                if (ex.Message.Contains("Code: NotFound"))
                {
                    try
                    {
                        Thread.Sleep(10000);
                        //Send Channel Message
                        var app4LegalTabMessage = new ChatMessage
                        {
                            Body = new ItemBody
                            {
                                ContentType = BodyType.Html,
                                Content = "<p>App4Legal tab fails to be installed automatically, please try to install it manually by pressing + button at the top channel bar and choosing the App4Legal tab.</p>"
                            }
                        };
                        
                        await graphClient.Teams[teamId].Channels[channelId].Messages
                                                       .Request()
                                                       .AddAsync(app4LegalTabMessage);
                    }
                    catch (Exception exc)
                    {
                        logger.Error("addTabToChannel method - Sending channel message - Message: " + exc.Message);
                    }
                }
                if (ex.Message.Contains("Message: Value cannot be null"))
                {
                    try
                    {
                        Thread.Sleep(1000);
                        //Send Channel Message
                        var app4LegalTabMessage = new ChatMessage
                        {
                            Body = new ItemBody
                            {
                                ContentType = BodyType.Html,
                                Content = "<div>Added a new tab \"App4Legal\" at the top of this channel.</div>"
                            }
                        };
                        await graphClient.Teams[teamId].Channels[channelId].Messages
                                                       .Request()
                                                       .AddAsync(app4LegalTabMessage);
                    }
                    catch (Exception exc)
                    {
                        logger.Error("addTabToChannel method - Sending channel message - Message: " + exc.Message);
                    }
                }
            }
            return false;
        }
        public static async Task<bool> addMemberToTheChannel(GraphServiceClient graphClient, GraphServiceClient graphAppClient, ConversationMember member, string teamId, string channelId, addingMemberCallBack callBack)
        {
            //Add members to the channel                       
            try
            {
                var result = await graphAppClient.Teams[teamId].Channels[channelId]
                    .Members
                    .Request()
                    .AddAsync(member);
            }
            catch (Exception e)
            {
                if (e.Message.Contains("NotFound"))
                {
                    try
                    {
                        //Add user to the the team
                        var result = await graphClient.Teams[teamId].Members.Request().AddAsync(member);
                        if (result != null)
                        {
                            //Add user to the channel
                            var addMemberToTeamResult = await graphAppClient.Teams[teamId].Channels[channelId]
                                                            .Members
                                                            .Request()
                                                            .AddAsync(member);
                        }
                    }
                    catch (Exception e1)
                    {
                        if (e1.Message.Contains("Forbidden"))
                        {
                            callBack?.Invoke(true, member.AdditionalData["mail"].ToString(), e1);
                        }
                    }
                }
                else
                {
                    logger.Error("addMemberToTheChannel method - Message: " + e.Message);
                }
            }
            return true;
        }
        public static async Task<bool> inviteAndAddGuestToTheChannel(GraphServiceClient graphClient, GraphServiceClient graphAppClient, string teamId, string channelId, Invitation invitation, addingGuestCallBack callBack)
        {
            Invitation result = await graphClient.Invitations.Request().AddAsync(invitation);
            if (result != null)
            {
                //Create new member from invited user
                ConversationMember invitedMember = new AadUserConversationMember()
                {
                    Roles = new List<String>(){},
                    AdditionalData = new Dictionary<string, object>()
                    {
                      { "user@odata.bind",ClaimConstants.graphApiBaseURL + "/users('" + result.InvitedUser.Id +"')" }
                    }
                };
                try
                {
                    //Add invited user to the the team
                    var resultedUser = await graphClient.Teams[teamId].Members.Request().AddAsync(invitedMember);
                    if (resultedUser != null)
                    {
                        //Add user to the channel
                        var addMemberToTeamResult = await graphAppClient.Teams[teamId].Channels[channelId]
                                                        .Members
                                                        .Request()
                                                        .AddAsync(invitedMember);
                    }
                }
                catch (Exception e)
                {
                    if (e.Message.Contains("Forbidden"))
                    {
                        if (callBack != null)
                        {
                            callBack(true, e);
                        }
                    }
                    else
                    {
                        logger.Error("inviteAndAddGuestToTheChannel method - Message: " + e.Message);
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        public static async Task<List<DriveItem>> getChannelFiles(GraphServiceClient graphClient, string teamId, string channelName)
        {
            try
            {

                var orgName = await getOrganizationName(graphClient);
                var channelNameWithoutSpaces = channelName.Replace(" ", "");
                string checkGroupSites = ClaimConstants.graphApiBaseURLBETA + "/groups/" + teamId + "/sites/root";
                var responses = Web.sendGenericRequestToGETtData(checkGroupSites, HomeController.tempToken);
                var teamName = "";
                if (responses != null)
                {
                    var parsedResponse = JsonConvert.DeserializeObject<Dictionary<string, object>>(responses);
                    if (parsedResponse["name"] != null)
                        teamName = parsedResponse["name"].ToString();
                }
                if (teamName.Length > 0)
                {
                    var siteInfo = await graphClient.Sites[orgName + ".sharepoint.com:"].Sites[teamName + "-" + channelNameWithoutSpaces].Request().GetAsync();
                    var drives = await graphClient.Sites[siteInfo.Id].Drives.Request().GetAsync();
                    Microsoft.Graph.Drive drive = null;
                    foreach (var d in drives)
                    {
                        if (d.DriveType.Equals("documentLibrary"))
                        {
                            drive = d;
                            break;
                        }
                    }

                    var rootDrive = await graphClient.Drives[drive.Id].Root.Request().GetAsync();
                    var items = await graphClient.Drives[drive.Id].Items[rootDrive.Id].Children.Request().GetAsync();
                    var data = await graphClient.Drives[drive.Id].Items[items.CurrentPage[0].Id].Children.Request().GetAsync();
                    var listoffiles = await extractFilesFromChannelDrive(graphClient, data, drive.Id);
                    return listoffiles;
                }
            }
            catch (Exception ex)
            {
                logger.Error("getChannelFiles - Message: " + ex.Message);
            }
            return new List<DriveItem>();
        }
        public static string getExtensionId(dynamic extensions)
        {
            if (extensions.Count > 0)
            {
                foreach (var temp in extensions)
                {
                    if (temp.Id.Contains("app4legal"))
                    {
                        //Return extension ID
                        return temp.Id;
                    }
                }
            }
            return "";
        }
        public static async Task<IOrganizationExtensionsCollectionPage> getOrgExtensions(GraphServiceClient graphAppClient)
        {
            try
            {
                var tenantId = await getOrganizationId(graphAppClient);
                var extensions = await graphAppClient.Organization[tenantId].Extensions.Request().GetAsync();
                return extensions;
            }
            catch (Exception e)
            {
                logger.Error("getOrgExtensions method - Message: " + e.Message);
                return null;
            }
        }
        public static async Task<string> getOrganizationId(GraphServiceClient graphAppClient)
        {
            if (Startup.tenantId == "")
            {
                try
                {
                    //var graphClient = GraphService.GetAuthenticatedApplicationClient();
                    var org = await graphAppClient.Organization.Request().GetAsync();
                    if (org != null)
                    {
                        Startup.tenantId = org.CurrentPage[0].Id;
                        return org.CurrentPage[0].Id;
                    }
                }
                catch (Exception e)
                {
                    logger.Error("getOrganizationId method - Message: " + e.Message);
                }
            }
            return Startup.tenantId;
        }
        public static async Task<string> getOrganizationName(GraphServiceClient graphClient)
        {
            try
            {
                //var graphClient = GraphService.GetAuthenticatedApplicationClient();
                var org = await graphClient.Organization.Request().GetAsync();
                if (org != null)
                {
                    return org.CurrentPage[0].DisplayName;
                }
            }
            catch (Exception e)
            {
                logger.Error("getOrganizationName method - Message: " + e.Message);
            }
            return "";
        }
        public static async Task<string> getMyUserId(GraphServiceClient graphClient)
        {
            
            if (graphClient != null)
            {
                try
                {
                    var user = await graphClient.Me.Request().GetAsync();
                    if (user != null)
                    {
                        myId = user.Id;
                        myMail = user.Mail;
                        return myId;
                    }

                }
                catch (Exception e)
                {
                    logger.Error("getMyUserId method - Message: " + e.Message);
                }
            }
            return "";
        }
        public static async Task<string> getMyUserMail(GraphServiceClient graphClient)
        {
            if (graphClient != null)
            {
                try
                {
                    var user = await graphClient.Me.Request().GetAsync();
                    if (user != null)
                    {
                        myMail = user.Mail;
                        return myMail;
                    }

                }
                catch (Exception e)
                {
                    logger.Error("getMyUserMail method - Message: " + e.Message);
                }
            }
            return "";
        }
        public static async Task<bool> logOut(GraphServiceClient graphClient)
        {           
            //Clear the user extension data
            var userId = await getMyUserId(graphClient);
            var extensions = await graphClient.Users[userId].Extensions.Request().GetAsync();
            var extensionID = getExtensionId(extensions);
            try
            {
                if (extensionID.Length > 0)
                {
                    await graphClient.Users[userId].Extensions[extensionID].Request().DeleteAsync();
                    //Delete extension succeeded
                    HomeController.Stat = false;
                    LoginController.credentials = null;
                    return true;
                }                
            }
            catch (Exception e)
            {
                logger.Error("logOut method - Message: " + e.Message);
            }
            return false;
        }

        public static async Task<List<DriveItem>> extractFilesFromChannelDrive(GraphServiceClient graphClient, IDriveItemChildrenCollectionPage items, string driveId)
        {
            List<DriveItem> listOfFiles = new List<DriveItem>();
            foreach (var item in items)
            {
                if (item.Folder != null)
                {
                    // Is Folder
                    // Get all driveItems of this folder
                    var subItems = await graphClient.Drives[driveId].Items[item.Id].Children.Request().GetAsync();
                    if (subItems != null)
                    {
                        //Make recursion until getting all sub files
                        listOfFiles.AddRange(await extractFilesFromChannelDrive(graphClient, subItems, driveId));
                    }
                }
                if (item.File != null || item.Package != null)
                {
                    // Is File
                    listOfFiles.Add(item);
                }
            }
            return listOfFiles;
        }
        public static void checkOTALicence()
        {
            try
            {
                BackgroundWorker BkWorker = new BackgroundWorker();
                System.Timers.Timer t = new System.Timers.Timer();
                BkWorker = new BackgroundWorker();
                BkWorker.DoWork += (s, o) =>
                {
                    t.Stop();
                    checkLicenseRequest();
                };
                t = new System.Timers.Timer(5000);
                t.Elapsed += (s, o) =>
                {
                    if (!BkWorker.IsBusy)
                        BkWorker.RunWorkerAsync();
                };
                t.Start();
            }
            catch (Exception e)
            {
                logger.Error("checkOTALicence method - Message: " + e.Message);
            }
        }
        /// <summary>
        /// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// </summary>
        /// <param name="showAlert"></param>
        public static void checkLicenseRequest(bool showAlert = true)
        {
            HomeController.BlockUserAction = false;
            try
            {
                string app4legalURLinstance = LoginController.credentials.userUrl;
                string responseCheck = Web.getdata(app4legalURLinstance + @"modules/microsoft-teams/license/checkAvailability");
                dynamic dynResponseParse = JObject.Parse(responseCheck);
                string status = dynResponseParse["status"];
                string error = dynResponseParse["error"];
                if (!string.IsNullOrEmpty(status) && !string.IsNullOrWhiteSpace(status))
                {

                    if (status.Equals("not-valid"))
                    {
                        if (!string.IsNullOrEmpty(app4legalURLinstance) && app4legalURLinstance.Contains("app4legal.com"))  // Cloud user will be blocked from doing any action
                            HomeController.BlockUserAction = true;

                        HomeController.A4MErrorMsg = error;
                        HomeController.A4MLicenseMsg = status;
                        HomeController.isLicensed = false;

                    }
                    else if (status.Equals("expired"))
                    {
                        if (!string.IsNullOrEmpty(app4legalURLinstance) && app4legalURLinstance.Contains("app4legal.com"))  // Cloud user will be blocked from doing any action
                            HomeController.BlockUserAction = true;

                        if (!string.IsNullOrEmpty(LoginController.SelectedInstanceType) && LoginController.SelectedInstanceType == "0")
                            return;

                        HomeController.A4MErrorMsg = error;
                        HomeController.A4MLicenseMsg = status;
                        HomeController.isLicensed = false;
                    }
                    else if (status.Equals("valid"))
                    {
                        HomeController.BlockUserAction = false;
                        HomeController.A4MLicenseMsg = status;
                        HomeController.isLicensed = true;
                    }
                    else
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                HomeController.isLicensed = false;
                logger.Error("checkLicenceRequest method - Message: " + ex.Message);
            }
        }
        public static async Task<string> saveUserExtension(GraphServiceClient graphClient, string newApiKey)
        {
            if (newApiKey != null)
            {
                if (LoginController.credentials != null)
                {
                    LoginController.credentials.apiKey = newApiKey;
                }
                //Save api-key in user extension

                var userId = await getMyUserId(graphClient);
                var extensions = await graphClient.Users[userId].Extensions.Request().GetAsync();
                UserExtension updateEx = new UserExtension()
                {
                    Id = Globals.ExtensionName,
                    AdditionalData = new Dictionary<string, object>()
                    {
                        { "x-api-key", Globals.EncryptString(LoginController.getExtensionSecretKey(), newApiKey) },
                        { "url", Globals.EncryptString(LoginController.getExtensionSecretKey(), LoginController.credentials.userUrl) },
                        { "@odata.type", "#microsoft.graph.openTypeExtension"}
                    }
                };
                //Update the extension with latest api-key
                var extensionID = getExtensionId(extensions);
                try
                {
                    await graphClient.Users[userId].Extensions[extensionID].Request().UpdateAsync(updateEx);
                    return "success";
                }
                catch (Exception e)
                {
                    logger.Error("saveUserExtension Method - Message: " + e.Message);
                }
            }
            return "failed";
        }
    }
}
