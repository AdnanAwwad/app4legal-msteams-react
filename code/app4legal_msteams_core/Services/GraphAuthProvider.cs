﻿/* 
*  Copyright (c) Microsoft. All rights reserved. Licensed under the MIT license. 
*  See LICENSE in the source repository root for complete license information. 
*/

using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Identity.Client;
using Microsoft.Graph;
using app4legal_msteams_core.Extensions;
using NLog;
using Microsoft_Teams_Graph_RESTAPIs_Connect;
using System.Linq;
using app4legal_msteams_core.Controllers;

namespace app4legal_msteams_core.Services
{
    public class GraphAuthProvider : IGraphAuthProvider
    {
        private readonly IConfidentialClientApplication _app;
        private readonly string[] _scopes;
        private static string[] _staticscopes;
        private static NLog.Logger logger = LogManager.GetCurrentClassLogger();

        public GraphAuthProvider(IConfiguration configuration)
        {
            var azureOptions = new AzureAdOptions();
            configuration.Bind("AzureAd", azureOptions);

            // More info about MSAL Client Applications: https://github.com/AzureAD/microsoft-authentication-library-for-dotnet/wiki/Client-Applications
            _app = ConfidentialClientApplicationBuilder.Create(azureOptions.ClientId)
                    .WithClientSecret(azureOptions.ClientSecret)
                    .WithAuthority(AzureCloudInstance.AzurePublic, AadAuthorityAudience.AzureAdAndPersonalMicrosoftAccount)
                    .WithRedirectUri(azureOptions.BaseUrl + azureOptions.CallbackPath)
                    .Build();
            MsalAppBuilder.publicClient = _app;
            Authority = _app.Authority;

            _scopes = azureOptions.GraphScopes.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            _staticscopes = azureOptions.GraphScopes.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
        }
        public string Authority { get; }

        // Gets an access token. First tries to get the access token from the token cache.
        // Using password (secret) to authenticate. Production apps should use a certificate.
        public async Task<string> GetUserAccessTokenAsync(string userId = "")
        {
            IAccount account = null;
            var accs = await _app.GetAccountsAsync();
            if (Startup.accountIdentifier.Length > 0)
                account = await _app.GetAccountAsync(Startup.accountIdentifier);
            else
                account = (await _app.GetAccountsAsync()).FirstOrDefault();
            if (account == null)
            {
                HomeController.authorized = false;
                throw new ServiceException(new Error
                {
                    Code = "TokenNotFound",
                    Message = "User not found in token cache. Maybe the server was restarted."
                });
            }

            try
            {
                var result = await _app.AcquireTokenSilent(_scopes, account).ExecuteAsync();
                return result.AccessToken;
            }

            // Unable to retrieve the access token silently.
            catch (Exception)
            {
                HomeController.authorized = false;
                throw new ServiceException(new Error
                {
                    Code = GraphErrorCode.AuthenticationFailure.ToString(),
                    Message = "Caller needs to authenticate. Unable to retrieve the access token silently."
                });
            }
        }

        public async Task<AuthenticationResult> GetUserAccessTokenByAuthorizationCode(string authorizationCode)
        {
            try
            {
                return await _app.AcquireTokenByAuthorizationCode(_scopes, authorizationCode).ExecuteAsync();
            }
            catch(Exception ex)
            {
                logger.Error("GetUserAccessTokenByAuthorizationCode - Message :" + ex.Message);
                return null;
            }
        }
        // Get an access token. First tries to get the token from the token cache.
        public async Task<string> GetApplicationAccessTokenAsync()
        {
            IConfidentialClientApplication cA = MsalAppBuilder.publicApplication;
            string[] appScopes = new string[] { "https://graph.microsoft.com/.default" };
            try
            {
                if(cA == null)
                {
                    cA = MsalAppBuilder.BuildConfidentialClientApplicationForAppLevel(Startup.tenantId);
                    try
                    {
                        AuthenticationResult result = await cA.AcquireTokenForClient(appScopes).WithForceRefresh(true)
                                                        .ExecuteAsync();
                        return result.AccessToken;
                    }
                    catch (MsalUiRequiredException ex)
                    {
                        // The application doesn't have sufficient permissions.
                        // - Did you declare enough app permissions during app creation?
                        // - Did the tenant admin grant permissions to the application?
                        logger.Error("GraphAuthProvider - AuthorizationCodeReceived event - MsalUiRequiredException - Message :" + ex.Message);
                    }
                    catch (MsalServiceException ex) when (ex.Message.Contains("AADSTS70011"))
                    {
                        // Invalid scope. The scope has to be in the form "https://resourceurl/.default"
                        // Mitigation: Change the scope to be as expected.
                        logger.Error("GraphAuthProvider - AuthorizationCodeReceived event - MsalServiceException - Message :" + ex.Message);
                    }
                    HomeController.authorized = false;
                    return null;
                }
                else
                {
                    AuthenticationResult result = await cA.AcquireTokenForClient(appScopes)
                                                    .WithForceRefresh(true)
                                                    .ExecuteAsync();
                    return result.AccessToken;
                }
            }

            // Unable to retrieve the access token.
            catch (MsalUiRequiredException e)
            {
                HomeController.authorized = false;
                throw e;
            }
            catch (Exception e)
            {
                HomeController.authorized = false;
                logger.Warn("GraphAuthProvider - GetApplicationAccessTokenAsync Method - Message:" + e.Message);
                throw new Exception(e.Message);
            }
        }
        public static async Task<string> GetStaticUserAccessTokenAsync()
        {
            // string signedInUserID = ClaimsPrincipal.Current.FindFirst(ClaimTypes.NameIdentifier).Value;
            IConfidentialClientApplication cc = MsalAppBuilder.publicClient;
            try
            {
                if (cc != null)
                {
                    var accounts = await cc.GetAccountsAsync();
                    AuthenticationResult result = await cc.AcquireTokenSilent(_staticscopes, accounts.FirstOrDefault()).ExecuteAsync();
                    HomeController.tempToken = result.AccessToken;
                    return result.AccessToken;
                }
                else
                {
                    HomeController.authorized = false;
                    logger.Warn("AuthProvider - GetUserAccessTokenAsync Method - instance is null.");
                    return null;
                }

            }

            // Unable to retrieve the access token silently.
            catch (MsalUiRequiredException e)
            {
                HomeController.authorized = false;
                throw e;
            }
            catch (Exception e)
            {
                HomeController.authorized = false;
                logger.Warn("AuthProvider - GetUserAccessTokenAsync Method - Message:" + e.Message);
                throw new Exception(e.Message);
            }
        }
    }

    public interface IGraphAuthProvider
    {
        string Authority { get; }

        Task<string> GetUserAccessTokenAsync(string userId = "");
        Task<string> GetApplicationAccessTokenAsync();

        Task<AuthenticationResult> GetUserAccessTokenByAuthorizationCode(string authorizationCode);
    }
}
