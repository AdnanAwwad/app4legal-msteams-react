﻿/* 
*  Copyright (c) Microsoft. All rights reserved. Licensed under the MIT license. 
*  See LICENSE in the source repository root for complete license information. 
*/

using app4legal_msteams_core.Controllers;
using Microsoft.Graph;
using System.Net.Http.Headers;
using System.Security.Claims;

namespace app4legal_msteams_core.Services
{
    public class GraphServiceClientFactory : IGraphServiceClientFactory
    {
        private readonly IGraphAuthProvider _authProvider;

        public GraphServiceClientFactory(IGraphAuthProvider authProvider)
        {
            _authProvider = authProvider;
        }
        public static GraphServiceClient GetStaticAuthenticatedClient()
        {
            return new GraphServiceClient(new DelegateAuthenticationProvider(
                 async (requestMessage) =>
                {                    // Append the access token to the request
                    string accessToken = await GraphAuthProvider.GetStaticUserAccessTokenAsync();
                    //string accessToken = HomeController.tempToken;
                    if (accessToken != null)
                    {
                        requestMessage.Headers.Authorization = new AuthenticationHeaderValue("bearer", accessToken);
                    }
                }));
        }
        public GraphServiceClient GetAuthenticatedGraphClient(ClaimsIdentity userIdentity = null) =>
            new GraphServiceClient(new DelegateAuthenticationProvider(
                async requestMessage =>
                {
                    // Get user's id for token cache.
                    var identifier = userIdentity?.FindFirst(Startup.ObjectIdentifierType)?.Value + "." + userIdentity?.FindFirst(Startup.TenantIdType)?.Value;

                    // Passing tenant ID to the sample auth provider to use as a cache key
                    var accessToken = await _authProvider.GetUserAccessTokenAsync(identifier);

                    //Save token to HomeController tempToken variable
                    HomeController.tempToken = accessToken;

                    // Append the access token to the request
                    requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                }));
        public GraphServiceClient GetAuthenticatedApplicationGraphClient()
        {
            bool isValid = true;
            DelegateAuthenticationProvider provider = new DelegateAuthenticationProvider(
                async (requestMessage) =>
                {
                    string accessToken = await _authProvider.GetApplicationAccessTokenAsync();
                    if (accessToken != null)
                    {
                        requestMessage.Headers.Authorization = new AuthenticationHeaderValue("bearer", accessToken);
                    }
                    else
                    {
                        isValid = false;
                    }
                });

            GraphServiceClient graphClient = new GraphServiceClient(provider);
            if (isValid)
                return graphClient;
            else
            {
                return null;
            }
            //return GetAuthenticatedGraphClient();
        }
    }

    public interface IGraphServiceClientFactory
    {
        GraphServiceClient GetAuthenticatedGraphClient(ClaimsIdentity userIdentity = null);
        GraphServiceClient GetAuthenticatedApplicationGraphClient();
    }
}
