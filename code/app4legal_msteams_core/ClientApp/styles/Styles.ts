﻿
export default class Styles {

     public static  layoutStyle = {
         "background-color": "inherit",
         'width': '100%',
    };
     public static  parentStyle = {
        'width': '100%',
        'height': '100%',
        "background-color": "transparent"
    };
     public static  imageStyle = {
        'width': '350px',
        'height': '75px',
    };
    public static topImageStyle = {
        'display': 'block',
        'max- width': '100px',
        'max-height': '50px',
        'width': 'auto',
        'height': 'auto'
    };
}