﻿import React = require('react');
import ReactDOM = require('react-dom');
import { Button, Provider, teamsTheme, Text, Image, Layout, Flex, Segment, Loader, Card, CardHeader, CardBody, Header, Input, SearchIcon, CardFooter, OpenOutsideIcon, Tooltip, Dialog, LinkIcon, ArrowRightIcon, List, MegaphoneIcon, Label, InfoIcon, ParticipantAddIcon, AttendeeIcon, TenantPersonalIcon, Checkbox, TeamsMonochromeIcon, Grid, CloseIcon, WorkOrSchoolIcon, SettingsIcon, Divider } from '@fluentui/react-northstar';
import * as msTeams from '@microsoft/teams-js';
import $ = require('jquery');
import { CaseType } from './helpers/Constants';
import * as Functions from './helpers/Functions';
import Styles from '../styles/Styles';
import SearchBox from './components/SearchBox';
import axios from 'axios';

class SettingsComponent extends React.Component<ISettingsProps, ISettingsState> {
    constructor(props) {
        super(props);        
        this.state = {
            isLoading: true, loaderLabel: '', openDialog: false, preferencesStatus: [{}], userInfo: { name: 'Loading...', image: 'Images/avatar.svg' }
        };
        this.handlePreferencesChanged = this.handlePreferencesChanged.bind(this);
    }
    componentDidMount() {        
        this.getUserInformation();
        this.getUserPreferences();
    }
    getUserInformation() {
        let url = "/setup/GetUserInformation";
        axios.get(url)
            .then(response => {
                var data = response.data;
                if (data != null) {
                    this.setState({ userInfo: { name: data.name, image: data.image} });
                }
            })
            .catch(function (error) {
            });
    }
    getUserPreferences() {
        let url = "/setup/getPreferencesStatus";
        axios.get(url)
            .then(response => {
                var data = response.data;
                if (data != null) {
                    var preferences = [];
                    for (const [key, value] of Object.entries(data)) {
                        preferences.push({ type: key, value: value, loading: false } )
                    }
                    this.setState({ isLoading: false, preferencesStatus: preferences });
                }
            })
            .catch(function (error) {
                this.setState({ isLoading: true, preferencesStatus:[] });
            });
    }
    handlePreferencesChanged(props, index, key) {
        var preferences = this.state.preferencesStatus;
        preferences[index].loading = true;
        this.setState({ preferencesStatus: preferences });
        switch (key) {
            case 'archiveFiles':
                {
                }; break;
        }
        var url = '/Setup/UpdatePreferencesStatus';
        let config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            }
        }
        let params = new URLSearchParams();
        params.append('key', key);
        params.append('value', String(props.checked));
        axios.post(url, params, config).then((resp) => {
            var preferences = this.state.preferencesStatus;
            preferences[index].loading = (resp.data.code != 'Success');
            preferences[index].value = !preferences[index].value;
            this.setState({ preferencesStatus: preferences });
        });        
    }
    render() {
        const preferences = [{
            type: 'archiveFiles',
            content: 'Archive channel files upon channel automatic deletion'
        }];
        return (
            <Flex column gap='gap.medium' styles={{width: '100%'}}>
                <Segment color='brand'
                    content={<Grid columns='repeat(4, 1fr)' rows='50px 150px'>
                        <Flex inline hAlign='start' vAlign='start' gap='gap.medium' styles={{ gridColumn: 'span 4' }}>
                            <WorkOrSchoolIcon size='larger' styles={({ theme: { siteVariables } }) => ({
                                color: siteVariables.colorScheme.brand.foreground,
                            })} />
                            <Header content='Personal Information' styles={{ marginTop: '-5px' }}/>
                            <Flex.Item push>
                                <Button secondary icon={<OpenOutsideIcon />} text content='Open DashBoard' onClick={
                                    (e) => {
                                        e.preventDefault();
                                        window.open(this.props.url);
                                    }
                                } title='Open in App4Legal' />
                            </Flex.Item>
                        </Flex>
                        <Flex inline hAlign='center' vAlign='center' styles={{ gridColumn: 'span 1' }}>
                            <Image src={this.state.userInfo.image} styles={{ width: '100px', height: '100px' }} circular />
                            <div
                                style={{
                                    display: 'flex',
                                    justifyContent: 'center',
                                    height: '80%',
                                    alignItems: 'center',
                                    marginLeft: '50px'
                                }}
                            >
                                <Divider vertical />
                            </div>
                        </Flex>
                        
                        <Flex column vAlign='center' gap='gap.medium' styles={{ gridColumn: 'span 3' }}>
                            <Flex inline hAlign='start' vAlign='start' gap='gap.medium'>
                                <Text content='Name:' />
                                <Text weight='semibold' content={this.state.userInfo.name}/>
                            </Flex>
                            <Flex inline hAlign='start' vAlign='start' gap='gap.medium'>
                                <Text content='Url instance:' />
                                <Text weight='semibold' content={this.props.url} />
                            </Flex>                            
                        </Flex>
                    </Grid>}
                />
                <Segment color='brand'
                    content={<Grid columns='repeat(4, 1fr)' rows={'50px ' + this.state.preferencesStatus.length*50 + 'px'}>
                        <Flex inline gap='gap.medium' styles={{ gridColumn: 'span 4' }}>
                            <SettingsIcon size='larger' styles={({ theme: { siteVariables } }) => ({
                                color: siteVariables.colorScheme.brand.foreground,
                            })} />
                            <Header content='Preferences' styles={{ marginTop: '-5px' }} />
                        </Flex>
                        <Flex column hAlign='start' vAlign='start' styles={{ gridColumn: 'span 4' }}>
                            {   this.state.preferencesStatus.length > 0?
                                this.state.preferencesStatus.map((item, index) => {
                                    return <Checkbox disabled={item.loading}
                                        key={Math.floor(Math.random() * 1000000000)}
                                        label={preferences[index].content}
                                        checked={item.value}
                                        onChange={(e, props) => { this.handlePreferencesChanged(props, index, item.type) }}
                                        styles={{ marginLeft: '20px' }} />;
                            }):<div/>}
                        </Flex>
                        
                    </Grid>}
                />
            </Flex>
            );
    }
}
export default SettingsComponent;
interface ISettingsState {
    isLoading: boolean;
    loaderLabel: string;
    openDialog: boolean;
    preferencesStatus: any;
    userInfo: any;
}
interface ISettingsProps {
    url: string;
    apiKey: string;
    redirectPage: string;
    executeDeepLink(link);
}