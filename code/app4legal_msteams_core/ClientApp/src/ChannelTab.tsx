﻿import React = require('react');
import ReactDOM = require('react-dom');
import { Button, Provider, teamsTheme, Text, Image, Layout, Flex, Segment, Loader, Card, CardHeader, CardBody, Header, Input, SearchIcon, CardFooter, OpenOutsideIcon, Tooltip, Dialog, LinkIcon, ArrowRightIcon, List, MegaphoneIcon, Label, InfoIcon, ParticipantAddIcon, AttendeeIcon, TenantPersonalIcon, Checkbox, TeamsMonochromeIcon, Grid, CloseIcon, WorkOrSchoolIcon, SettingsIcon, Divider, Form, Table, gridCellMultipleFocusableBehavior } from '@fluentui/react-northstar';
import * as msTeams from '@microsoft/teams-js';
import $ = require('jquery');
import { CaseType } from './helpers/Constants';
import * as Functions from './helpers/Functions';
import Styles from '../styles/Styles';
import axios from 'axios';
var noteTitle = '';
var fullOrSync = 'full';
var teamId;
var channelId;
var channelName;
var dialogMessage;
var channelFiles = [];
class ChannelTabComponent extends React.Component<ITabProps, ITabState> {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true, loaderLabel: '', openDialog: false, type: '', caseInfo: { type: '', id: '' }, caseData: {},
            validChannel: true, isSendingConversations: false, isLoadingFiles: true, didAnyFileSelected: false, isSendingFiles: false
        }
        this.handleErrorResponse = this.handleErrorResponse.bind(this);
        this.handleSubmitExpConversation = this.handleSubmitExpConversation.bind(this);
        this.handleFilesSelectionChanged = this.handleFilesSelectionChanged.bind(this);
        this.handleSubmitExpFiles = this.handleSubmitExpFiles.bind(this);
    }
    componentDidMount() {
        msTeams.getContext(context => {
            teamId = context.teamId;
            channelId = context.channelId;
            channelName = context.channelName;
            this.retrieveCaseIdFromChannelId(context.channelId, context.channelName);
            this.getChannelFiles();
        })
    }
    handleSubmitExpFiles() {
        this.setState({ isSendingFiles: true });
        var self = this;
        var filesId = [];
        channelFiles.forEach(file => {
            if (file.checked)
                filesId.push(file.id);
        });
        console.log(filesId);
        var url = "Setup/ExportChannelFiles";
        var model = { filesID: filesId, isContract: this.state.caseInfo.type == CaseType.contract };
        $.post(url, model, function (response) {
            if (response == "Error")
                dialogMessage = "Something went wrong, please try again later";
            else {
                dialogMessage = response;
            }
            self.setState({ isSendingFiles: false, openDialog: true });
        });
    }
    handleSubmitExpConversation(e, props) {
        var self = this;
        this.setState({ isSendingConversations: true });
        let url = '/Setup/ExportChannelMessages';
        var model = { teamId: teamId, channelId: channelId, channelName: channelName, noteTitle: noteTitle, isFullConversation: (fullOrSync == 'full'), isContract: (this.state.caseInfo.type == CaseType.contract) };
        $.post(url, model, function (response) {
            switch (response) {
                case "NotFound": {
                    dialogMessage = "Something went wrong, please try again later!";
                } break;
                case "Updated": {
                    dialogMessage = "No new messages found!";
                } break;
                case "NotSupported": {
                    dialogMessage = "Notes for this type not supported yet!";
                } break;
                default:{
                    dialogMessage = "Channel conversation exported successfully";
                }
            }
            self.setState({ openDialog: true, isSendingConversations: false });
        });
    }
    handleFilesSelectionChanged(e, props) {
        try {
            console.log(props);
            var items = channelFiles;
            console.log(items);                                              
            var anyFileSelected = false;
            for (let i = 0; i < items.length; i++) {
                if (props.label.trim() == items[i].items[0].props.children[1].props.label.trim()) {
                    items[i].checked = props.checked;
                }
                if (items[i].checked) {
                    anyFileSelected = true;
                }
            }
            channelFiles = items;
            this.setState({ didAnyFileSelected: anyFileSelected });
        }
        catch (err) {
            console.log(err);
        }
    }
    async handleErrorResponse(error, callBack) {
        console.log(error);
        if (error.code == "refresh_key_required") {
            var responseDataObj = await Functions.refreshToken(this.props.url, this.props.apiKey);
            var errorRefresh = responseDataObj.error;
            var successRefresh = responseDataObj.success;
            if (errorRefresh == "") {
                var newApiKey = successRefresh.data.key;
                let params = new URLSearchParams();
                params.append('apiKey', newApiKey);
                let url = "/login/RefreshApiKey";
                let config = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                    }
                }
                axios.post(url, params, config).
                    then((result) => {
                        if (result) {
                            if (result.data == "failed") {

                            }
                            else {
                                if (callBack != null)
                                    callBack;
                            }
                            return;
                        }
                    });
            }
        } else if (error.code == "unauthorized") {
            let url = "/login/Unauthorized";
            let from = this.props.redirectPage;  
            let config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                }
            }
            let params = new URLSearchParams();
            params.append('from', from);
            axios.post(url, params, config)
                .then((result) => {
                    if (result.data) {
                        let url = "/login/SignIn";
                        window.location.href = url;
                        return;
                    }
                    else {
                        return;
                    }
                });
        }
    }
    retrieveCaseIdFromChannelId(channelId, channelName) {
        var url = '/Setup/RetriveCaseIdFromChannelId';
        let config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            }
        }
        const headers = {
            'x-api-key': this.props.apiKey,
            'Content-Type': 'application/x-www-form-urlencoded',
            'x-api-channel': 'microsoft-teams'
        };
        let params = new URLSearchParams();
        params.append('channelId', channelId);
        params.append('channelName', channelName);
        axios.post(url, params, config).then((resp) => {
            try {
                var type = resp.data.teamName.split('-')[1];
                this.setState({ caseInfo: { type: type, id: resp.data.caseId }, isLoading: false, validChannel: String(resp.data.caseId).length > 0 });
                let caseUrl = this.props.url;
                var caseData;
                switch (type) {
                    case CaseType.litigation: case CaseType.matter: {
                        caseUrl += '/modules/api/cases/edit/' + resp.data.caseId;
                        axios.get(caseUrl, { headers: headers })
                            .then(response => {
                                var resp = response.data;
                                var error = resp.error;
                                var success = resp.success;
                                if (error) {
                                    this.handleErrorResponse(error, null);
                                }
                                if (success) {
                                    var caseValues = success.data.caseValues;
                                    caseData = {
                                        'Id': caseValues.id,
                                        'Subject': caseValues.subject,
                                        'Priority': caseValues.priority,
                                        'Creation Date': caseValues.createdOn,
                                        'Due Date': caseValues.dueDate,
                                        'Assignee': caseValues.assignedToFullName,
                                        'Requested By': caseValues.requestedByName,
                                        'Referred By': caseValues.referredByName,
                                        'Client Name': caseValues.clientName,
                                        'Client Id': caseValues.client_id,
                                        'Client Type': caseValues.clientType,
                                    }
                                    this.setState({ caseData: caseData });
                                }
                            });
                    } break;
                    case CaseType.hearing: {
                        caseUrl += '/modules/api/hearings/edit/' + resp.data.caseId;
                        axios.get(caseUrl, { headers: headers })
                            .then(response => {
                                var resp = response.data;
                                var error = resp.error;
                                var success = resp.success;
                                if (error) {
                                    this.handleErrorResponse(error, null);
                                }
                                if (success) {
                                    var caseValues = success.data.hearingValues;
                                    caseData = {
                                        'Summary': caseValues.summary,
                                        'Type': caseValues.type.name,
                                        'Legal Case Id': caseValues.legal_case_id,
                                        'Start Date': caseValues.startDate,
                                        'Start Time': caseValues.startTime,
                                        'Postponed Date': caseValues.postponedDate,
                                        'PostPoned Time': caseValues.postponedTime,
                                        'Reason Of Postponement': caseValues.reasons_of_postponement
                                    }
                                    this.setState({ caseData: caseData });
                                }
                            });
                    } break;
                    case CaseType.contract: {
                        caseUrl += '/modules/api/contracts/edit/' + resp.data.caseId;
                        axios.get(caseUrl, { headers: headers })
                            .then(response => {
                                var resp = response.data;
                                var error = resp.error;
                                var success = resp.success;
                                if (error) {
                                    this.handleErrorResponse(error, null);
                                }
                                if (success) {
                                    var caseValues = success.data.contract;
                                    var contributorsArray = caseValues.contributors;
                                    var collaboratorsArray = caseValues.collaborators;
                                    var partiesArray = caseValues.parties;
                                    var contributors = '';
                                    contributorsArray.forEach((item, index) => {
                                        contributors = contributors.concat(item.name.trim(),(index == contributorsArray.length - 1) ? '' : ', ');
                                    });
                                    var collaborators = '';
                                    collaboratorsArray.forEach((item, index) => {
                                        collaborators = collaborators.concat(item.name.trim(), (index == collaboratorsArray.length - 1) ? '' : ', ');
                                    })
                                    var parties = '';
                                    partiesArray.forEach((item, index) => {
                                        parties = parties.concat(item.party_name.trim(), '[', item.party_member_type, ']', (index == partiesArray.length - 1) ? '' : ', ');
                                    })
                                    caseData = {
                                        'Name': caseValues.name,
                                        'Summary': caseValues.summary,
                                        'Type': caseValues.type,
                                        'Priority': caseValues.priority,
                                        'Currency': caseValues.currency,
                                        'Assigned Team': caseValues.assigned_team,
                                        'Assignee': caseValues.assignee,
                                        'Parties': parties,
                                        'Collaborators': collaborators,
                                        'Contributors': contributors,
                                        'Reference Number': caseValues.reference_number,
                                        'Value': caseValues.value,
                                        'Status': caseValues.status
                                    }
                                    this.setState({ caseData: caseData });
                                }
                            });
                    } break;
                }
            }
            catch (err) {
                this.setState({ caseInfo: { type: '', id: '' } });
            }
        });        
    }
    getChannelFiles() {
        var filesUrl = '/Setup/GetChannelFiles';
        var model = { teamId: teamId, channelId: channelId, channelName: channelName };
        var self = this;
        $.post(filesUrl, model, function (response) {
            try {
                channelFiles = [];
                var parsedResponse = JSON.parse(response);
                parsedResponse.forEach((item, index) => {
                    var imgSource = "";
                    var mimeType = "";
                    if (item.file != null)
                        mimeType = item.file.mimeType;
                    else {
                        if (item.package != null)
                            mimeType = item.package.type;
                    }
                    if (mimeType.length > 0) {
                        if (mimeType.includes("image/"))
                            imgSource = "/Images/picture.svg";
                        else {
                            switch (mimeType) {
                                case "text/html": imgSource = "Images/html.svg"; break;
                                case "application/pdf": imgSource = "Images/pdf.svg"; break;
                                case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                                    imgSource = "Images/word.svg"; break;
                                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                                    imgSource = "Images/excel.svg"; break;
                                case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
                                    imgSource = "Images/powerpoint.svg"; break;
                                case "oneNote":
                                    imgSource = "Images/onenote.svg"; break;
                                default:
                                    imgSource = "Images/blank.svg"; break;
                            }
                        }
                    }
                    channelFiles.push({
                        key: index,
                        items: [
                            <Flex inline>
                                <Image src={imgSource} styles={{ width: '20px', height: '20px', paddingTop: '4px' }} />
                                <Checkbox label={item.name} disabled={(parseFloat((((item.size) / 1024) / 1024).toFixed(1))) > 49.9} onChange={self.handleFilesSelectionChanged} />
                            </Flex>,
                            item.size
                        ],
                        id: item.id,
                        checked: false
                    });
                });
            }
            catch (err) {
            }
            self.setState({ isLoadingFiles: false });
        });
    }
    render() {
        const items = [
            {
                name: 'full',
                key: 'Full',
                label: 'All Conversations',
                value: 'full',
            },
            {
                name: 'sync',
                key: 'Sync',
                label: 'Newest Conversations',
                value: 'sync',
            },
        ];
        const divStyle = {
            'overflow': 'auto',
            'maxHeight': '200px',
            'width': '100%'
        }
        return (           
                this.state.isLoading ? <Loader /> :
                    (this.state.validChannel ?
                    <Flex column gap='gap.medium' styles={{ width: '100%' }}>
                    <Segment color='brand'
                        content={<Grid columns='repeat(4, 1fr)' rows={'50px ' + Object.keys(this.state.caseData).length*30 + 'px'}>
                            <Flex inline hAlign='center' vAlign='center' gap='gap.medium' styles={{ gridColumn: 'span 4' }}>
                                <Image src={'Images/' + Functions.getCaseDetailsImage(this.state.caseInfo.type) + '.png'} styles={{ width: '40px', height: '40px' }} />
                                <Header content={this.state.caseInfo.type + ' details'} />
                                <Flex.Item push>
                                    <Tooltip content="Open in App4Legal" trigger={<Button icon={<OpenOutsideIcon />} iconOnly onClick={
                                        (e) => {
                                            e.preventDefault();
                                            var caseUrl = this.props.url;
                                            switch (this.state.caseInfo.type) {
                                                case CaseType.litigation: case CaseType.matter: case CaseType.litigation: {
                                                    caseUrl += '/cases/edit/' + this.state.caseInfo.id;
                                                } break;
                                                case CaseType.contract: {
                                                    caseUrl += '/modules/contract/contracts/view/' + this.state.caseInfo.id;
                                                } break;
                                            }
                                            window.open(caseUrl);
                                        }
                                    }/>}/>
                                </Flex.Item>
                            </Flex>
                            <Flex column hAlign='start' vAlign='center' gap='gap.small' styles={{ gridColumn: 'span 2', marginLeft: '30px' }}>
                                {
                                    Object.entries(this.state.caseData).map(([k, v], i) => {
                                        return (
                                            <Flex key={Math.floor(Math.random() * 1000000000)}
                                                inline hAlign='start' vAlign='start' gap='gap.medium'>
                                                <Text content={k + ':'} />
                                                <Text weight='semibold' content={(v == null || String(v).length == 0) ? '-' : v} />
                                            </Flex>)
                                    })
                                }
                            </Flex>

                            <Flex column vAlign='center' gap='gap.medium' styles={{ gridColumn: 'span 2' }}>
                                {Object.keys(this.state.caseData).length == 0 ? <div /> : <Image src='Images/cloudexport.svg' alt='Export channel conversations and files to the App4Legal core' styles={{width: '80%', height: '80%'}}/>}
                            </Flex>
                        </Grid>}
                    />
                <Flex inline hAlign='center' vAlign='center' gap='gap.medium'>
                    <Flex.Item size='size.half'>
                        <Segment color='brand'
                            content={<Grid columns='repeat(4, 1fr)' rows={'50px 230px'}>
                                <Flex inline gap='gap.medium' styles={{ gridColumn: 'span 4' }}>
                                    <SettingsIcon size='larger' styles={({ theme: { siteVariables } }) => ({
                                        color: siteVariables.colorScheme.brand.foreground,
                                    })} />
                                    <Header content='Export Conversations' styles={{ marginTop: '-5px' }} />
                                    <Flex.Item push>
                                        <Tooltip content='Export channel conversation as Note to the related case' trigger={<Button icon={<InfoIcon />} iconOnly />} />
                                    </Flex.Item>
                                </Flex>
                                <Flex column hAlign='center' vAlign='center' styles={{ gridColumn: 'span 4', marginTop: '15px' }}>
                                    <Form onSubmit={this.handleSubmitExpConversation}>
                                        <Form.Input fluid label='Note title' name='noteTitle' required
                                            onChange={(e, props) => {
                                                noteTitle = props.value;
                                            }}
                                        />
                                        <Form.RadioGroup label='Exportation method' vertical
                                            defaultCheckedValue='full' items={items}
                                            onCheckedValueChange={(e, props) => {
                                                fullOrSync = props.name;
                                            }
                                            }
                                        />
                                        {this.state.isSendingConversations ? <Loader /> :
                                            <Form.Button primary disabled={this.state.isLoading || (this.state.caseInfo.type == CaseType.contract)} fluid content='Submit' />
                                        }
                                    </Form>
                                </Flex>

                            </Grid>}
                                />
                    </Flex.Item>
                    <Flex.Item size='size.half'>
                        <Segment color='brand'
                            content={<Grid columns='repeat(4, 1fr)' rows={'50px 230px'}>
                                <Flex inline gap='gap.medium' styles={{ gridColumn: 'span 4' }}>
                                    <SettingsIcon size='larger' styles={({ theme: { siteVariables } }) => ({
                                        color: siteVariables.colorScheme.brand.foreground,
                                    })} />
                                    <Header content='Export Files' styles={{ marginTop: '-5px' }} />
                                    <Flex.Item push>
                                        <Tooltip content='Export channel files as Attachments to the related case' trigger={<Button icon={<InfoIcon />} iconOnly />} />
                                    </Flex.Item>
                                </Flex>
                                <Flex column hAlign='center' vAlign='start' styles={{ gridColumn: 'span 4' }}>
                                    <div style={divStyle}>
                                        {this.state.isLoadingFiles ? <Loader /> :
                                            <Flex column hAlign='center' vAlign='center'>
                                                {channelFiles.length > 0 ?
                                                    <Table styles={{ width: '100%' }} accessibility={gridCellMultipleFocusableBehavior} header={{ items: ['Name', 'Size'] }} rows={
                                                        channelFiles
                                                    } /> :
                                                    <Text content='No files in the channel' />}
                                            </Flex>
                                        }
                                    </div>
                                    <Flex.Item push>
                                        {this.state.isSendingFiles ? <Loader /> :
                                            <Button primary fluid content='Export Files' disabled={!this.state.didAnyFileSelected} onClick={this.handleSubmitExpFiles} />
                                        }
                                    </Flex.Item>
                                </Flex>
                            </Grid>}
                        />
                    </Flex.Item>
                </Flex>
                <Dialog
                    open={this.state.openDialog}
                    header='Information'
                    content={dialogMessage}
                    confirmButton={null}
                    cancelButton="Cancel"
                    onCancel={() => {
                        this.setState({ openDialog: false })
                    }}
                            />
                        </Flex> : <Header color='red' content='This channel is not related to any of App4Legal case, please choose a channel created by App4Legal app' />)
            
        );
    }
}
export default ChannelTabComponent;
interface ITabState {
    isLoading: boolean;
    loaderLabel: string;
    openDialog: boolean;
    type: string;
    caseInfo: any;
    caseData: any;
    validChannel: boolean;
    isSendingConversations: boolean;
    isSendingFiles: boolean;
    isLoadingFiles: boolean;
    didAnyFileSelected: boolean;
}
interface ITabProps {
    url: string;
    apiKey: string;
    redirectPage: string;
    executeDeepLink(link);
}