﻿import React = require('react');
import ReactDOM = require('react-dom');
import { Button, Provider, teamsTheme, ThemePrepared, Loader, Flex, Image, Segment, Divider, Avatar, BreakoutRoomIcon, Text } from '@fluentui/react-northstar';
import TeamsThemeHelper from './helpers/TeamsThemeHelper'
import AuthHelper from './helpers/Auth-Helper'
import * as msTeams from '@microsoft/teams-js';
import SignIn from './SignIn'
import $ = require('jquery')
import Styles from '../styles/Styles';
import SearchCase from './SearchIndex';
import * as Constants from './helpers/Constants';
import CreateChannelComponent from './CreateChannel';
import axios from 'axios';
import SettingsComponent from './Settings';
import ChannelTabComponent from './ChannelTab';
class App extends React.Component<{}, IAppState> {
    constructor(props: IAppProps) {
        super(props);
        this.handleLogOut = this.handleLogOut.bind(this);
        this.state = {
            theme: TeamsThemeHelper.getTheme('default'),
            loggedIn: true,
            isLoading: true,
            redirectedPage: "",
            isLoadingChildPage: true,
            userInfo: {name: 'Name', image:'Images/avatar.svg', status: 'Offline', color: 'grey'}
        };

        msTeams.initialize();
        msTeams.registerOnThemeChangeHandler(this.updateTheme.bind(this));
        msTeams.getContext(context => { this.updateTheme(context.theme) });
    }
    componentDidMount() {
        this.checkUserIfLoggedIn();
        this.redirectToPage();
    }
    handleLogOut() {
        var url = "/setup/Logout";
        let signUrl = "/login/SignIn";
        axios.get(url)
            .then(response => {
                window.location.href = signUrl;
            })
            .catch(error => {
                console.log(error);
                window.location.href = signUrl;
            });       
    }
    getUserInformation() {
        let url = "/setup/GetUserInformation";
        axios.get(url)
            .then(response => {
                var data = response.data;
                if (data != null) {
                    this.setState({ userInfo: { name: data.name, image: data.image, status:'Available', color: 'green' } });
                }
            })
            .catch(function (error) {
            });
    }
    redirectToPage()
    {
        msTeams.getContext(context => {
            var mail = context.upn;
            let url = "/home/GetLoggedInMail";
            var self = this;
            axios.get(url)
                .then(response => 
                {
                    if (response != null) {
                        var loggedMail = response.data;
                        console.log(loggedMail)
                        if (loggedMail.length > 0 && mail.length > 0) {
                            if (loggedMail.localeCompare(mail) != 0) {
                                let url = window.location.origin + "/login/MsUserChanged";
                                let config = {
                                    headers: {
                                        'Content-Type': 'application/x-www-form-urlencoded',
                                    }
                                }
                                let params = new URLSearchParams();
                                params.append('teamsMail', mail);
                                params.append('serverMail', loggedMail);
                                axios.post(url, params, config)
                                    .then(response => {
                                        self.setState({ loggedIn: false, isLoading: false })
                                    });
                                return;
                            }                                                    
                            let url = "/home/GetPage";
                            axios.get(url)
                                .then( response => {
                                    if (response.data != null) {
                                        self.setState({ redirectedPage: response.data, isLoading: false, isLoadingChildPage: false });
                                    }
                                });
                        }                        
                    }
                })
                .catch(function (error) {
                    alert(error);
                    self.setState({ isLoading: true })
                });
        });        
        return <div/>
    }
    async checkUserIfLoggedIn() {
        var authData = await AuthHelper.IsUserLoggedIn();
        url = authData.url;
        apiKey = authData.apiKey;
        if (authData.isLoggedIn)
            this.getUserInformation();
        this.setState({ loggedIn: authData.isLoggedIn, isLoading: false });
    }
    executeDeepLink(link) {
        if(link.length > 0)
            msTeams.executeDeepLink(link);
    }
    render() {
        const switchPage = () => {
            switch (this.state.redirectedPage) {
                case "/Cases/Meet": return <SearchCase executeDeepLink={this.executeDeepLink} redirectPage={this.state.redirectedPage} type={Constants.CaseType.matter} url={url} apiKey={apiKey} />;
                case "/Hearing/Meet": return <SearchCase executeDeepLink={this.executeDeepLink}  redirectPage={this.state.redirectedPage} type={Constants.CaseType.hearing} url={url} apiKey={apiKey} />;
                case "/Contracts/Meet": return <SearchCase executeDeepLink={this.executeDeepLink} redirectPage={this.state.redirectedPage} type={Constants.CaseType.contract} url={url} apiKey={apiKey} />;
                case "/Cases/Create": return <CreateChannelComponent executeDeepLink={this.executeDeepLink} redirectPage={this.state.redirectedPage} type={Constants.CaseType.matter} url={url} apiKey={apiKey} />;
                case "/Hearing/Create": return <CreateChannelComponent executeDeepLink={this.executeDeepLink} redirectPage={this.state.redirectedPage} type={Constants.CaseType.hearing} url={url} apiKey={apiKey} />;
                case "/Contracts/Create": return <CreateChannelComponent executeDeepLink={this.executeDeepLink} redirectPage={this.state.redirectedPage} type={Constants.CaseType.contract} url={url} apiKey={apiKey} />;
                case "/Setup/Index": return <SettingsComponent executeDeepLink={this.executeDeepLink} redirectPage={this.state.redirectedPage} url={url} apiKey={apiKey} />;
                case "/Setup/ChannelTab": return <ChannelTabComponent executeDeepLink={this.executeDeepLink} redirectPage={this.state.redirectedPage} url={url} apiKey={apiKey} />;
                default: return <div />;
            }
        };
        const navigatePage = () =>
        {
            return <Flex column gap='gap.large' hAlign="center" vAlign="center">
                <Flex styles={{ width: '80%' }}>
                    <Flex.Item align="center" size="size.small">
                        <Image src='Images/app4legalTitle.png' styles={Styles.topImageStyle} />
                    </Flex.Item>
                    <Flex.Item push align='center'>
                            <div
                                style={{
                                    display: 'flex',
                                    justifyContent: 'center',
                                    height: '32px',
                                    alignItems: 'center',
                                }}
                            >
                            <Divider vertical />
                            <Text content={this.state.userInfo.name} />
                            <Divider vertical />
                            <Avatar
                                image={this.state.userInfo.image}
                                status={{
                                    color: this.state.userInfo.color ,
                                    title: this.state.userInfo.status ,
                                }}
                            />
                            <Divider vertical />
                            <Button icon={<BreakoutRoomIcon />} text primary content="Sign Out" onClick={this.handleLogOut} />
                            <Divider vertical />
                        </div>
                    </Flex.Item>
                </Flex>
                <Flex vAlign='center' hAlign='center' styles={{ width: '80%' }}>
                    {this.state.isLoadingChildPage ?
                        <Loader/>:
                        (switchPage())
                    }
                </Flex>
                </Flex>
            
        };
        return (
            <Provider theme={this.state.theme} styles={{ backgroundColor: '#F3F2F1' }}>
                {
                    (this.state.isLoading) ?
                        loader :
                        (this.state.loggedIn) ? (navigatePage()) : <SignIn source={null} />                      
                }
            </Provider>
        )
    }

    private updateTheme(themeString: string | undefined): void {
        this.setState({
            theme: TeamsThemeHelper.getTheme(themeString)
        });
    }
}
var url = "";
var apiKey = "";

interface IAppProps { }

interface IAppState {
    theme: ThemePrepared;
    loggedIn: boolean;
    isLoading: boolean;
    redirectedPage: string;
    isLoadingChildPage: boolean;
    userInfo: any;
}
const loader = 
    <Flex column hAlign="center" vAlign="center" styles={Styles.parentStyle}>
        <Flex gap="gap.medium" column hAlign="center" vAlign="center" key="center" styles={Styles.layoutStyle}>
            <Loader size="largest" labelPosition="below" />
        </Flex>
    </Flex>;
ReactDOM.render(
    <App/>,
    document.getElementById('content')
);