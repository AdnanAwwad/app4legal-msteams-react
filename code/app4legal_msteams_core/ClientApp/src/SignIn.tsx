﻿import React = require('react');
import ReactDOM = require('react-dom');
import { TeamsThemes } from './helpers/Constants'
import { Button, Provider, teamsTheme, Text, Image, Layout, Flex, Segment, Header } from '@fluentui/react-northstar';
import * as msTeams from '@microsoft/teams-js';
import $ = require('jquery');
import Styles from '../styles/Styles';
import axios from 'axios';

class SignIn extends React.Component<{source: string}, {isAuthorized: boolean, isLoggedIn: boolean, isLicensed: boolean, textLabel: string}> {
    constructor(props) {
        super(props);
        this.state = { isAuthorized: false, isLoggedIn: false, isLicensed: false, textLabel: '' }
        this.handleSignInClick = this.handleSignInClick.bind(this);
    }
    componentDidMount() {
        axios.get("/login/CheckAuthorization").
            then(response => {
                if (response.data) {
                    try {
                        var result = response.data;
                        var microsoft = result.microsoft;
                        var app4Legal = result.app4legal;
                        var app4LegalLicense = result.app4legalLicense;
                        var text = 'Authorization';
                        if (!microsoft)
                            text = 'Please sign in first to App4legal and Microsoft';
                        if (microsoft && !app4Legal)
                            text = 'To start using App4Legal in Microsoft Teams.';
                        if (microsoft && app4Legal && !app4LegalLicense)
                            text = 'Subscribe to App4Legal For Teams and try again.';

                        this.setState({ isAuthorized: microsoft, isLoggedIn: app4Legal, isLicensed: app4LegalLicense, textLabel: text });
                    }
                    catch (err) {

                    }
                }
            });
    }
    handleSignInClick() {
        axios.get("/login/CheckAuthorization").
            then(response => {
                if (response.data) {
                    try {
                        var result = response.data;
                        var microsoft = result.microsoft;
                        var app4Legal = result.app4legal;
                        var app4LegalLicense = result.app4legalLicense;
                        var text = 'Authorization';
                        if (!microsoft)
                            text = 'Please sign in first to App4legal and Microsoft';
                        if (microsoft && !app4Legal)
                            text = 'To start using App4Legal in Microsoft Teams.';
                        if (microsoft && app4Legal && !app4LegalLicense)
                            text = 'Subscribe to App4Legal For Teams and try again.';

                        this.setState({ isAuthorized: microsoft, isLoggedIn: app4Legal, isLicensed: app4LegalLicense, textLabel: text });
                        var url = window.location.origin + "/login" + (this.state.isAuthorized ? "/login" : "/authorizeMicrosoft");
                        msTeams.authentication.authenticate({
                            url: url,
                            width: 600,
                            height: 550,
                            successCallback: function (result) {
                                $.ajax({
                                    type: "GET",
                                    url: "/Home/GetPage",
                                    dataType: 'text',
                                    success: function (response) {
                                        if(response.length> 0)
                                            window.location.href = response;
                                    },
                                    error: function () {
                                    }
                                });
                        },
                        failureCallback: function (reason) {
                            if (reason == "login closed") {
                                location.reload();
                            }
                            if (reason.includes("login.microsoft")) {

                            }
                        }
                    });
                }
                catch (err) {
                    alert(err);
                }
            }
        });
    }
    render() {
        return (
            <Flex column hAlign="center" vAlign="center" styles={Styles.parentStyle}>
                <Flex gap="gap.medium" column hAlign="center" vAlign="center" key="center" styles={Styles.layoutStyle}>
                    {(this.props.source != null) ? <Image src={'../' + this.props.source} /> : <Image src='Images/authorization.png' />}
                    {(!this.state.isLicensed && this.state.isLoggedIn) ? <Header as="h4" styles={{ color: "red" }} content="Not licensed, please subscribe to this application via App4Legal core." /> : <div />}
                    <Header as='h4' content={this.state.textLabel} />
                    <Button primary onClick={this.handleSignInClick}>{!this.state.isAuthorized ? "Sign In with Microsoft and App4Legal" : "Authorize in App4Legal"}</Button>
                 </Flex>
            </Flex>
            );
    }
}

export default SignIn;