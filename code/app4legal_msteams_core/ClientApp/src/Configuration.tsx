﻿import React = require('react');
import ReactDOM = require('react-dom');
import { Button, Provider, teamsTheme, Text, Image, Layout, Flex, Segment, Loader, Card, CardHeader, CardBody, Header, Input, SearchIcon, CardFooter, OpenOutsideIcon, Tooltip, Dialog, LinkIcon, ArrowRightIcon, List, MegaphoneIcon, Label, InfoIcon, ParticipantAddIcon, AttendeeIcon, TenantPersonalIcon, Checkbox, TeamsMonochromeIcon, Grid, CloseIcon, WorkOrSchoolIcon, SettingsIcon, Divider, Form, Table, gridCellMultipleFocusableBehavior, ThemePrepared } from '@fluentui/react-northstar';
import * as msTeams from '@microsoft/teams-js';
import $ = require('jquery');
import { CaseType } from './helpers/Constants';
import * as Functions from './helpers/Functions';
import Styles from '../styles/Styles';
import axios from 'axios';
import TeamsThemeHelper from './helpers/TeamsThemeHelper';
import AuthHelper from './helpers/Auth-Helper';
import SignIn from './SignIn';

class ConfigurationComponent extends React.Component<{}, { isLoading: boolean, theme: ThemePrepared, loggedIn: boolean }> {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true, theme: TeamsThemeHelper.getTheme('default'), loggedIn: false};
        msTeams.initialize();
        msTeams.registerOnThemeChangeHandler(this.updateTheme.bind(this));
        msTeams.getContext(context => { this.updateTheme(context.theme) });
    }
    componentDidMount() {
        this.checkUserIfLoggedIn();
    }
    getServerInformation() {
        var filesUrl = '/Setup/GetServerInformation';
        axios.get(filesUrl)
            .then(response => {
                if (response.data != null) {
                    console.log(response.data);
                    var info = {
                        domainUrl: response.data.domainUrl,
                        redirectUrl: response.data.redirectUrl
                    };
                    if (response.data != null) {
                        msTeams.settings.setValidityState(true);
                        msTeams.settings.registerOnSaveHandler((saveEvent) => {
                            msTeams.settings.setSettings({
                                websiteUrl: info.domainUrl,
                                contentUrl: info.redirectUrl + "Setup/ChannelTab",
                                entityId: "channelTab",
                                suggestedDisplayName: "App4Legal"
                            });
                            saveEvent.notifySuccess();
                        });
                    }
                }
            })
            .catch(error => {

            });
    }
    async checkUserIfLoggedIn() {
        var authData = await AuthHelper.IsUserLoggedIn();
        if (authData.isLoggedIn) {
            await this.getServerInformation();            
        }
        this.setState({ loggedIn: authData.isLoggedIn, isLoading: false });
    }
    private updateTheme(themeString: string | undefined): void {
        this.setState({
            theme: TeamsThemeHelper.getTheme(themeString)
        });
    }
    render() {
        return (
            <Provider theme={this.state.theme} styles={{ backgroundColor: '#F3F2F1' }}>
                {
                    (this.state.isLoading) ?
                        <Loader/> :
                        (this.state.loggedIn) ? 
                            <Flex hAlign='center' vAlign='center'>
                                <Text content='You are authenticated and you can use the App4Legal app'/>
                            </Flex>
                            : <SignIn source='Images/authorization.png'/>
                }
            </Provider>
        );
    }
}

   
   
    
   
   

interface IAppState {
    theme: ThemePrepared;
    loggedIn: boolean;
    isLoading: boolean;
    redirectedPage: string;
    isLoadingChildPage: boolean;
    userInfo: any;
}
const loader =
    <Flex column hAlign="center" vAlign="center" styles={Styles.parentStyle}>
        <Flex gap="gap.medium" column hAlign="center" vAlign="center" key="center" styles={Styles.layoutStyle}>
            <Loader size="largest" labelPosition="below" />
        </Flex>
    </Flex>;
ReactDOM.render(
    <ConfigurationComponent />,
    document.getElementById('content')
);

