﻿import React = require('react');
import ReactDOM = require('react-dom');
import { Button, Provider, teamsTheme, Text, Image, Layout, Flex, Segment, Loader, Card, CardHeader, CardBody, Header, Input, SearchIcon, CardFooter, OpenOutsideIcon, Tooltip, Dialog, LinkIcon, ArrowRightIcon, List, MegaphoneIcon } from '@fluentui/react-northstar';
import * as msTeams from '@microsoft/teams-js';
import $ = require('jquery');
import { CaseType } from './helpers/Constants'
import Styles from '../styles/Styles';
import SearchBox from './components/SearchBox'
import CustomDialog from './components/DialogGlobal'
import axios from 'axios';

var currentData;
var modelMap = new Map();
var dialogMessage = "";
var channelsListMemberOf = [];
var channelsListNotMemberOf = [];
var channelsExistContent = "";
class SearchCase extends React.Component<ISearchProps, ISearchState> {
    constructor(props) {
        super(props);
        this.state = { isLoading: false, openDialog: false, didItemSelected: false, didChannelsExist: false, selectedIndex: -1, isLoadingNext: false, loaderLabel: 'Checking status' };
        this.handleClickNext = this.handleClickNext.bind(this);
        this.handleSearchClick = this.handleSearchClick.bind(this);
        this.handleSelectItem = this.handleSelectItem.bind(this);
        this.didConfirmed = this.didConfirmed.bind(this);
        this.didCancelled = this.didCancelled.bind(this);
        this.openCaseButton = this.openCaseButton.bind(this);
        this.didSelectAnExistedChannel = this.didSelectAnExistedChannel.bind(this);
    }
    componentDidMount() {

    }
    getCaseTypeName() {
        switch (this.props.type) {
            case CaseType.contract: case CaseType.hearing: return this.props.type;
            default: return "Matter";
        }
    }    
    handleSearchClick() {
        if (currentData != null) {
            this.setState({ isLoading: true });
            var url = this.props.redirectPage;
            let config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                }
            }
            let params = new URLSearchParams();
            modelMap = new Map();
            if (currentData.content == CaseType.hearing) {
                var hearingAndcaseIds = currentData.key.split(':');
                var hearingId = hearingAndcaseIds[0];
                var caseId = hearingAndcaseIds[1];
                modelMap.set('CaseId', caseId);
                modelMap.set('hearingId', hearingId);
            }
            else {
                if (currentData.content == CaseType.contract) {
                    modelMap.set('ContractId', currentData.key);
                }
                else {
                    modelMap.set('CaseId', currentData.key);
                }
            }
            modelMap.set('CaseName', currentData.header);
            modelMap.set('CaseCategory', currentData.content);
            for (const [key, value] of modelMap.entries()) {
                params.append(key, value);
            }
            axios.post(url, params, config).then((resp) => {
                try {
                    var response = resp.data;
                    switch (response.code) {
                        case "Error":
                            {
                                dialogMessage = response.message;
                                this.setState({ openDialog: true, isLoading: false });
                            }
                            break;
                        case "CreateChannel":
                            {
                                var lastSlash = this.props.redirectPage.lastIndexOf('/');
                                var caseBase = this.props.redirectPage.substring(0, lastSlash);
                                var url = caseBase + '/Create';
                                window.location.href = url;
                            } break;
                        case "RedirectToChannel":
                            {
                                channelsListMemberOf = [];
                                channelsListNotMemberOf = [];
                                channelsExistContent = "Channels with same " + this.props.type + " id: " + ((this.props.type == CaseType.contract) ? modelMap.get('ContractId') :
                                    ((this.props.type == CaseType.hearing) ? modelMap.get('HearingId') : modelMap.get('CaseId'))) + " already exist. Choose one to redirect to it or click Next to create a new one";
                                response.message.forEach(channel => {
                                    if (channel.link.length > 0)
                                        channelsListMemberOf.push({
                                            key: channel.link,
                                            header: channel.name,
                                            content: "",
                                            media: <ArrowRightIcon />
                                        });
                                    else
                                        channelsListNotMemberOf.push({
                                            key: Math.floor(Math.random() * 1000000000),
                                            header: channel.name,
                                            content: "You are not a member in this channel",
                                        });
                                });
                                this.setState({ didChannelsExist: true, isLoading: false });
                            } break;
                        case "CreatingTeam":
                            {
                                this.setState({ loaderLabel: 'Creating team for the first time. This may take a while, please wait...', isLoading: true });
                                var lastSlash = this.props.redirectPage.lastIndexOf('/');
                                var caseBase = this.props.redirectPage.substring(0, lastSlash);
                                var url = caseBase + '/CreatingTeam';
                                axios.get(url).then((resp) => {
                                    var response = resp.data;
                                    switch (response.code) {
                                        case "Error":
                                            {
                                                dialogMessage = response.message;
                                                this.setState({ openDialog: true, isLoading: false });
                                            }
                                            break;
                                        case "CreateChannel":
                                            {
                                                url = caseBase + '/Create';
                                                window.location.href = url;
                                            } break;
                                        default:
                                            {
                                                dialogMessage = 'Something wrong!';
                                                this.setState({ openDialog: true, isLoading: false });
                                            } break;
                                    }
                                });
                            } break;
                    }
                }
                catch (err) {
                    console.log(err);
                    dialogMessage = err;
                    this.setState({ openDialog: true, isLoading: false });
                }
            }).catch(error => {
                dialogMessage = error;
                this.setState({ openDialog: true, isLoading: false });
            });
        }
    }
    handleClickNext() {
        var lastSlash = this.props.redirectPage.lastIndexOf('/');
        var caseBase = this.props.redirectPage.substring(0, lastSlash);
        var url = caseBase + '/DuplicateChannel';
        this.setState({ isLoadingNext: true });
        let config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            }
        }
        let params = new URLSearchParams();  
        for (const [key, value] of modelMap.entries()) {
            params.append(key, value);
        }
        axios.post(url, params, config)
            .then(response => {
                var resp = response.data;
                var code = resp.code;
                switch (code) {
                    case "CreateChannel": {
                        var url = caseBase + '/Create';
                        window.location.href = url;
                    } break;
                }
            });
    }
    handleSelectItem(data) {
        if (data != null) {
            if (data.value == null) {
                currentData = null;
                this.setState({ didItemSelected: false, didChannelsExist: false });
            }
            else {
                currentData = data.value;
                this.setState({ didItemSelected: true });                
            }           
        }        
    }
    didConfirmed() {
        this.setState({ openDialog: false });
    }
    didCancelled() {
        this.setState({ openDialog: false });
    }
    didSelectAnExistedChannel() {
        if (this.state.selectedIndex != -1) {
            if (channelsListMemberOf.length > 0) {
                this.props.executeDeepLink(channelsListMemberOf[this.state.selectedIndex].key)
            }
        }
    }
    openCaseButton() {
        var url = this.props.url;
        if (currentData != null) {
            switch (currentData.content) {
                case CaseType.matter: case CaseType.litigation:
                    {
                        url += '/cases/edit/' + currentData.key;
                    } break;
                case CaseType.hearing:
                    {
                        var id = currentData.key.split(':')[1];
                        url += '/cases/edit/' + id;
                    } break;
                case CaseType.contract:
                    {
                        url += '/modules/contract/contracts/view/' + currentData.key;
                    } break;
            }
            window.open(url);
        }
    }
    render() {
        const title = "Select a " + this.getCaseTypeName() + " to start a meeting";
        return (
            <Flex column hAlign="center" vAlign="center" styles={Styles.parentStyle}>
                <Flex gap="gap.medium" column hAlign="center" vAlign="center" key="center" styles={Styles.layoutStyle}>                    
                    <Layout
                        styles={{
                            width: "100%",
                        }}
                        renderMainArea={() =>
                            <Card fluid styles={{ boxShadow: "0 2px 5px 2px rgba(0,0,0,.24), 0 17px 50px 0 rgba(0,0,0,.19)" }}>
                            <CardHeader>
                                <Flex hAlign="center" vAlign="center">
                                    <Header content={title} />
                                </Flex>
                            </CardHeader>
                                <CardBody>
                                    <Flex inline hAlign="center" vAlign="center" gap="gap.medium">
                                        <Header as="h3" content={this.getCaseTypeName() + " name"} />
                                        <Flex styles={{ width: "70%" }}>
                                            <SearchBox isMultiple={false} isSearching={this.state.isLoading || this.state.isLoadingNext} onSelectItem={this.handleSelectItem} type={this.props.type} url={this.props.url} apiKey={this.props.apiKey} items={[]} />
                                        </Flex>
                                        <Tooltip content="Open in App4Legal" trigger={<Button icon={<OpenOutsideIcon />} iconOnly onClick={this.openCaseButton} disabled={!this.state.didItemSelected} />} />
                                    </Flex>
                                </CardBody>
                                <CardFooter>
                                    <Flex hAlign="center" vAlign="center" gap="gap.large">
                                        {(this.state.isLoading) ?
                                            <Loader size="medium" label={this.state.loaderLabel} labelPosition="below" />
                                            :
                                            <Button primary onClick={this.handleSearchClick} disabled={this.state.isLoadingNext || !this.state.didItemSelected}>Search</Button>
                                        }
                                    </Flex>

                                </CardFooter>
                        </Card>}
                    />
                    {this.state.didChannelsExist && this.state.didItemSelected ?
                        <Layout
                            styles={{
                                width: "100%",
                            }}
                            renderMainArea={() =>
                                <Card fluid styles={{ boxShadow: "0 2px 5px 2px rgba(0,0,0,.24), 0 17px 50px 0 rgba(0,0,0,.19)" }}>
                                    <CardHeader>
                                        <Flex hAlign="center" vAlign="center">
                                            <Flex inline gap='gap.medium'>
                                                <MegaphoneIcon />
                                                <Text content={channelsExistContent} />
                                            </Flex>
                                        </Flex>
                                    </CardHeader>
                                    <CardBody>
                                        <Flex hAlign="center" vAlign="center" gap="gap.medium">
                                            {channelsListMemberOf.length > 0 ?
                                                <Segment color='brand'
                                                    content={<List
                                                        selectable
                                                        selectedIndex={this.state.selectedIndex}
                                                        onSelectedIndexChange={(e, newProps) => {
                                                            this.setState({
                                                                selectedIndex: newProps.selectedIndex,
                                                            })
                                                            this.didSelectAnExistedChannel();
                                                        }}
                                                        items={channelsListMemberOf}
                                                    />}
                                                /> : <div />}
                                            {channelsListNotMemberOf.length > 0 ?
                                                <Segment color='orange'
                                                    content={<List
                                                        items={channelsListNotMemberOf}
                                                        styles={{ color: "red" }}
                                                    />}
                                                /> : <div />}
                                            </Flex>
                                    </CardBody>
                                    <CardFooter>                                        
                                        <Flex hAlign="center" vAlign="center" gap="gap.large">
                                            {this.state.isLoadingNext ? <Loader /> :
                                                <Button primary onClick={this.handleClickNext}>Next</Button>
                                            }
                                        </Flex>

                                    </CardFooter>
                                </Card>}
                        /> : <Text></Text>}
                </Flex>
                    <Dialog
                        open={this.state.openDialog}
                        header='Information'
                        content={dialogMessage}
                        confirmButton={null}
                        cancelButton="Cancel"
                        onConfirm={this.didConfirmed}
                        onCancel={this.didCancelled}
                    />
            </Flex>
            );
    }
}
export default SearchCase;

const alignment = {
    hAlign: "center",
    vAlign: "center",
};
interface ISearchProps {
    type: string;
    url: string;
    apiKey: string;
    redirectPage: string;
    executeDeepLink(link);
}
interface ISearchState {
    isLoading: boolean;
    loaderLabel: string;
    openDialog: boolean;
    didItemSelected: boolean;
    didChannelsExist: boolean;
    selectedIndex: number;
    isLoadingNext: boolean;
}