﻿import React = require('react');
import { Button, Flex, Provider, teamsTheme, Form, Input, List, Image, Dropdown, DropdownItem, Box, Loader, Dialog } from '@fluentui/react-northstar';

class CustomDialog extends React.Component<DialogEvents> {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <Dialog
                open={this.props.openDialog}
                confirmButton="Confirm"
                cancelButton="Cancel"
                onConfirm={this.props.didConfirmed}
                onCancel={this.props.didCanceled}
            />)
    }
}
interface DialogEvents {
    didConfirmed(dialogProps);
    didCanceled(dialogProps);
    openDialog: boolean
}
export default CustomDialog
