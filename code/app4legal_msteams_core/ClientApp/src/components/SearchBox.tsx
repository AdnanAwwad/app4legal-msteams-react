﻿import React = require('react');
import ReactDOM = require('react-dom');
import { Button, Provider, teamsTheme, Text, Image, Layout, Flex, Segment, Loader, Dropdown, Input, SearchIcon, DropdownItemProps, DropdownItem, ListItemProps, ShorthandCollection, DropdownSelectedItemProps, Tooltip, Menu, CustomerHubIcon, TeamsMonochromeIcon, TenantPersonalIcon, } from '@fluentui/react-northstar';
import * as msTeams from '@microsoft/teams-js';
import $ = require('jquery');
import { CaseType } from '../helpers/Constants'
import Styles from '../../styles/Styles';
//import { useBooleanKnob } from '@fluentui/docs-components';
import axios from 'axios';
import * as Functions from '../helpers/Functions';
import { Map } from 'typescript';
class SearchBox extends React.Component<ISearchBoxProps, ISearchBoxState> {
    constructor(props) {
        super(props);
        this.handleSearchQueryChanged = this.handleSearchQueryChanged.bind(this);
        this.handleSearchDidChanged = this.handleSearchDidChanged.bind(this);
        this.state = { items: [], apiKey: this.props.apiKey, isSearching: false };
    }    
    componentDidMount() {
        switch (this.props.type) {
            case CaseType.matter: case CaseType.litigation: fullURL = this.props.url + "/modules/api/cases/autocomplete"; break;
            case CaseType.hearing: fullURL = this.props.url + "/modules/api/hearings/list_hearings"; break;
            case CaseType.contract: fullURL = this.props.url + "/modules/api/contracts/autocomplete"; break;
            case 'Invitation': fullURL = this.props.url + "/modules/api/contacts/autocomplete"; break;
            case 'Organization': {
                axios.get('/Cases/GetOrganizationMembers')
                    .then(response => {
                        var members = response.data;
                        var items = [];
                        members.forEach(member => {
                            items.push({
                                key: member.id,
                                header: member.name,
                                content: member.mail,
                                type: 'member',
                                media: < TeamsMonochromeIcon alt='Organization member' />,
                                checked: true
                            });
                        });
                        this.setState({ items: items });
                    });
            } break;
        }
    }
    handleSearchQueryChanged(e, data) {
        const headers = {
            'x-api-key': this.state.apiKey,
            'Content-Type': 'application/x-www-form-urlencoded',
            'x-api-channel': 'microsoft-teams'
        };
        var requestData = new FormData();
        requestData.append("term", data.searchQuery);
        if (data.searchQuery.length >= 3) {
            this.setState({ items: [], isSearching: true });
            axios.post(fullURL, requestData, {
                headers: headers
                })
                .then(async (response) => {
                    var error = response.data.error;
                    if (error) {
                        if (error.code == "refresh_key_required") {
                            var responseDataObj = await Functions.refreshToken(this.props.url, this.props.apiKey);
                            console.log(responseDataObj);
                            var errorRefresh = responseDataObj.error;
                            var successRefresh = responseDataObj.success;
                            if (errorRefresh == "") {
                                var newApiKey = successRefresh.data.key;
                                this.setState({ apiKey: newApiKey });
                                let params = new URLSearchParams();
                                params.append('apiKey', newApiKey);
                                let url = "/login/RefreshApiKey";
                                let config = {
                                    headers: {
                                        'Content-Type': 'application/x-www-form-urlencoded',
                                    }
                                }
                                axios.post(url, params, config).
                                    then((result) => {
                                        if (result) {
                                            if (result.data == "failed") {

                                        }
                                        return;
                                    }
                                });
                            }
                        } else if (error.code == "unauthorized") {
                            let url = "/login/Unauthorized";
                            let from = "/";
                            switch (this.props.type) {
                                case CaseType.matter: case CaseType.litigation:
                                    from += "Cases/Meet"; break;
                                case CaseType.hearing:
                                    from += "Hearing/Meet"; break;
                                case CaseType.contract:
                                    from += "Contracts/Meet"; break;
                                
                            }
                            let config = {
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded',
                                }
                            }
                            let params = new URLSearchParams();
                            params.append('from', from);
                            axios.post(url, params, config)
                                .then((result) => {
                                    if (result.data) {
                                        let url = "/login/SignIn";
                                        window.location.href = url;
                                        return;
                                    }
                                    else {
                                        return;
                                    }
                                });
                        }
                    }
                    var content = response.data.success.data;
                    var items = [];
                    switch (this.props.type) {
                        case CaseType.matter: case CaseType.litigation:
                            {
                                content.forEach(item => {
                                    items.push(
                                        {
                                            key: String(item.id),
                                            header: String(item.caseID + ': ' + Functions.strip_html_tags(item.subject)),
                                            content: String((item.category == 'Matter') ? CaseType.matter : CaseType.litigation),
                                            image: { src: 'Images/' + item.category + '.svg' }
                                        });
                                });
                            } break;
                        case CaseType.hearing:
                            {
                                content.forEach(item => {
                                    items.push(
                                        {
                                            key: String(item.id + ':' + item.legal_case_id),
                                            header: String(Functions.strip_html_tags(item.caseSubject) + " [" + item.startDate + " " + (item.startTime).substring(0, 5) + "] " + ((item.type_name_en != null) ? item.type_name_en : "")),
                                            content: String(CaseType.hearing),
                                            image: { src: 'Images/Litigation.svg' }
                                        });
                                });
                            } break;
                        case CaseType.contract:
                            {
                                content.forEach(item => {
                                    items.push(
                                        {
                                            key: String(item.id),
                                            header: String(item.contract_id + ": " + Functions.strip_html_tags(item.name)),
                                            content: String(CaseType.contract),
                                            image: { src: 'Images/' + CaseType.contract + '.svg' }
                                        });
                                });
                            } break;
                        case 'Invitation':
                            {
                                content.forEach(item => {
                                    items.push(
                                        {
                                            key: String(item.id),
                                            header: String(item.name),
                                            type: "contact",
                                            media: <TenantPersonalIcon alt='App4Legal contact' />,
                                            checked: true
                                        });
                                });
                            } break;
                    }
                   
                    this.setState({ items: items, isSearching: false });
                })
                .catch((error) => {
                    alert(error);
                });
        }
    }
    handleSearchDidChanged(e, data) {
        this.props.onSelectItem(data);
    }
    getFieldType(type) {
        switch (type) {
            case CaseType.matter: case CaseType.litigation: return
        }
    }
    render() {
       
        const divStyle = {
            width: '100%'
        };
        return (
            <div style={divStyle}>
                    <Dropdown
                    search
                    multiple={this.props.isMultiple}
                    fluid
                    items={this.state.items}
                    loading
                    loadingMessage={this.state.isSearching ? { content: <Loader size='smallest' /> } : null}
                    placeholder="Start typing..."
                    noResultsMessage="We couldn't find any matches."
                    getA11ySelectionMessage={{
                        onAdd: item => `${item} has been selected.`,
                    }}
                    onSearchQueryChange={this.props.type == 'Organization'? null : this.handleSearchQueryChanged}
                    clearable={!this.props.isSearching}
                    toggleIndicator={null}
                    onChange={this.handleSearchDidChanged}
                />   
            </div>
        );
    }
}
export default SearchBox;
var fullURL = "";
//const[loading] = useBooleanKnob({
//    name: 'loading',
//    initialValue: true,
//});
interface ISearchBoxProps {
    type: string;
    url: string;
    apiKey: string;
    items: string[];
    onSelectItem(data);
    isSearching: boolean;
    isMultiple: boolean;
}
interface ISearchBoxState {
    items: DropdownItemProps[];
    apiKey: string;
    isSearching: boolean;
}