﻿import React = require('react');
import ReactDOM = require('react-dom');
import { Button, Provider, teamsTheme, Text, Image, Layout, Flex, Segment, Loader, Card, CardHeader, CardBody, Header, Input, SearchIcon, CardFooter, OpenOutsideIcon, Tooltip, Dialog, LinkIcon, ArrowRightIcon, List, MegaphoneIcon, Label, InfoIcon, ParticipantAddIcon, AttendeeIcon, TenantPersonalIcon, Checkbox, TeamsMonochromeIcon, Grid, CloseIcon } from '@fluentui/react-northstar';
import * as msTeams from '@microsoft/teams-js';
import $ = require('jquery');
import { CaseType } from './helpers/Constants';
import * as Functions from './helpers/Functions';
import Styles from '../styles/Styles';
import SearchBox from './components/SearchBox';
import CustomDialog from './components/DialogGlobal';
import axios from 'axios';

var dialogMessage = "";
var caseMembers = [];
var orgMembersSelection = [];
var guestsSelection = [];
var emails = [];
var noEmails = [];
var responseCode;
var channelUrl;
var yesButton = null;
var model;
class CreateChannelComponent extends React.Component<ICreateProps, ICreateState>{
    constructor(props) {
        super(props);
        this.state = { isLoading: false, isLoadingCreate: false, openDialog: false, loaderLabel: "Creating channel...", caseData: new Map(), isLoadingResult: true, items: [], memberdidExists: false, orgMemberChosen: false, guestChosen: false, anyMemberSelected: false, showBottomBanner: false, bottomBannerText: '' };
        this.handleSelectGuest = this.handleSelectGuest.bind(this);
        this.handleSelectOrgMember = this.handleSelectOrgMember.bind(this);
        this.openCaseButton = this.openCaseButton.bind(this);
        this.changedCaseAction = this.changedCaseAction.bind(this);
        this.handleMemberSelectionChanged = this.handleMemberSelectionChanged.bind(this);
        this.handleClickMeet = this.handleClickMeet.bind(this);
        this.handleClickBanner = this.handleClickBanner.bind(this);
        this.handleClickAddGuest = this.handleClickAddGuest.bind(this);
        this.handleClickAddMember = this.handleClickAddMember.bind(this);
        this.handleClickBottomBanner = this.handleClickBottomBanner.bind(this);
        this.didCancelled = this.didCancelled.bind(this);
        this.didConfirmed = this.didConfirmed.bind(this);
        this.handleErrorResponse = this.handleErrorResponse.bind(this);
    }            
    componentDidMount() {
        this.getCurrentCase();
    }
    handleClickBanner() {
        this.setState({ memberdidExists: false });
    }
    handleClickBottomBanner() {
        this.setState({ showBottomBanner: false });
    }
    async handleErrorResponse(error, callBack) {
        console.log(error);
        if (error.code == "refresh_key_required") {
            var responseDataObj = await Functions.refreshToken(this.props.url, this.props.apiKey);
            var errorRefresh = responseDataObj.error;
            var successRefresh = responseDataObj.success;
            if (errorRefresh == "") {
                var newApiKey = successRefresh.data.key;
                let params = new URLSearchParams();
                params.append('apiKey', newApiKey);
                let url = "/login/RefreshApiKey";
                let config = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                    }
                }
                axios.post(url, params, config).
                    then((result) => {
                        if (result) {
                            if (result.data == "failed") {

                            }
                            else {
                                if (callBack != null)
                                    callBack;
                            }
                            return;
                        }
                    });
            }
        } else if (error.code == "unauthorized") {
            let url = "/login/Unauthorized";
            let from = this.props.redirectPage;            
            let config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                }
            }
            let params = new URLSearchParams();
            params.append('from', from);
            axios.post(url, params, config)
                .then((result) => {
                    if (result.data) {
                        let url = "/login/SignIn";
                        window.location.href = url;
                        return;
                    }
                    else {
                        return;
                    }
                });
        }
    }
    handleMemberSelectionChanged(e, props) {
        try {
            var items = this.state.items;
            var anyMemberSelected = false;            
            for (let i = 0; i < items.length; i++) {               
                if (props.label.content.trim() == items[i].header.trim()) {
                    items[i].checked = props.checked;
                }
                if (items[i].checked) {
                    anyMemberSelected = true;
                }
            }
            this.setState({ items: items, anyMemberSelected: anyMemberSelected });
        }
        catch (err) {
            console.log(err);
        }

    }
    changedCaseAction(caseId) {
        try {
            var fullURL = "";
            switch (this.props.type) {
                case CaseType.matter: case CaseType.litigation: fullURL = this.props.url + "/modules/api/cases/edit/"; break;
                case CaseType.hearing: fullURL = this.props.url + "/modules/api/hearings/edit/"; break;
                case CaseType.contract: fullURL = this.props.url + "/modules/api/contracts/edit/"; break;
            }
            this.setState({ items: [] });
            const headers = {
                'x-api-key': this.props.apiKey,
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-api-channel': 'microsoft-teams'
            };
            axios.get(fullURL + this.state.caseData.get('CaseId'), {
                    headers: headers
                })
                    .then(async (response) => {
                        var error = response.data.error;
                        if (error) {
                            await this.handleErrorResponse(error, this.changedCaseAction(caseId));
                        }
                        console.log(response.data);
                        var content = response.data.success.data;
                        var items;
                        switch (this.props.type) {
                            case CaseType.matter: case CaseType.litigation:
                                {
                                    caseMembers = [];
                                    var caseValues = content.caseValues;
                                    var isClientContact = caseValues.clientType == "company" ? false : true;
                                    var assignedToFullName = (caseValues.assignedToFullName).trim();
                                    var referredByName = (caseValues.referredByName).trim();
                                    var requestedByName = (caseValues.requestedByName).trim();
                                    if (assignedToFullName != null && assignedToFullName.length > 0)
                                        caseMembers.push({ id: caseValues.assignedTo, name: assignedToFullName, type: (caseValues.assignedTo.length == 10) ? "user" : "contact" });
                                    if (referredByName != null && referredByName.length > 0)
                                        caseMembers.push({ id: caseValues.referredBy, name: referredByName, type: (caseValues.referredBy.length == 10) ? "user" : "contact" });
                                    if (requestedByName != null && requestedByName.length > 0)
                                        caseMembers.push({ id: caseValues.requestedBy, name: requestedByName, type: (caseValues.requestedBy.length == 10) ? "user" : "contact" });
                                    if (isClientContact) {
                                        var clientName = (caseValues.clientName).trim();
                                        if (clientName != null && clientName.length > 0)
                                            caseMembers.push({ id: caseValues.contact_company_id, name: clientName, type: (caseValues.contact_company_id.length == 10) ? "user" : "contact" });
                                    }                                    
                                    this.getRelatedContacts(this.state.caseData.get('CaseId'), this.relatedContactCallBack);
                                } break;
                            case CaseType.hearing:
                                {
                                    caseMembers = [];
                                    var users = content.hearingValues.hearingLawyersUsers;                                    
                                    for (const [key, value] of Object.entries(users)) {
                                        caseMembers.push({ id: key, name: value });
                                    }
                                    //Remove duplicates
                                    caseMembers = Array.from(new Set(caseMembers.map(a => a.name))).map(name => {
                                        return caseMembers.find(a => a.name === name)
                                    })
                                    items = this.state.items;
                                    caseMembers.forEach(item => {
                                        items.push(
                                            {
                                                key: item.id,
                                                header: item.name,
                                                type: 'user',
                                                checked: true
                                            });
                                    });
                                    this.setState({ items: items, isLoadingResult: false, anyMemberSelected: (items.length > 0) });                                    
                                } break;
                            case CaseType.contract:
                                {
                                    caseMembers = [];
                                    var contractValues = content.contract;

                                    var contributors = contractValues.contributors;
                                    var assignedToFullName = contractValues.assignee;
                                    var requestedByName = contractValues.requester;
                                    var parties = contractValues.parties;

                                    if (assignedToFullName != null && assignedToFullName.length > 0)
                                        caseMembers.push({ id: contractValues.assignee_id, name: assignedToFullName, type: "user" });

                                    if (requestedByName != null && requestedByName.length > 0)
                                        caseMembers.push({ id: contractValues.requester_id, name: requestedByName, type: "contact" });

                                    if (contributors.length > 0) {
                                        contributors.forEach(contributor => {
                                            var name = (contributor.name).trim();
                                            var id = contributor.id;
                                            if (name != null && name.length > 0)
                                                caseMembers.push({ id: id, name: name, type: "user" });
                                        });
                                    }

                                    if (parties.length > 0) {
                                        parties.forEach(party => {
                                            var name = (party.party_name).trim();
                                            var id = party.party_member_id;
                                            var partyType = party.party_member_type;
                                            if (partyType == "contact") {
                                                if (name != null && name.length > 0)
                                                    caseMembers.push({ id: id, name: name, type: "contact" });
                                            }
                                        });
                                    }
                                    //Remove duplicates
                                    caseMembers = Array.from(new Set(caseMembers.map(a => a.name))).map(name => {
                                        return caseMembers.find(a => a.name === name)
                                    })
                                    items = this.state.items;
                                    caseMembers.forEach(item => {
                                        items.push(
                                            {
                                                key: item.id,
                                                header: item.name,
                                                type: item.type,
                                                checked: true
                                            });
                                    });
                                    this.setState({ items: items, isLoadingResult: false, anyMemberSelected: (items.length > 0) }); 
                                } break;
                            case 'Invitation':
                                {
                                    content.forEach(item => {
                                        items.push(
                                            {
                                                key: String(item.id),
                                                header: String(item.name),
                                                content: ""
                                            });
                                    });
                                } break;
                        }
                    })
                    .catch((error) => {
                        alert(error);
                    });
           
                    //if (success) {
                    //    //Fill in the array with all case members and(((check if null or empty, then dismiss it)))
                       
                    //    //Related Contacts
                    //    getRelatedContacts(matterId, relatedContactCallBack);
                    //}
                    //getContactMailById(success.data.caseValues.assignedTo);
                } catch (e) {
                    console.log(e);
                }  
    }
    getRelatedContacts(matterID, callBack) {         
        var url = this.props.url + '/modules/api/cases/related_contacts/' + matterID;
        const headers = {
            'x-api-key': this.props.apiKey,
            'Content-Type': 'application/x-www-form-urlencoded',
            'x-api-channel': 'microsoft-teams'
        };
        axios.get(url, { headers: headers }).
            then(result => {
                this.relatedContactCallBack(result.data);
            });       
    }
    async relatedContactCallBack(result) {
        try {
            var success = result.success;
            var error = result.error;
            if (error) {
                await this.handleErrorResponse(error, null);
            }
            if (success) {
                var contacts = [];
                contacts = success.data.data;
                contacts.forEach(element => {
                    if (element.contactType == "contact")
                        caseMembers.push({ id: element.contactId, name: element.contactName, type: "contact" });
                });
                $("#membersList").html("");
                //Remove duplicates
                caseMembers = Array.from(new Set(caseMembers.map(a => a.name))).map(name => {
                    return caseMembers.find(a => a.name === name)
                })
                var items = this.state.items;
                caseMembers.forEach(item => {
                    items.push(
                        {
                            key: item.id,
                            header: item.name,
                            type: item.type,
                            checked: true
                        });
                });
                this.setState({ items: items, isLoadingResult: false, anyMemberSelected: (items.length > 0) });
            }
        } catch (e) {
            console.log(e);
        }
    }
    async handleClickMeet() {
        emails = [];
        noEmails = [];
        var contactsToMeet = [];
        var items = this.state.items;
        items.forEach(item => {
            if (item.checked)
                contactsToMeet.push(item);
        });
        this.setState({ isLoadingCreate: true });
        for (var i = 0; i < contactsToMeet.length; i++) {
            var contactToMeet = contactsToMeet[i];
            if (contactToMeet.type === "contact")
                await this.getContactMailById(contactToMeet.key);
            else
                if (contactToMeet.type === "member") {
                    emails.push(contactToMeet.content);
                }
                else {
                    await this.getUserMailByName(contactToMeet.header);
                }
        }
        if (noEmails.length > 0) {
            let noEmailString = noEmails.join();
            var text = '';
            if (noEmails.length == 1)
                text = noEmailString + " has not an email";
            else {
                if (noEmailString.length > 0)
                    text = noEmailString + " have not emails";
                else text = "Some members haven't email to contact";
            }
            this.setState({ showBottomBanner: true, bottomBannerText: text });
        }
        var self = this;
        setTimeout(function () {
            //Send data to the controller
            if (emails.length > 0)
            {
                let url;                
                switch (self.props.type) {
                    case CaseType.matter: case CaseType.litigation:
                        url = "Cases/MeetNow"; break;
                    case CaseType.hearing:
                        url = "Hearing/MeetNow"; break;
                    case CaseType.contract:
                        url = "Contracts/MeetNow"; break;
                    default: break;
                }
                model = { CaseID: self.state.caseData.get('CaseId'), Members: emails };
                $.post(url, model, function (resp) {
                    self.checkResponse(resp);
                });
            }
            else {
                self.setState({ showBottomBanner: true, bottomBannerText: "Please select at least one member which has an email!", isLoadingCreate: false });
            }
        }, 250);
    }
    checkResponse(resp) {
        try {
            var response = JSON.parse(resp);
            var code = response.code;
            responseCode = code;
            yesButton = null;
            switch (code) {
                case "MaxPrivateChannelLimitExceeded": {
                    yesButton = 'Yes';
                    dialogMessage = "Your team has reached the maximum number of private channels. Do you want to create another App4Legal-" + this.props.type + " team?";
                } break;
                case "ChannelExisted": {
                    dialogMessage = "Channel name already existed or recently deleted, please choose other matter or restore the deleted channel.";
                } break;
                case "Succeeded": {
                    var url = response.message.url;
                    var forbiddenMails = response.message.forbiddenMails;
                    if (forbiddenMails != null) {
                        dialogMessage = "Some members have not been added due to either you are not a team owner or the invite guest option not enabled from the organization azure portal, Not added members: " + forbiddenMails;
                        if (url != null)
                            channelUrl = url;
                    }
                    else {
                        if (url != null)
                            this.props.executeDeepLink(url);
                    }
                } break;
                case "Error": {
                    var message = response.message;
                    if (message.length > 0) {
                        if (message.includes("CreateChannel_Private"))
                            dialogMessage = "There's an existing teams which you are not a member of it. Please ask to add you";
                        else
                            dialogMessage = message;
                    }
                    else
                        dialogMessage = "Something went wrong! Please try again.";
                } break;
            }
            this.setState({
                openDialog: !(code == 'Succeeded' && forbiddenMails == null)
            });
        }
        catch (err) {
            console.log(err);
            if (resp.length != 0) {
                dialogMessage = resp;
            }
        }
    }
    async getContactMailById(contactID) {
        var fullURL = this.props.url + '/modules/api/contacts/edit/' + contactID;
        const headers = {
            'x-api-key': this.props.apiKey,
            'Content-Type': 'application/x-www-form-urlencoded',
            'x-api-channel': 'microsoft-teams'
        };
        const res = await axios.get(fullURL, {
            headers: headers
        });
        const { data } = await res;
        try {
                    var success = data.success;
                    var error = data.error;
                    if (error) {
                        this.handleErrorResponse(error, this.getContactMailById(contactID));
                    }
                    if (success) {
                        var contactValues = success.data.contactValues;
                        var email = contactValues.email;
                        if (email.length > 0 && email != null) {
                            emails.push(email);
                        }
                        else {
                            noEmails.push(contactValues.firstName + " " + contactValues.lastName);
                        }
                    }
                } catch (e) {
                    console.log(e);
                }
    }
    async getUserMailByName(userName) {
        var fullURL = this.props.url + '/modules/api/auth_users/autocomplete';
        const headers = {
            'x-api-key': this.props.apiKey,
            'Content-Type': 'application/x-www-form-urlencoded',
            'x-api-channel': 'microsoft-teams'
        };
        var requestData = new FormData();
        requestData.append("term", userName);
        const res = await axios.post(fullURL, requestData, {
            headers: headers
        });
        const { data } = await res;
                try {
                    var success = data.success;
                    var error = data.error;
                    if (error) {
                        this.handleErrorResponse(error, this.getUserMailByName(userName));
                    }
                    if (success) {
                        var user = (success.data)[0];
                        var email = user.email;
                        if (email.length > 0 && email != null) {
                            emails.push(email);
                        }
                        else {
                            noEmails.push(user.firstName + " " + user.lastName);
                        }
                    }
                } catch (e) {
                    console.log(e);
                }
    }
    handleClickAddGuest() {
        var items = this.state.items;
        var addGuests = [];
        guestsSelection.forEach(guest => {
            var didExists = false;
            items.forEach(item => {
                if (guest.header.trim() == item.header.trim())
                    didExists = true;
            });
            if (!didExists)
                addGuests.push(guest);
        });
        if (addGuests.length > 0) {
            items = items.concat(addGuests);
            this.setState({ items: items, guestChosen: false, anyMemberSelected: true });
            guestsSelection = [];
        }        
    }
    handleClickAddMember() {
        var items = this.state.items;
        var addOrgMembers = [];
        orgMembersSelection.forEach(member => {
            var didExists = false;
            items.forEach(item => {
                if (member.header.trim() == item.header.trim())
                    didExists = true;
            });
            if (!didExists)
                addOrgMembers.push(member);
        });
        if (addOrgMembers.length > 0) {
            items = items.concat(addOrgMembers);
            this.setState({ items: items, orgMemberChosen: false, anyMemberSelected: true });
            orgMembersSelection = [];
        } 
    }
    handleSelectOrgMember(data) {
        if (data != null) {
            var items = data.value;           
            this.setState({ orgMemberChosen: items.length > 0 ? true: false});           
            var caseItems = this.state.items;
            items.forEach(item => {
                caseItems.forEach(caseItem => {
                    if (caseItem.header.trim() == item.header.trim()) {
                        this.setState({ memberdidExists: true });
                    }
                });
            });
            orgMembersSelection = items;
        }        
    }
    handleSelectGuest(data) {
        if (data != null) {
            var items = data.value;
            this.setState({ guestChosen: items.length > 0 ? true : false });
            var caseItems = this.state.items;
            items.forEach(item => {
                caseItems.forEach(caseItem => {
                    if (caseItem.header.trim() == item.header.trim()) {
                        this.setState({ memberdidExists: true });
                    }
                });
            });
            guestsSelection = items;
        } 
    }
    didConfirmed() {
        this.setState({ openDialog: false });
        if (responseCode.length > 0) {
            switch (responseCode) {
                case "MaxPrivateChannelLimitExceeded": {                    
                    var url;
                    switch (this.props.type) {
                        case CaseType.matter: case CaseType.litigation:
                            url = "Cases/CreateNewTeam"; break;
                        case CaseType.hearing:
                            url = "Hearing/CreateNewTeam"; break;
                        case CaseType.contract:
                            url = "Contracts/CreateNewTeam"; break;
                        default: break;
                    }
                    var self = this;
                    $.post(url, model, function (response) {
                        self.checkResponse(response);
                    });
                    this.setState({ isLoadingCreate: true, loaderLabel: "Creating new team and adding the channel to it. This may take a while..." });
                } break;
                case "ChannelExisted": {
                } break;
                case "Succeeded": {
                } break;
                case "Error": {
                } break;
            }
        }
    }
    didCancelled() {
        this.setState({ openDialog: false });
        if (responseCode.length > 0) {
            switch (responseCode) {
                case "MaxPrivateChannelLimitExceeded": {
                } break;
                case "ChannelExisted": {
                } break;
                case "Succeeded": {
                    if (channelUrl != null)
                        this.props.executeDeepLink(channelUrl);
                } break;
                case "Error": {
                } break;
            }
        }
    }
    openCaseButton() {
        var url = this.props.url;
        var data = this.state.caseData;
        console.log(data);
        if (data.get('CaseCategory').length > 0) {
            switch (data.get('CaseCategory')) {
                case CaseType.matter: case CaseType.litigation:
                    {
                        url += '/cases/edit/' + data.get('CaseId')
                    } break;
                case CaseType.hearing:
                    {
                        url += '/cases/edit/' + data.get('CaseHearingId')
                    } break;
                case CaseType.contract:
                    {
                        url += '/modules/contract/contracts/view/' + data.get('CaseId')
                    } break;
            }
            window.open(url);
        }
    }
    async getCurrentCase() {
        var navigations = this.props.redirectPage.split('/');
        console.log(navigations);
        var url = '/' + navigations[1] + '/GetCurrentCase';
        const res = await axios.get(url);
        const { data } = await res;
        var dataMap = new Map();
        dataMap.set('CaseName', data.CaseName);
        dataMap.set('CaseCategory', data.CaseCategory);
        switch (data.CaseCategory) {
            case CaseType.litigation: case CaseType.matter:
                dataMap.set('CaseId', data.CaseId); break;
            case CaseType.hearing:
                {
                    dataMap.set('CaseId', data.HearingId); 
                    dataMap.set('CaseHearingId', data.CaseId);
                } break;
            case CaseType.contract:
                dataMap.set('CaseId', data.ContractId); break;
        }
        this.setState({ caseData: dataMap });
        this.changedCaseAction(this.state.caseData.get('CaseId'));
    }
    render() {
        const titleLabel = "Add members to the meeting";
        const inviteGuestLabel = "Invite Contacts";
        const addOrgMembersLabel = "Add Members";
        const caseMembersLabel = "Members";
        const inviteContactsTip = "Invite additional contacts as guests to the meeting."
        const addOrgMembersTip = "Add an existed member in the organization to the meeting.";
        
        return (
            <Flex column hAlign="center" vAlign="center" styles={Styles.parentStyle}>
                <Flex gap="gap.medium" column hAlign="center" vAlign="center" key="center" styles={Styles.layoutStyle}>
                    <Layout
                        styles={{
                            width: "100%",
                        }}
                        renderMainArea={() =>
                            <Card fluid styles={{ boxShadow: "0 2px 5px 2px rgba(0,0,0,.24), 0 17px 50px 0 rgba(0,0,0,.19)" }}>
                                <Card.TopControls styles={{ marginRight: '10px' }}>
                                    <Tooltip content="Open in App4Legal" trigger={<Button icon={<OpenOutsideIcon />} iconOnly onClick={this.openCaseButton} />} />
                                </Card.TopControls>
                                <CardHeader>
                                    <Flex hAlign="center" vAlign="center">
                                        <Header content={titleLabel} />
                                    </Flex>
                                </CardHeader>
                                <CardBody>
                                    <Flex column gap='gap.small'>                                        
                                        <Flex inline hAlign="center" vAlign="center" gap="gap.medium">
                                            
                                            <Flex.Item size='size.half'>
                                                <Label fluid content={this.state.caseData.get('CaseName')} image={'Images/' + Functions.getCaseDetailsImage(this.props.type) + '.png'} />
                                            </Flex.Item>
                                            
                                        </Flex>
                                        <Flex inline hAlign="center" vAlign="center" gap="gap.medium">
                                            <Flex.Item size='size.quarter'>
                                                <Flex gap="gap.small" hAlign="center" vAlign="center">
                                                    <Flex.Item grow>
                                                        <Flex>
                                                            <Flex hAlign="center" vAlign="center">
                                                                <TenantPersonalIcon />
                                                            </Flex>
                                                            <Flex.Item grow styles={{ paddingLeft: '10px' }}>
                                                                <Header as="h4" content={inviteGuestLabel} />
                                                            </Flex.Item>
                                                        </Flex>
                                                    </Flex.Item>
                                                    <Tooltip content={inviteContactsTip} trigger={<Button icon={<InfoIcon />} iconOnly />} />
                                                </Flex>
                                            </Flex.Item>
                                            <Flex.Item size='size.half'>
                                                <SearchBox isMultiple={true} isSearching={this.state.isLoading} onSelectItem={this.handleSelectGuest} type="Invitation" url={this.props.url} apiKey={this.props.apiKey} items={[]} />
                                            </Flex.Item>
                                            <Flex.Item size='size.quarter'>
                                                <Button primary content="Add Guest(s)" icon={<ParticipantAddIcon />} iconPosition='before' onClick={this.handleClickAddGuest} disabled={!this.state.guestChosen}></Button>                                                
                                            </Flex.Item>
                                        </Flex>
                                        <Flex inline hAlign="center" vAlign="center" gap="gap.medium">
                                            <Flex.Item size='size.quarter'>
                                                <Flex gap="gap.small" hAlign="center" vAlign="center">
                                                    <Flex.Item grow>
                                                        <Flex>
                                                            <Flex hAlign="center" vAlign="center">
                                                                <TeamsMonochromeIcon />
                                                            </Flex>
                                                            <Flex.Item grow styles={{ paddingLeft: '10px' }}>
                                                                <Header as="h4" content={addOrgMembersLabel} />
                                                            </Flex.Item>
                                                        </Flex>
                                                    </Flex.Item>
                                                    <Tooltip content={addOrgMembersTip} trigger={<Button icon={<InfoIcon />} iconOnly />} />
                                                </Flex>
                                            </Flex.Item>
                                            <Flex.Item size='size.half'>
                                                <SearchBox isMultiple={true} isSearching={this.state.isLoading} onSelectItem={this.handleSelectOrgMember} type="Organization" url={this.props.url} apiKey={this.props.apiKey} items={[]} />
                                            </Flex.Item>
                                            <Flex.Item size='size.quarter'>
                                                <Button primary content="Add Member(s)" icon={<ParticipantAddIcon />} iconPosition='before' onClick={this.handleClickAddMember} disabled={!this.state.orgMemberChosen} ></Button>
                                            </Flex.Item>
                                        </Flex>
                                        {this.state.memberdidExists ?
                                            <Flex hAlign="center" vAlign="center">
                                                <Segment color='red' content={<Layout start='Member already exists in the Members list' end={<Button icon={<CloseIcon />} text iconOnly title="Close" onClick={this.handleClickBanner} />} />} />
                                            </Flex> : <div />}
                                        <Flex inline hAlign="center" vAlign="center" gap="gap.medium">
                                            <Flex.Item size='size.small' align='start'>
                                                <Flex gap="gap.small" hAlign="start" vAlign="center">
                                                    <Flex>
                                                            <Flex hAlign="center" vAlign="center">
                                                                <AttendeeIcon />
                                                            </Flex>
                                                            <Flex.Item grow styles={{ paddingLeft: '10px' }}>
                                                                <Header as="h4" content={caseMembersLabel} />
                                                            </Flex.Item>
                                                    </Flex>
                                                    <Tooltip content={addOrgMembersTip} trigger={<Button icon={<InfoIcon />} iconOnly />} />
                                                </Flex>
                                            </Flex.Item>
                                            <Flex.Item size='size.half'>
                                                {this.state.isLoadingResult ? <Loader /> :
                                                    <div>
                                                        <Grid content={this.state.items.map((item, index) => {
                                                            return <Segment key={item.key} content=
                                                                {
                                                                 <Flex>
                                                                    <Checkbox label={
                                                                        { content: item.header, weight: 'semibold' }} key={item.key} onChange={this.handleMemberSelectionChanged} checked={item.checked}/>
                                                                    <Flex.Item push>
                                                                        {item.media != null ? item.media
                                                                        : <AttendeeIcon alt={this.props.type + ' member'} />
                                                                        }
                                                                        </Flex.Item>
                                                                  </Flex>
                                                                }
                                                                color="brand" styles={{ width: '200px', maxWidth: '300px' }} />
                                                        })}/>
                                                    </div>                                                            
                                                }
                                            </Flex.Item>
                                            <Flex.Item size='size.quarter'>
                                                <div/>
                                            </Flex.Item> 
                                        </Flex>
                                    </Flex>
                                </CardBody>
                                <CardFooter>
                                    <Flex column hAlign="center" vAlign="center" gap="gap.large" styles={{ marginTop: '30px' }}>
                                        <Flex>
                                        {this.state.isLoadingCreate ? <Loader labelPosition='below' label={this.state.loaderLabel} /> :
                                            <Button primary onClick={this.handleClickMeet} disabled={!this.state.anyMemberSelected}> Meet Now</Button>
                                        }
                                        </Flex>
                                        {this.state.showBottomBanner ?
                                            <Flex>
                                            <Segment color='orange'
                                                content={<Layout start={this.state.bottomBannerText}
                                                            end={<Button icon={<CloseIcon />} text iconOnly title="Close"
                                                            onClick={this.handleClickBottomBanner} />} />} />
                                            </Flex>
                                            : <div/>
                                        }
                                    </Flex>

                                </CardFooter>
                            </Card>}
                    />
             
                </Flex>
                <Dialog
                    open={this.state.openDialog}
                    header='Information'
                    content={dialogMessage}
                    confirmButton={yesButton}
                    cancelButton="Cancel"
                    onConfirm={this.didConfirmed}
                    onCancel={this.didCancelled}
                />
            </Flex>
            );
    }
}
export default CreateChannelComponent;

interface ICreateProps {
    type: string;
    url: string;
    apiKey: string;
    redirectPage: string;
    executeDeepLink(link);
}
interface ICreateState {
    isLoading: boolean;
    isLoadingCreate: boolean;
    loaderLabel: string;
    openDialog: boolean;
    caseData: Map<string, string>;
    isLoadingResult: boolean;
    items: any[];
    memberdidExists: boolean;
    orgMemberChosen: boolean;
    guestChosen: boolean;
    anyMemberSelected: boolean;
    showBottomBanner: boolean;
    bottomBannerText: string;
}