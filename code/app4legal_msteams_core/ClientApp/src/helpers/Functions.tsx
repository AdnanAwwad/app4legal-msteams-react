﻿import { CaseType } from './Constants';
import axios from 'axios';
import React =require('react');

export async function refreshToken(url, oldToken) {
    const headers = {
        'x-api-key': oldToken,
        'Content-Type': 'application/x-www-form-urlencoded',
        'x-api-channel': 'microsoft-teams'
    };
    var requestData = new FormData();
    const res = await axios.post(url + '/modules/api/users/refresh_api_key', requestData, {
        headers: headers
    });
    const { data } = await res;
    return data;
};
export function strip_html_tags(str) {
    if ((str === null) || (str === ''))
        return false;
    else
        str = str.toString();    
    return str.replace(/<[^>]*>/g, '');
};
export function getCaseDetailsImage(type) {
    switch (type) {
        case CaseType.contract: return "contractdetails";
        case CaseType.hearing: return "hearingdetails";
        case CaseType.matter: return "matterdetails";
        case CaseType.litigation: return "litigationdetails";
        default: return type;
    }
}