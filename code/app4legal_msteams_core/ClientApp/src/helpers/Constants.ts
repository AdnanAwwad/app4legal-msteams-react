﻿export const TeamsThemes = {
    contrast: "contrast",
    dark: "dark",
    default: "default"
};
export const CaseType = {
    matter: "Corporate Matter",
    litigation: "Litigation Case",
    hearing: "Hearing",
    contract: "Contract"
};