﻿import axios from "axios";
import $  = require("jquery")
export default class AuthHelper {

    public static async IsUserMicrosoftLoggedIn(): Promise<boolean> {

        var url = '/login/CheckAuthorization';
        const res = await axios.get(url);
        const { data } = await res;
        if (data != null) {
            if (data != null) {
                var isUserLoggedIn = data.microsoft;
                return isUserLoggedIn;
            }
        } 
    }
    public static async IsUserApp4LegalLoggedIn(): Promise<boolean> {

        var url = '/login/CheckAuthorization';
        const res = await axios.get(url);
        const { data } = await res;
        if (data != null) {
            if (data != null) {
                var isUserLoggedIn = data.app4legal;
                return isUserLoggedIn;
            }
        }  
    }
    public static async IsUserLoggedIn(): Promise<any> {        
        var url = '/login/CheckAuthorization';
        const res = await axios.get(url);
        const { data } = await res;
        if (data != null) {
            if (data != null) {
                var isUserAuthorized = data.microsoft;
                var isUserLoggedIn = data.app4legal;
                var userUrl = ""
                var apiKey = "";
                if (data.url != null && data.apiKey != null) {
                    userUrl = data.url.substring(0, data.url.length - 1);
                    apiKey = data.apiKey;
                }
                return {
                    isLoggedIn: (isUserAuthorized && isUserLoggedIn), url: userUrl, apiKey: apiKey
                };
            }
        }     
    }    
}
