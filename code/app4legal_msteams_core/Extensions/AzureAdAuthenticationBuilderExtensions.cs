﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using app4legal_msteams_core.Services;
using Microsoft.Identity.Client;
using Microsoft_Teams_Graph_RESTAPIs_Connect;
using NLog;
using app4legal_msteams_core.Controllers;
using Microsoft.Graph;
using System.Net.Http.Headers;
using Microsoft_Teams_Graph_RESTAPIs_Connect.Helpers;

namespace app4legal_msteams_core.Extensions
{
    public static class AzureAdAuthenticationBuilderExtensions
    {
        private static NLog.Logger logger = LogManager.GetCurrentClassLogger();

        public static AuthenticationBuilder AddAzureAd(this AuthenticationBuilder builder)
            => builder.AddAzureAd(_ => { });

        public static AuthenticationBuilder AddAzureAd(this AuthenticationBuilder builder, Action<AzureAdOptions> configureOptions)
        {
            builder.Services.Configure(configureOptions);
            builder.Services.AddSingleton<IConfigureOptions<OpenIdConnectOptions>, ConfigureAzureOptions>();
            builder.AddOpenIdConnect();
            return builder;
        }

        public class ConfigureAzureOptions : IConfigureNamedOptions<OpenIdConnectOptions>
        {
            private readonly AzureAdOptions _azureOptions;
            private readonly IGraphAuthProvider _authProvider;
            public AzureAdOptions GetAzureAdOptions() => _azureOptions;

            public ConfigureAzureOptions(IOptions<AzureAdOptions> azureOptions, IGraphAuthProvider authProvider)
            {
                _azureOptions = azureOptions.Value;
                _authProvider = authProvider;
            }

            public void Configure(string name, OpenIdConnectOptions options)
            {
                options.ClientId = _azureOptions.ClientId;
                options.Authority = $"{_authProvider.Authority}v2.0";
                options.UseTokenLifetime = true;
                options.CallbackPath = _azureOptions.CallbackPath;
                options.RequireHttpsMetadata = false;
                options.ResponseType = OpenIdConnectResponseType.CodeIdToken;
                var allScopes = $"{_azureOptions.Scopes} {_azureOptions.GraphScopes}".Split(new[] { ' ' });
                foreach (var scope in allScopes) { options.Scope.Add(scope); }

                options.TokenValidationParameters = new TokenValidationParameters
                {
                    // Ensure that User.Identity.Name is set correctly after login
                    NameClaimType = "name",

                    // Instead of using the default validation (validating against a single issuer value, as we do in line of business apps),
                    // we inject our own multitenant validation logic
                    ValidateIssuer = false,

                    // If the app is meant to be accessed by entire organizations, add your issuer validation logic here.
                    //IssuerValidator = (issuer, securityToken, validationParameters) => {
                    //    if (myIssuerValidationLogic(issuer)) return issuer;
                    //}
                };

                options.Events = new OpenIdConnectEvents
                {
                    OnTicketReceived = context =>
                    {
                        // If your authentication logic is based on users then add your logic here
                        return Task.CompletedTask;
                    },
                    OnAuthenticationFailed = context =>
                    {
                        context.Response.Redirect("/Home/Error");
                        context.HandleResponse(); // Suppress the exception
                        return Task.CompletedTask;
                    },
                    OnAuthorizationCodeReceived = async (context) =>
                    {
                        var code = context.ProtocolMessage.Code;
                        var identifier = context.Principal.FindFirst(Startup.ObjectIdentifierType)?.Value;
                        var result = await _authProvider.GetUserAccessTokenByAuthorizationCode(code);

                        // Check whether the login is from the MSA tenant. 
                        // The sample uses this attribute to disable UI buttons for unsupported operations when the user is logged in with an MSA account.
                        var currentTenantId = context.Principal.FindFirst(Startup.TenantIdType)?.Value;
                        Startup.accountIdentifier = identifier + "." + currentTenantId;
                        if (currentTenantId == "9188040d-6c67-4c5b-b112-36a304b66dad")
                        {
                            // MSA (Microsoft Account) is used to log in
                        }
                        Startup.tenantId = currentTenantId;
                        try
                        {
                            var graphClient = GraphServiceClientFactory.GetStaticAuthenticatedClient();
                            HomeController.authorized = true;
                            //Set/Get userID
                            await GraphService.getMyUserId(graphClient);

                            //Check if logged in for App4Legal
                            var credentials = await GraphService.checkIfLoggedIn(graphClient);
                            if (credentials != null)
                            {
                                HomeController.Stat = true;
                                LoginController.credentials = credentials;

                                //Check if user licensed in App4Legal
                                GraphService.checkLicenseRequest();
                            }
                            else
                            {
                                HomeController.Stat = false;
                                LoginController.credentials = null;
                            }
                        }
                        catch(Exception ex)
                        {
                            logger.Error("AuthenticationExtension - Register Client - Message: " + ex.Message);
                        }
                        
                        IConfidentialClientApplication cA = MsalAppBuilder.BuildConfidentialClientApplicationForAppLevel(currentTenantId);
                        string[] appScopes = new string[] { "https://graph.microsoft.com/.default" };
                        try
                        {
                            AuthenticationResult results = await cA.AcquireTokenForClient(appScopes).WithForceRefresh(true)
                                             .ExecuteAsync();
                            var token = results.AccessToken;
                        }
                        catch (MsalUiRequiredException ex)
                        {
                            // The application doesn't have sufficient permissions.
                            // - Did you declare enough app permissions during app creation?
                            // - Did the tenant admin grant permissions to the application?
                            logger.Error("AzureAdExtension - AuthorizationCodeReceived event - MsalUiRequiredException - Message :" + ex.Message);
                        }
                        catch (MsalServiceException ex) when (ex.Message.Contains("AADSTS70011"))
                        {
                            // Invalid scope. The scope has to be in the form "https://resourceurl/.default"
                            // Mitigation: Change the scope to be as expected.
                            logger.Error("AzureAdExtension - AuthorizationCodeReceived event - MsalServiceException - Message :" + ex.Message);
                        }
                        context.HandleCodeRedemption(result.AccessToken, result.IdToken);
                    },
                    // If your application needs to do authenticate single users, add your user validation below.
                    //OnTokenValidated = context =>
                    //{
                    //    return myUserValidationLogic(context.Ticket.Principal);
                    //}
                };
            }

            public void Configure(OpenIdConnectOptions options)
            {
                Configure(Options.DefaultName, options);
            }
        }
    }
}
