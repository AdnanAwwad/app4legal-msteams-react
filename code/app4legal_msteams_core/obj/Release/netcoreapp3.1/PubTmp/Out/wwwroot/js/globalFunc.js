var validateIntegers = function (id) {
    if ($("#" + id).length > 0) {
        $("#" + id).keydown(function (e) {
            value = $(this).val();
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                if (value.indexOf('.') !== -1) {
                    if ($.inArray(e.keyCode, [110, 190])) {
                        return false;
                    }
                }
                return;
            }
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    }
}

function currencyValidation(ctrl) {
    setInputFilter(ctrl, function (value) {
        return /^-?\d*[.,]?\d{0,2}$/.test(value);
    });
}



function setInputFilter(textbox, inputFilter) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function (event) {
        textbox.addEventListener(event, function () {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            } else {
                this.value = "";
            }
        });
    });
}



function fixTabScrolling() {
    try {
        $(document).on('focus', '.select2.select2-container', function (e) {
            var footerHeight = 120; //footerHeight = footer height + element height + buffer
            var element = $(this);
            if (element.offset().top - ($(window).scrollTop()) > ($(window).height() - footerHeight)) {
                $('html, body').animate({
                    scrollTop: element.offset().top - ($(window).height() - footerHeight)
                }, 100);
            }
        });


        $('input, select, button, a, textarea, span, ul').focus(function () {
            var footerHeight = 120; //footerHeight = footer height + element height + buffer
            var element = $(this);
            if (element.offset().top - ($(window).scrollTop()) > ($(window).height() - footerHeight)) {
                $('html, body').animate({
                    scrollTop: element.offset().top - ($(window).height() - footerHeight)
                }, 100);
            }
        });
    } catch (e) {
    }
}

        function formatOptionBySpan(state) {
            if (state.id)
                return "<span>" + state.text + "</span>";
            else
                return state.text

        };