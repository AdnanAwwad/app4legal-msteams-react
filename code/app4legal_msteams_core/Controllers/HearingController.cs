﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using app4legal_msteams_core.Models;
using app4legal_msteams_core.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Graph;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;

namespace app4legal_msteams_core.Controllers
{
    public class HearingController : Controller
    {

        private static NLog.Logger logger = LogManager.GetCurrentClassLogger();
        public static HearingMeetModel currentCase;
        private static string teamName, channelId;
        public static string teamId;
        public static List<string> teamIds;
        private static ChannelMembersCollectionPage conversationMembers;
        private static bool isForbidden = false;
        private static List<string> notAddedMembers = new List<string>();
        private ManualResetEvent _mainThread = new ManualResetEvent(false);

        private readonly IGraphServiceClientFactory _graphServiceClientFactory;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public HearingController(IGraphServiceClientFactory graphServiceClientFactory, IWebHostEnvironment webHostEnvironment)
        {
            _graphServiceClientFactory = graphServiceClientFactory;
            _webHostEnvironment = webHostEnvironment;
        }

        /// <summary>
        /// First view in case controller, aim to select a matter from database then check whether created in team or not,
        /// if yes redirect to the channel, else navigate to the next view : Create matter
        /// </summary>
        /// <returns>If user logged in, it returns the MeetIndex view, otherwise move to login page</returns>
        public IActionResult Meet()
        {
            var from = new
            {
                from = "/" + this.RouteData.Values["controller"].ToString() +
                                   "/" + this.RouteData.Values["action"].ToString()
            };
            HomeController.redirectedPage = from.from;
            return RedirectToAction("Index", "Home");
            //LoginController.redirectOrigin = from.from;
            //if (HomeController.authorized)
            //{
            //    var graphClient = _graphServiceClientFactory.GetAuthenticatedGraphClient();
            //    ViewBag.mail = await GraphService.getMyUserMail(graphClient);

            //    if (HomeController.Stat && HomeController.isLicensed)
            //    {
            //        return View("HearingIndex", LoginController.credentials);
            //    }
            //}
            //return RedirectToAction("SignIn", "Login", from);
        }
        ///// <summary>
        ///// Action performed after clicking the Go button in MatterIndex view
        ///// </summary>
        ///// <param name="model">Matter case selected from the view</param>
        ///// <returns>Status of matter</returns>
        [HttpPost]
        public async Task<ActionResult> Meet(HearingMeetModel model)
        {

            var graphClient = _graphServiceClientFactory.GetAuthenticatedGraphClient();
            var graphAppClient = _graphServiceClientFactory.GetAuthenticatedApplicationGraphClient();
            try
            {

                //Start the thread responsible for checking on all channels teams status
                //(if idle for more than two days, delete it and archive its messages as matter note)
                if (GraphService.isThreadFinished)
                {
                    var tempFilesPath = Path.Combine(_webHostEnvironment.WebRootPath, "TempFiles");
                    if (!System.IO.Directory.Exists(tempFilesPath))
                        System.IO.Directory.CreateDirectory(tempFilesPath);
                    HomeController.checkChannelsValidityThread = new Thread(() => GraphService.checkTeamsChannelsValidity(graphClient, graphAppClient, tempFilesPath));
                    HomeController.checkChannelsValidityThread.Start();
                    GraphService.isThreadFinished = false;
                }

                //Check if Team exists
                teamName = "App4Legal-" + GraphService.getCaseCategory(model.CaseCategory);
                teamIds = null;
                var teamIdsObject = await GraphService.getTeamIdByNameFromOrgExtensionsAsync(graphAppClient, GraphService.getCaseCategory(model.CaseCategory));
                teamIds = ((JArray)(teamIdsObject))?.ToObject<List<string>>();
                var resetEvents = new List<AutoResetEvent>();
                //teamId = await GraphService.getTeamIdByNameAsync(teamName);
                if (teamIds != null)
                {
                    var teamsExistsInTeams = await GraphService.checkIfTeamExistsAsync(graphClient, graphAppClient, teamIds, teamName);
                    if (teamsExistsInTeams.Count > 0)
                    {
                        teamId = teamsExistsInTeams.Last();
                        string hearingId = string.Empty;
                        Dictionary<string, object> channelsData = new Dictionary<string, object>();

                        foreach (var teamId in teamsExistsInTeams)
                        {
                            var re = new AutoResetEvent(false);
                            resetEvents.Add(re);

                            ThreadPool.QueueUserWorkItem(async w =>
                            {
                                var threadReset = w as AutoResetEvent;
                                var random = new Random();
                                try
                                {
                                    //Check if Channel exists
                                    var channels = await graphClient.Teams[teamId].Channels.Request().GetAsync();
                                    var innerResetEvents = new List<AutoResetEvent>();
                                    foreach (var channel in channels)
                                    {
                                        var re = new AutoResetEvent(false);
                                        innerResetEvents.Add(re);

                                        ThreadPool.QueueUserWorkItem(async w =>
                                        {
                                            var threadReset = w as AutoResetEvent;
                                            var random = new Random();
                                            try
                                            {
                                                if (channel.Description != null)
                                                {
                                                    try
                                                    {
                                                        //Extract the case id from description, get first number:
                                                        var numbers = Regex.Split(channel.Description, @"\D+");
                                                        foreach (var value in numbers)
                                                        {
                                                            if (value.Length > 0)
                                                            {
                                                                hearingId = value;
                                                                break;
                                                            }
                                                        }
                                                    }
                                                    catch (Exception e)
                                                    {
                                                        logger.Warn("Meet Method - Extracting Channel name from description. Message :" + e.Message);
                                                    }
                                                    if (hearingId.Equals(model.HearingId))
                                                    {
                                                        //Check if the user is a channel member

                                                        var isMember = await GraphService.checkIfUserIsAChannelMember(graphClient, graphAppClient, teamId, channel.Id);
                                                        channelsData.Add(channel.Id, new Dictionary<string, string>()
                                                        {
                                                            {channel.DisplayName, isMember? channel.WebUrl : ""}
                                                        });
                                                    }
                                                }
                                                Thread.Sleep(random.Next(100, 500));
                                            }
                                            catch (Exception ex)
                                            {
                                                logger.Error("Meet Method - Inner Thread Catch - Message :" + ex.Message);
                                                // make sure you catch exceptions and release the lock.
                                                // otherwise you will get into deadlocks
                                                threadReset.Set();
                                            }

                                            threadReset.Set();
                                        }, re);
                                    }
                                    foreach (AutoResetEvent resetEvent in innerResetEvents)
                                    {
                                        resetEvent.WaitOne();
                                    }
                                    Thread.Sleep(random.Next(100, 500));
                                }
                                catch (Exception ex)
                                {
                                    logger.Error("Meet Method - Inner Catch - Message :" + ex.Message);
                                    // make sure you catch exceptions and release the lock.
                                    // otherwise you will get into deadlocks
                                    threadReset.Set();
                                }

                                threadReset.Set();
                            }, re);
                        }
                        foreach (AutoResetEvent resetEvent in resetEvents)
                        {
                            resetEvent.WaitOne();
                        }
                        if (channelsData.Count > 0)
                        {
                            //Redirect to channel page in microsoft teams

                            var listOfLinks = new List<Dictionary<string, string>>();
                            foreach (var key in channelsData.Keys)
                            {
                                var channelData = channelsData[key] as Dictionary<string, string>;
                                listOfLinks.Add(new Dictionary<string, string>()
                                    {
                                        {"name", channelData.Keys.First() },
                                        {"link", channelData.Values.First()}
                                    });
                            }
                            Dictionary<string, object> contentDic = new Dictionary<string, object>()
                            {
                                {"code", "RedirectToChannel"},
                                {"message", listOfLinks }
                            };
                            return Content(JsonConvert.SerializeObject(contentDic));
                        }
                        else
                        {
                            //Move to the create new channel view
                            currentCase = model;
                            return Content("{\"code\":\"CreateChannel\"}");
                        }
                    }
                }
                //Team not created yet or deleted before, create new one by calling another action "CreatingTeam".
                currentCase = model;
                return Content("{\"code\":\"CreatingTeam\"}");
            }
            catch (Exception e)
            {
                if (e.Message.Contains("Forbidden"))
                {
                    //Current signed-in user is not a member of selected team, inform the user to ask the administrator adding him to the team
                    return Content("{\"code\":\"Error\", \"message\":\"You are not a member of \"" + teamName + "\" team, please ask your administrator to add you as an owner to the team.\"}");

                }
                logger.Error("Meet Method. Message :" + e.Message);
                return Content("{\"code\":\"Error\", \"message\":\"" + e.Message + "\"}");
            }
        }
        ///// <summary>
        ///// This method aim to create a new team and return the result to the client for navigating to the channel creation page
        ///// </summary>
        ///// <returns>Creation team result</returns>
        [HttpGet]
        public async Task<IActionResult> CreatingTeam()
        {
            try
            {
                var graphClient = _graphServiceClientFactory.GetAuthenticatedGraphClient((ClaimsIdentity)User.Identity);
                var graphAppClient = _graphServiceClientFactory.GetAuthenticatedApplicationGraphClient();
                var response = await GraphService.createTeamAsync(graphClient, graphAppClient, teamName, this);
                return Content(response);
            }
            catch (Exception ex)
            {
                logger.Error("CreatingTeam - Message: " + ex.Message);
            }
            return Content("{\"code\":\"Error\"}");
        }
        ///// <summary>
        ///// Create matter view which permits to add members and create the related channel for the selected matter
        ///// </summary>
        ///// <returns>MatterMeet view</returns>
        public IActionResult Create()
        {
            var from = new
            {
                from = "/" + this.RouteData.Values["controller"].ToString() +
                                    "/" + this.RouteData.Values["action"].ToString()
            };
            HomeController.redirectedPage = from.from;
            return RedirectToAction("Index", "Home");
        }
        [HttpGet]
        public IActionResult GetCurrentCase()
        {
            return Content(JsonConvert.SerializeObject(currentCase));
        }
        [HttpGet]
        public async Task<IActionResult> GetOrganizationMembers()
        {
            var graphClient = _graphServiceClientFactory.GetAuthenticatedGraphClient();
            var listOfMembers = await GraphService.GetOrganizationMembers(graphClient);
            var listToJson = JsonConvert.SerializeObject(listOfMembers);
            return Content(listToJson);
        }
        ///// <summary>
        ///// Create channel and add members to it, invite guests and redirect to the created channel.
        ///// </summary>
        ///// <param name="model">Matter case model selected</param>
        ///// <returns>Channel url for redirection</returns>
        public async Task<IActionResult> MeetNow(HearingMeetModel model, string channelName = "")
        {
            var graphClient = _graphServiceClientFactory.GetAuthenticatedGraphClient((ClaimsIdentity)User.Identity);
            var graphAppClient = _graphServiceClientFactory.GetAuthenticatedApplicationGraphClient();
            if ((channelName.Length == 0) && (currentCase?.ChannelName == null))
                channelName = await GraphService.fixChannelName(graphClient, currentCase);
            else
                channelName = currentCase?.ChannelName;
            var resetEvents = new List<AutoResetEvent>();
            var teamsIdList = await GraphService.getDataFromOrganizationExtension(graphAppClient, teamName);
            var teamsId = teamsIdList.First()[teamsIdList.First().Keys.First()] as JArray;
            int maxChannelsExceededInTeams = 0;
            Exception channelCreationException = new Exception();
            foreach (var teamId in teamsId)
            {
                //Create channel 
                var selectedMembers = await GraphService.CheckMembersAndGuestsAsync(graphClient, model.Members);

                Thread inviteMembersThread = new Thread(() => inviteMembers(selectedMembers["InviteGuestMail"] as List<object>));

                var membersToBeAdded = selectedMembers["AddMemberIdAndMail"];
                if (membersToBeAdded != null && membersToBeAdded.Count > 0)
                {
                    //Add existed users by id

                    conversationMembers = new ChannelMembersCollectionPage();
                    membersToBeAdded.ForEach(s =>
                    {
                        var convertedMember = s as Dictionary<string, string>;
                        conversationMembers.Add(new AadUserConversationMember()
                        {
                            Roles = new List<String>()
                            {

                            },
                            Email = convertedMember["mail"],
                            AdditionalData = new Dictionary<string, object>()
                            {
                            { "user@odata.bind",ClaimConstants.graphApiBaseURL + "/users('" + convertedMember["id"] +"')" },
                            { "mail", convertedMember["mail"] }
                            },
                        });
                    });
                }
                isForbidden = false;
                notAddedMembers.Clear();
                try
                {
                    Microsoft.Graph.Channel createdChannel = null;
                    bool isChannelExisted = true;
                    int attempts = 0;
                    while (isChannelExisted && attempts < 30)
                    {
                        try
                        {
                            var channel = new Microsoft.Graph.Channel
                            {
                                MembershipType = ChannelMembershipType.Private,
                                DisplayName = channelName,
                                Description = "Hearing ID: " + currentCase.HearingId + ", Case ID: " + currentCase.CaseId.ToString() + ", Channel full name: \"" + currentCase.CaseName + "\""
                            };

                            createdChannel = await graphClient.Teams[teamId.ToString()].Channels
                                                    .Request()
                                                    .AddAsync(channel);
                            isChannelExisted = false;
                        }
                        catch (Exception ex)
                        {
                            if (ex.Message.Contains("Code: Forbidden") || ex.Message.Contains("MaxPrivateChannelLimitExceeded"))
                            {
                                maxChannelsExceededInTeams++;
                                break;
                            }
                            else
                            {
                                channelCreationException = ex;
                                if (ex.Message.Contains("BadRequest") && ex.Message.Contains("CreateChannel_Private"))
                                {
                                    break;
                                }
                                attempts++;
                                channelName = await GraphService.fixChannelName(graphClient, currentCase, false, true, teamId.ToString(), attempts);
                            }
                            logger.Warn("MeetNow method - Creating Channel- Message: " + ex.Message);
                        }
                    }
                    if (createdChannel != null)
                    {
                        channelId = createdChannel.Id;
                        currentCase.CaseURL = createdChannel.WebUrl;

                        inviteMembersThread.Start();

                        //Send Channel messages
                        Thread sendMessagesThread = new Thread(() => GraphService.sendChannelMessages(graphClient, teamId.ToString(), channelId, currentCase));
                        sendMessagesThread.Start();

                        _mainThread.WaitOne();
                        foreach (ConversationMember member in conversationMembers)
                        {
                            var re = new AutoResetEvent(false);
                            resetEvents.Add(re);

                            ThreadPool.QueueUserWorkItem(async w =>
                            {
                                var threadReset = w as AutoResetEvent;
                                var random = new Random();
                                try
                                {
                                    //Add members to the channel                       
                                    await GraphService.addMemberToTheChannel(graphClient, graphAppClient, member, teamId.ToString(), channelId, (bool forbidden, string mail, Exception ex) =>
                                    {
                                        isForbidden = forbidden;
                                        notAddedMembers.Add(mail);
                                        logger.Warn("MeetNow Method - User not owner of the team - Message :" + ex.Message);

                                    });
                                    Thread.Sleep(random.Next(100, 1000));
                                }
                                catch (Exception ex)
                                {
                                    logger.Error("MeetNow Method - Message :" + ex.Message);
                                    // make sure you catch exceptions and release the lock.
                                    // otherwise you will get into deadlocks
                                    threadReset.Set();
                                }

                                threadReset.Set();
                            }, re);
                            Thread.Sleep(1500);
                        }
                        foreach (AutoResetEvent resetEvent in resetEvents)
                        {
                            resetEvent.WaitOne();
                        }
                        var response = new Dictionary<string, object>();
                        response["url"] = currentCase.CaseURL;
                        if (isForbidden)
                        {
                            response["forbiddenMails"] = notAddedMembers;
                        }
                        string serializeMessage = JsonConvert.SerializeObject(response, Formatting.Indented);

                        //Add App4Legal Tab to the created channel                    
                        Thread addTabThread = new Thread(async () => await GraphService.addTabToChannel(graphClient, teamId.ToString(), channelId));
                        addTabThread.Start();

                        return Content("{\"code\":\"Succeeded\", \"message\":" + serializeMessage + "}");
                    }
                }
                catch (Exception e)
                {
                    logger.Error("MeetNow Method - Outer Catch - Message :" + e.Message);
                    if (e.Message.Contains("MaxPrivateChannelLimitExceeded"))
                    {
                        continue;
                    }
                    if (e.Message.Contains("Channel name already existed"))
                    {
                        return Content("{\"code\":\"ChannelExisted\"}");
                    }
                    else
                    {
                        return Content("{\"code\":\"Error\", \"message\":\"" + e.Message + "\"}");
                    }
                }
            }
            if (maxChannelsExceededInTeams == teamsId.Count)
                return Content("{\"code\":\"MaxPrivateChannelLimitExceeded\"}");
            else
            {
                string serializedException = "";
                if (channelCreationException.Message.Length > 0)
                    serializedException = JsonConvert.SerializeObject(channelCreationException.Message);
                return Content("{\"code\":\"Error\", \"message\": " + serializedException + "}");
            }
        }
        ///// <summary>
        ///// This method is used then the team has exceeded its private channels limit, it aims to create another new team and callBack the MeetNow method to continue adding members and createing the channel
        ///// </summary>
        ///// <param name="model"></param>
        ///// <returns></returns>
        public async Task<IActionResult> CreateNewTeam(HearingMeetModel model)
        {
            var graphClient = _graphServiceClientFactory.GetAuthenticatedGraphClient((ClaimsIdentity)User.Identity);
            var graphAppClient = _graphServiceClientFactory.GetAuthenticatedApplicationGraphClient();
            var response = await GraphService.createTeamAsync(graphClient, graphAppClient, teamName?.ToString(), this);
            var parsedResponse = JsonConvert.DeserializeObject<dynamic>(response);
            string code = parsedResponse["code"].ToString();
            if (code.Equals("CreateChannel"))
            {
                currentCase.ChannelName = null;
                var createChannelResponse = await MeetNow(model);
                return createChannelResponse;
            }
            return Content(response);
        }
        ///// <summary>
        ///// This method is used to duplicate the existed channel(s) with incremental channel name number
        ///// </summary>
        ///// <param name="model"></param>
        ///// <returns></returns>
        public async Task<ActionResult> DuplicateChannel(HearingMeetModel model)
        {
            var graphClient = _graphServiceClientFactory.GetAuthenticatedGraphClient((ClaimsIdentity)User.Identity);
            currentCase = model;
            var channelName = await GraphService.fixChannelName(graphClient, model, false, true, teamId);
            currentCase.ChannelName = channelName;
            return Content("{\"code\":\"CreateChannel\"}");
        }
        ///// <summary>
        ///// Invite members as guests, this function is called on a seperate thread for better performance
        ///// </summary>
        ///// <param name="membersList">Members to invite</param>
        private void inviteMembers(List<object> membersList)
        {
            var graphClient = _graphServiceClientFactory.GetAuthenticatedGraphClient();
            var graphAppClient = _graphServiceClientFactory.GetAuthenticatedApplicationGraphClient();
            var resetEvents = new List<AutoResetEvent>();
            if (membersList != null && membersList.Count > 0)
            {
                //Invite user by email
                foreach (var member in membersList)
                {
                    var re = new AutoResetEvent(false);
                    resetEvents.Add(re);

                    ThreadPool.QueueUserWorkItem(async w =>
                    {
                        var threadReset = w as AutoResetEvent;
                        var random = new Random();
                        try
                        {
                            if (member != null)
                            {
                                var invitation = new Invitation()
                                {
                                    InvitedUserEmailAddress = member.ToString(),
                                    InviteRedirectUrl = currentCase.CaseURL,
                                    SendInvitationMessage = true
                                };
                                try
                                {
                                    await GraphService.inviteAndAddGuestToTheChannel(graphClient, graphAppClient, teamId, channelId, invitation, (bool forbidden, Exception e) =>
                                    {
                                        isForbidden = true;
                                        notAddedMembers.Add(member.ToString());
                                        logger.Warn("inviteMembers Method - Invite Guest to the team - Enable option from azure portal - Message :" + e.Message);
                                    });
                                }
                                catch (Exception e)
                                {
                                    logger.Error("inviteMembers Method - Message :" + e.Message);
                                }
                            }
                            Thread.Sleep(random.Next(100, 500));
                        }
                        catch (Exception ex)
                        {
                            logger.Error("inviteMembers Method - Outer Catch - Message :" + ex.Message);
                            // make sure you catch exceptions and release the lock.
                            // otherwise you will get into deadlocks
                            threadReset.Set();
                        }

                        threadReset.Set();
                    }, re);
                    Thread.Sleep(1500);
                }
                foreach (AutoResetEvent resetEvent in resetEvents)
                {
                    resetEvent.WaitOne();
                }
                _mainThread.Set();
            }
            _mainThread.Set();
        }
    }
}
