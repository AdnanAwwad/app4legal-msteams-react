﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using app4legal_msteams_core.Helpers.SetupClasses;
using app4legal_msteams_core.Models;
using app4legal_msteams_core.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Identity.Web;
using Microsoft_Teams_Graph_RESTAPIs_Connect;
using Microsoft_Teams_Graph_RESTAPIs_Connect.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;

namespace app4legal_msteams_core.Controllers
{
    public class LoginController : Controller
    {
        private static NLog.Logger logger = LogManager.GetCurrentClassLogger();

        public static string SelectedInstanceType = string.Empty;
        public Dictionary<string, string> Instances = new Dictionary<string, string>();
        public static string CloudUrl = string.Empty;
        string CloudApiValue = string.Empty;
        string valueReturnLogin = string.Empty;
        string dataToPost = string.Empty;
        static string ErroMsgForJS = string.Empty;
        public bool LoginStatus = false;
        private string GLtxturlApp4Legal = string.Empty;
        private string GLtxtEmail = string.Empty;
        private string GLtxtPwd = string.Empty;
        string GoToUrl = string.Empty;
        private static string UrlInstance;
        private Success SucessRes = null;
        private Dictionary<string, string> resultMessage = new Dictionary<string, string>();
        public static string redirectOrigin = null;
        public static CredentialsModel credentials = new CredentialsModel();
        public static string extensionID = string.Empty;
        private static readonly string key = "a5s2e8d9f5x2h5t886y5f2b5h6y1j895";
        private readonly IGraphServiceClientFactory _graphServiceClientFactory;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public LoginController(IGraphServiceClientFactory graphServiceClientFactory, IWebHostEnvironment webHostEnvironment)
        {
            _graphServiceClientFactory = graphServiceClientFactory;
            _webHostEnvironment = webHostEnvironment;
        }
        public ActionResult Login()
        {
            if (Request.Query["from"].ToString().Length > 0)
                redirectOrigin = Request.Query["from"].ToString();
            return View("App4LegalLogin");
        }
        public IActionResult SignIn(string from)
        {
            //if (from != null)
            //{
            //    redirectOrigin = from;
            //}
            //ViewBag.licensed = HomeController.isLicensed;
            //ViewBag.microsoft = HomeController.authorized;
            //ViewBag.app4legal = HomeController.Stat;
            //try
            //{
            //    var graphClient = _graphServiceClientFactory.GetAuthenticatedGraphClient();
            //    ViewBag.mail = await GraphService.getMyUserMail(graphClient);
            //}
            //catch (Exception e)
            //{
            //    ViewBag.mail = "";
            //    logger.Error("LoginController - SignIn Method - Message :" + e.Message);
            //}
            return RedirectToAction("Index","Home");
        }
        [HttpPost]
        public async Task<IActionResult> MsUserChanged(string teamsMail, string serverMail)
        {
            try
            {
                if(!teamsMail.Equals(serverMail))
                {
                    HomeController.authorized = false;
                    HomeController.Stat = false;
                    GraphService.myId = "";
                    GraphService.myMail = "";
                    var graphClient = _graphServiceClientFactory.GetAuthenticatedGraphClient();
                    var mail = await GraphService.getMyUserMail(graphClient);
                    var callbackUrl = Url.Action(nameof(SignIn), "Login", values: null, protocol: Request.Scheme);
                    //await HttpContext.SignOutAsync(
                    //    new AuthenticationProperties()
                    //    {
                    //        RedirectUri = callbackUrl
                    //    });
                }                
            }
            catch (Exception ex)
            {
                logger.Error("LoginController - MsUserChanged Method - Message :" + ex.Message);
            }
            return new EmptyResult();
        }
        public async Task<IActionResult> AuthorizationRedirect()
        {
            try
            {
                var graphAppClient = _graphServiceClientFactory.GetAuthenticatedApplicationGraphClient();
                var extensions = await GraphService.getOrgExtensions(graphAppClient);
                var url = (extensions == null) ? "https://login.microsoftonline.com/" + Startup.tenantId + "/adminconsent?client_id=" + Globals.ClientId + "&state=12345&redirect_uri=" + Globals.BaseUri + "/Login/AuthorizationRedirect" : "";
                ViewBag.url = url;
            }
            catch(Exception ex)
            {
                logger.Error("AuthorizationRedirect - Message: " + ex.Message);
            }
            return View("AuthorizationRedirect");
        }
        
        public IActionResult AuthorizeMicrosoft()
        {
            logger.Info("LoginController - AuthorizeMicrosoft Method");
            var redirectUrl = Url.Action(nameof(AuthorizationRedirect), "Login", values: null, protocol: Request.Scheme);
            return Challenge(
                new AuthenticationProperties { RedirectUri = redirectUrl },
                OpenIdConnectDefaults.AuthenticationScheme);
        }
        [HttpGet]
        public string GetCredentials()
        {
            if (LoginController.credentials != null)
                return JsonConvert.SerializeObject(LoginController.credentials);
            return "";
        }
        [HttpGet]
        public string CheckAuthorization()
        {
            var authorizationData = new Dictionary<string, object>()
            {
                {"microsoft", HomeController.authorized},
                {"app4legal", HomeController.Stat },
                {"app4legalLicense", HomeController.isLicensed }

            };
            if(LoginController.credentials != null)
            {
                authorizationData.Add("url", credentials.userUrl);
                authorizationData.Add("apiKey", credentials.apiKey);
            }
            return JsonConvert.SerializeObject(authorizationData);
        }
        [HttpPost]
        public async Task<string> SaveData(LoginModel loginData)
        {
            try
            {
                string dllServerCloud = string.Empty;
                string Url = string.Empty;
                string Username = string.Empty;
                string password = string.Empty;
                dllServerCloud = loginData.cloudOrServer;
                if (dllServerCloud.ToLower() == "cloud")
                {
                    SelectedInstanceType = "1";
                }
                else
                {
                    SelectedInstanceType = "0";
                }
                Url = loginData.url;
                Username = loginData.userName;
                password = loginData.password;
                GLtxtEmail = Username;
                GLtxtPwd = password;

                if (dllServerCloud.ToLower() == "cloud")
                {
                    await BtnSave_Cloud(Url, Username, password);
                }
                else
                {
                    UrlInstance = Url + "/";
                    await BtnSave_Server(Url, Username, password);
                }
                return JsonConvert.SerializeObject(resultMessage);
            }
            catch (Exception ex)
            {
                logger.Error("LoginController - SaveData Method - Message :" + ex.Message);
            }
            return "";
        }
        public async System.Threading.Tasks.Task BtnSave_Cloud(string urlhtml, string Emailhtml, string pswdhtml)
        {
            string url = urlhtml;
            string Email = Emailhtml;
            string pswd = pswdhtml;

            logger.Info("LoginController - BtnSave_Cloud Method - BEGIN");
            try
            {
                if ((!string.IsNullOrEmpty(Email)) || (!string.IsNullOrWhiteSpace(Email)))
                {
                    if ((!string.IsNullOrEmpty(pswd)) || (!string.IsNullOrWhiteSpace(pswd)))
                    {
                        string EmailVal = string.Empty;
                        EmailVal = Email;
                        dataToPost = "email=" + EmailVal + "&userLogin=" + EmailVal;
                        CloudApiValue = await Web.sendRequestToPostData(ClaimConstants.A4lSignInUrl, dataToPost);
                        if (CloudApiValue.Contains("error"))
                        {
                            Helpers.Classes.errors data = JsonConvert.DeserializeObject<Helpers.Classes.errors>(CloudApiValue);
                            PostError(data.error.ToString());
                        }
                        else
                        {
                            string goToURL = string.Empty;
                            var dyn = JsonConvert.DeserializeObject<dynamic>(CloudApiValue);
                            var p = dyn["instances"];
                            var jObject = (JObject)p;
                            Instances = JsonConvert.DeserializeObject<Dictionary<string, string>>(jObject.ToString());
                            goToURL = dyn["goToURL"];
                            UrlInstance = goToURL;
                            // fill list of companies or instanvces
                            if (Instances.Count == 1)
                            {
                                string ComId = string.Empty;
                                ComId = Instances.Keys.First();
                                LoginModel login = new LoginModel() { instanceId = ComId, cloudOrServer = "cloud", userName = Email, password = pswd, url = url };
                                await SignIn(login);
                            }
                            else
                            {
                                string listOfLinks = string.Empty;
                                foreach (var item in Instances)
                                {
                                    listOfLinks = listOfLinks + "<li class=\"list-group-item\"  role=\"presentation\"><a href=\"javascript:;\" id=\"" + item.Key + "\" onclick=\"loginApp4Legal(" + item.Key + ");\">" + item.Value + "</a></li>";
                                }
                                resultMessage["message"] = listOfLinks;
                            }
                        }
                    }
                    else
                    {
                        PostError("Password is required !");
                    }
                }
                else
                {
                    PostError("Email is required !");
                }
            }
            catch (System.Exception ex)
            {
                logger.Error("LoginController - BtnSave_Cloud Method - Message: " + ex.Message);
            }
            logger.Info("LoginController - BtnSave_Cloud Method - END");
        }
        public async System.Threading.Tasks.Task BtnSave_Server(string urlhtml, string Emailhtml, string pswdhtml)
        {
            string url = urlhtml;
            string Email = Emailhtml;
            string pswd = pswdhtml;
            logger.Info("LoginController - BtnSave_Server Method - BEGIN");
            try
            {
                if ((!string.IsNullOrEmpty(url)) || (!string.IsNullOrWhiteSpace(url)))
                {
                    if ((!string.IsNullOrEmpty(Email)) || (!string.IsNullOrWhiteSpace(Email)))
                    {
                        if ((!string.IsNullOrEmpty(pswd)) || (!string.IsNullOrWhiteSpace(pswd)))
                        {

                            string OfficeVersion = string.Empty;
                            string UrlApp4Legal = string.Empty;
                            UrlApp4Legal = url.Trim();
                            await Login(Email, pswd);
                        }
                        else
                        {
                            PostError("Password is required !");
                        }
                    }
                    else
                    {
                        PostError("Email is required !");
                    }
                }
                else
                {
                    await SaveCredentials(SelectedInstanceType, null, null, null);
                    WhenSuccess(null, null);
                    PostError("Url is required !");
                }
            }
            catch (System.Exception ex)
            {
                logger.Error("LoginController - BtnSave_Server Method - Message: " + ex.Message);
            }

            logger.Info("LoginController - BtnSave_Server Method - END");
        }
        [HttpPost]
        public async Task<string> SignIn(LoginModel model)
        {
            UrlInstance = UrlInstance + "/" + model.instanceId + "/";
            await BtnSave_Server(UrlInstance, model.userName, model.password);
            return JsonConvert.SerializeObject(resultMessage); ;
        }
        public async System.Threading.Tasks.Task SaveCredentials(string InstanceType, Success res = null, string UserEmail = null, string Password = null)
        {
            string UId = null;
            string Ukey = null;
            try
            {
                if (res != null)
                {
                    UId = res.data.userId;
                    Ukey = res.data.key;

                    //Save profile picture
                    new Globals(_webHostEnvironment, null).SaveProfileImage(res);

                    if (LoginController.credentials == null)
                        LoginController.credentials = new CredentialsModel();
                    LoginController.credentials.apiKey = Ukey;
                    LoginController.credentials.userUrl = UrlInstance;

                    //Save api-key in user extension
                    var graphClient = _graphServiceClientFactory.GetAuthenticatedGraphClient();
                    var userId = await GraphService.getMyUserId(graphClient);
                    var extensions = await graphClient.Users[userId].Extensions.Request().GetAsync();
                    UserExtension updateOrCreateEx = new UserExtension()
                    {
                        Id = Globals.ExtensionName,
                        AdditionalData = new Dictionary<string, object>()
                                    {
                                        { "x-api-key", Globals.EncryptString(key, LoginController.credentials.apiKey) },
                                        { "url", Globals.EncryptString(key, LoginController.credentials.userUrl) },
                                        { "@odata.type", "#microsoft.graph.openTypeExtension"}
                                    }
                    };
                    //Check if extension exists
                    var extensionID = GraphService.getExtensionId(extensions);
                    if (extensionID.Length > 0 && extensionID != null)
                    {
                        try
                        {
                            await graphClient.Users[userId].Extensions[extensionID].Request().UpdateAsync(updateOrCreateEx);
                        }
                        catch (Exception e)
                        {
                            logger.Error("LoginController - SaveCredentials Method - Check User extension - Message: " + e.Message);
                        }
                    }
                    else
                    {
                        //Create extension
                        try
                        {
                            await graphClient.Users[userId].Extensions.Request().AddAsync(updateOrCreateEx);
                        }
                        catch (Exception e)
                        {
                            logger.Error("LoginController - SaveCredentials Method - Create User extension - Message: " + e.Message);
                        }
                    }
                    WhenSuccess(UId, Ukey);
                }
            }
            catch (Exception ex)
            {
                logger.Error("LoginController - SaveCredentials Method - Message: " + ex.Message);
            }
        }
        public string Redirect()
        {
            return redirectOrigin;
        }
        private void WhenSuccess(string UId, string Ukey)
        {
            if (UId != null && Ukey != null)
            {
                HomeController.Stat = true;
            }
            else
            {
                HomeController.Stat = false;
            }

            //Check if user licensed in App4Legal
            GraphService.checkLicenseRequest();
        }
        private async System.Threading.Tasks.Task Login(string Emailhtml, string pswdhtml)
        {
            string msgSignedIn = "Successfully signed in";
            try
            {
                valueReturnLogin = string.Empty;
                string Email = string.Empty;
                string pwsd = string.Empty;
                Email = Emailhtml.Trim();
                pwsd = pswdhtml.Trim();
                if (!Email.Equals(""))
                {
                    if (!pwsd.Equals(""))
                    {
                        dataToPost = "email=" + Email + "&password=" + pwsd + "&lang=english" + "&userLogin=" + Email;
                        WorkerMethodbtnSave_Server(this, null);
                        string RespError = string.Empty;
                        var dyn = JsonConvert.DeserializeObject<dynamic>(valueReturnLogin);
                        var p = dyn["error"];
                        RespError = p.ToString();
                        if (RespError.Equals(""))
                        {
                            string result = Web.serializeJson(valueReturnLogin);
                            Success res = JsonConvert.DeserializeObject<Success>(result);
                            SucessRes = res;
                            await SaveCredentials(SelectedInstanceType, SucessRes, Email, pwsd);
                            PostError(msgSignedIn);
                        }
                        else
                        {
                            await SaveCredentials(SelectedInstanceType, null, null, null); // since nothing is send with function the all crendtails will be Deleted
                            PostError("Username or password is incorrect!");
                        }
                    }
                    else
                    {
                        PostError("The password field is empty");
                    }
                }
                else
                {
                    PostError("The login field is empty");
                }
            }
            catch (Exception ex)
            {
                PostError("Could not connect to the server please check App4Legal URL or Try again");
                logger.Error("LoginController - Login Method - Message: " + ex.Message);
            }
        }
        private async void WorkerMethodbtnSave_Server(object sender, EventArgs e)
        {
            valueReturnLogin = await Web.sendRequestToPostData(UrlInstance + @"modules/api/users/login/", dataToPost);
        }
        public void PostError(string error)
        {
            ErroMsgForJS = error;
            resultMessage["error"] = error;
        }

        [HttpPost]
        public async Task<string> RefreshApiKey(string apiKey)
        {
            var graphClient = _graphServiceClientFactory.GetAuthenticatedGraphClient();
            return await GraphService.saveUserExtension(graphClient, apiKey);
        }
        [HttpPost]
        public async Task<bool> Unauthorized(string from)
        {
            //Log out user and move to login screen
            var graphClient = _graphServiceClientFactory.GetAuthenticatedGraphClient();

            HomeController.redirectedPage = from;
            logger.Warn("LoginController - Unauthorized Method");
            return await GraphService.logOut(graphClient);
        }
        public static string getExtensionSecretKey()
        {
            return key;
        }
    }
}
