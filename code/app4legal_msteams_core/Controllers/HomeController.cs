﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using app4legal_msteams_core.Models;
using app4legal_msteams_core.Services;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using System.Threading;
using NLog;

namespace app4legal_msteams_core.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IGraphServiceClientFactory _graphServiceClientFactory;
        public static string userKey = null;
        public static bool Stat = false;
        public static string CloudApiValue = String.Empty;
        public static string tempToken;
        public static bool BlockUserAction = false;
        public static string A4MLicenseMsg = string.Empty;
        public static string A4MErrorMsg = string.Empty;
        public static bool isLicensed = false;
        public static string theme = String.Empty;
        public static bool authorized = false;
        public static Thread checkChannelsValidityThread;
        public static string redirectedPage;
        private static NLog.Logger logger = LogManager.GetCurrentClassLogger();

        public HomeController(ILogger<HomeController> logger, IGraphServiceClientFactory graphServiceClientFactory)
        {
            _logger = logger;
            _graphServiceClientFactory = graphServiceClientFactory;
        }
        [AllowAnonymous]
        public IActionResult Index()
        {
            return View();
        }
        [AllowAnonymous]
        public IActionResult Privacy()
        {
            return View();
        }
        [HttpGet]
        public string GetPage()
        {
            return HomeController.redirectedPage;
        }
        [HttpGet]
        public async Task<string> GetLoggedInMail()
        {
            try
            {
                var graphClient = _graphServiceClientFactory.GetAuthenticatedGraphClient();
                var mail = await GraphService.getMyUserMail(graphClient);                                   
                return (mail == null)? "" : mail;
            }
            catch(Exception ex)
            {
                logger.Error("GetLoggedInMail - Message: " + ex.Message);
                return "";
            }
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
