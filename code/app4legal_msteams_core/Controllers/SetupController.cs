﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using app4legal_msteams_core.Controllers;
using app4legal_msteams_core.Helpers;
using app4legal_msteams_core.Helpers.SetupClasses;
using app4legal_msteams_core.Models;
using Newtonsoft.Json;
using NLog;
using System.IO;
using System.Net;
using System.Threading;
using app4legal_msteams_core.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft_Teams_Graph_RESTAPIs_Connect.Helpers;
using Nancy.Json;

namespace app4legal_msteams_core.Controllers
{
    public class SetupController : Controller
    {
        private static NLog.Logger logger = LogManager.GetCurrentClassLogger();
        public static UserModel userModel;
        private static List<Microsoft.Graph.DriveItem> files;
        private static string caseId = "", teamId = "", teamName = "";
        private static TeamContextModel currentModel;

        private readonly IGraphServiceClientFactory _graphServiceClientFactory;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public SetupController(IGraphServiceClientFactory graphServiceClientFactory, IWebHostEnvironment webHostEnvironment)
        {
            _graphServiceClientFactory = graphServiceClientFactory;
            _webHostEnvironment = webHostEnvironment;
        }
        // GET: Setup
        public ActionResult Configure()
        {
            var from = new
            {
                from = "/" + this.RouteData.Values["controller"].ToString() +
                                   "/" + this.RouteData.Values["action"].ToString()
            };
            HomeController.redirectedPage = from.from;
            return View("Configure");
        }
        public IActionResult ChannelTab()
        {
            teamName = "";
            var graphClient = _graphServiceClientFactory.GetAuthenticatedGraphClient();
            SetupController.teamId = "";
            SetupController.caseId = "";
            var from = new
            {
                from = "/" + this.RouteData.Values["controller"].ToString() +
                                       "/" + this.RouteData.Values["action"].ToString()
            };
            //LoginController.redirectOrigin = from.from;
            //var isFromContractChannel = Request.Query["contract"].ToString();
            //if (!HomeController.authorized)
            //{
            //    return RedirectToAction("SignIn", "Login", from);
            //}

            //ViewBag.mail = await GraphService.getMyUserMail(graphClient);
            //ViewBag.isContract = (isFromContractChannel != null && isFromContractChannel.Length > 0);

            //if (HomeController.Stat && HomeController.isLicensed)
            //{
            //    return View("ChannelTab");
            //}
            //else
            //{
            //    return RedirectToAction("SignIn", "Login", from);
            //}
            HomeController.redirectedPage = from.from;
            return RedirectToAction("Index", "Home");
        }
        public IActionResult Index()
        {
            //var graphClient = _graphServiceClientFactory.GetAuthenticatedGraphClient();
            //var from = new
            //{
            //    from = "/" + this.RouteData.Values["controller"].ToString() +
            //                       "/" + this.RouteData.Values["action"].ToString()
            //};
            //LoginController.redirectOrigin = from.from;
            //if (!HomeController.authorized)
            //{
            //    return RedirectToAction("SignIn", "Login", from);
            //}
            //ViewBag.mail = await GraphService.getMyUserMail(graphClient);
            //string relativePath = "~/Images/ProfileImage." + ((Globals.imageFormat.Length > 0) ? Globals.imageFormat : "jpg");
            //if (HomeController.Stat)
            //{
            //    string userName = await GetUserDisplayName();
            //    string url = LoginController.credentials.userUrl;
            //    string organizationName = "Organization name";
            //    UserModel model = new UserModel()
            //    {
            //        displayName = userName,
            //        organizationName = organizationName,
            //        credentials = new CredentialsModel()
            //        {
            //            userUrl = url
            //        },
            //        imagePath = relativePath

            //    };
            //    return View("Index", model);
            //}
            //else
            //{
            //    return RedirectToAction("SignIn", "Login", from);
            //}
            var from = new
            {
                from = "/" + this.RouteData.Values["controller"].ToString() +
                                   "/" + this.RouteData.Values["action"].ToString()
            };
            HomeController.redirectedPage = from.from;
            return RedirectToAction("Index", "Home");
        }
        [HttpGet]
        public async Task<string> GetUserInformation()
        {
            string imagePath = "Images/ProfileImage." + ((Globals.imageFormat.Length > 0) ? Globals.imageFormat : "jpg");
            string userName = await GetUserDisplayName();
            var dic = new Dictionary<string, string>()
            {
                {"name", userName },
                {"image", imagePath }
            };
            return JsonConvert.SerializeObject(dic);
        }
        public async Task<string> RetriveCaseIdFromChannelId(TeamContextModel model)
        {
            var graphClient = _graphServiceClientFactory.GetAuthenticatedGraphClient();
            var graphAppClient = _graphServiceClientFactory.GetAuthenticatedApplicationGraphClient();
            currentModel = model;
            currentModel.teamName = "";
            try
            {
                var channelId = model.channelId;
                var teamId = "";
                int attempts = 0;
                while (teamId.Length == 0 && attempts < 5)
                {
                    var teamData = await GraphService.fetchTeamIdByChannelId(graphClient, graphAppClient, channelId);
                    teamName = teamData["name"];
                    teamId = teamData["id"];
                    if (teamId.Length == 0)
                        logger.Warn("RetriveCaseIdFromChannelId - TeamId empty for the " + (attempts + 1).ToString() + "Attempt");
                    attempts++;
                }

                if (teamId.Length > 0)
                {
                    SetupController.teamId = teamId;
                    currentModel.teamId = teamId;
                    var teams = await GraphService.getDataFromOrganizationExtension(graphAppClient);
                    bool isFound = false;
                    foreach(var team in teams)
                    {
                        foreach(KeyValuePair<string, dynamic> pair in team)
                        {
                            try
                            {
                                foreach (var id in pair.Value)
                                {
                                    if (id.ToString().Equals(teamId))
                                    {
                                        currentModel.teamName = pair.Key;
                                        isFound = true;
                                        break;
                                    }
                                }
                                if (isFound) break;
                            }
                            catch(Exception e)
                            {

                            }
                        }
                        if (isFound) break;
                    }
                    var channelName = model.channelName;
                    var channelDescriptionTask = GraphService.getChannelDescription(graphClient, teamId, channelId);
                    var channelDescription = await channelDescriptionTask;
                    if (channelDescription != null)
                    {
                        //Extract case id from channel description
                        var caseId = Globals.extractingCaseIdFromChannelDescription(channelDescription);
                        currentModel.caseId = Globals.extractingCaseIdFromChannelDescription(channelDescription, currentModel.teamName);
                        if (caseId.Length > 0)
                        {
                            SetupController.caseId = caseId;
                        }
                    }
                }
                else
                {
                    logger.Warn("RetriveCaseIdFromChannelId method - Team Id empty");
                }
            }
            catch (Exception ex)
            {
                logger.Error("RetriveCaseIdFromChannelId method - Message: " + ex.Message);
            }
            return JsonConvert.SerializeObject(currentModel);
        }
        public async Task<string> ExportChannelMessages(TeamContextModel model)
        {
            var graphClient = _graphServiceClientFactory.GetAuthenticatedGraphClient();
            var graphAppClient = _graphServiceClientFactory.GetAuthenticatedApplicationGraphClient();
            if (!model.isContract)
            {
                try
                {
                    var channelId = model.channelId;
                    var teamId = "";
                    
                    int attempts = 0;
                    while (teamId.Length == 0 && attempts < 5)
                    {
                        var teamData = await GraphService.fetchTeamIdByChannelId(graphClient, graphAppClient, channelId, true);
                        teamName = teamData["name"];
                        teamId = teamData["id"];
                        if (teamId.Length == 0)
                            logger.Warn("ExportChannelMessages - TeamId empty for the " + (attempts + 1).ToString() + "Attempt");
                        attempts++;
                    }
                    if (teamId.Length > 0)
                    {
                        if(teamId.Equals("notSupported"))
                            return "NotSupported";
                        SetupController.teamId = teamId;
                        var channelName = model.channelName;
                        var channelDescription = await GraphService.getChannelDescription(graphClient, teamId, channelId);
                        var htmlMessagesString = "";
                        if (model.isFullConversation)
                            htmlMessagesString = await GraphService.retrieveChannelMessages(graphClient, teamId, channelId, "", channelName, model.noteTitle);
                        else
                        {
                            var token = GraphService.getTokenFromChannelDescription(channelDescription);
                            var synchronizedMessages = await GraphService.retrieveChannelMessagesSynchronized(graphClient, teamId, channelId, "", channelName, model.noteTitle, token);
                            if (synchronizedMessages != null && synchronizedMessages.Count > 0)
                                htmlMessagesString = GraphService.getChannelsMessagesString(synchronizedMessages, "", channelName, model.noteTitle);
                            else
                                return "Updated";
                        }
                        if (htmlMessagesString != null)
                        {
                            var caseId = "";
                            if (SetupController.caseId.Length == 0)
                            {
                                if (channelDescription != null)
                                {
                                    //Extract case id from channel description
                                    caseId = Globals.extractingCaseIdFromChannelDescription(channelDescription);
                                    if (caseId.Length > 0)
                                        SetupController.caseId = caseId;
                                }
                            }
                            else
                                caseId = SetupController.caseId;
                            if (caseId.Length > 0)
                            {
                                GraphService.sendCaseNoteAsync(caseId, htmlMessagesString);
                                string url = ((LoginController.credentials != null) ? LoginController.credentials.userUrl : "") + @"cases/edit/" + caseId;
                                return url;
                            }
                        }
                    }
                    else
                    {
                        logger.Warn("exportChannelMessages method - Team Id empty");
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("ExportChannelMessages method - Message: " + ex.Message);
                }
                return "NotFound";
            }
            return "NotSupported";
        }

        public async Task<ActionResult> ExportChannelFiles(TeamContextModel model)
        {
            try
            {
                var filesPath = Path.Combine(_webHostEnvironment.WebRootPath, "TempFiles", "Export-Manually");
                if (!Directory.Exists(filesPath))
                    Directory.CreateDirectory(filesPath);
                string[] filesToDelete = Directory.GetFiles(filesPath);
                if (filesToDelete.Length > 0)
                {
                    try
                    {
                        for (int i = 0; i < filesToDelete.Length; i++)
                        {
                            System.IO.File.Delete(filesToDelete[i]);
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Warn("ExportChannelFiles - deleting temp files - Message: " + ex.Message);
                    }
                }
                var selectedFiles = new List<Microsoft.Graph.DriveItem>();
                if (files.Count > 0)
                {
                    selectedFiles.AddRange(files.Where(file => model.filesID.Contains(file.Id)));
                }
                if (selectedFiles.Count > 0)
                {
                    var urlTasks = selectedFiles.Select((currentFile) =>
                    {
                        WebClient webClient = new WebClient();
                        string url = currentFile.AdditionalData?.Values.First().ToString();
                        var pathName = Path.Combine( filesPath, currentFile.Name);
                        var downloadTask = webClient.DownloadFileTaskAsync(new Uri(url), pathName);
                        return downloadTask;
                    });
                    await System.Threading.Tasks.Task.WhenAll(urlTasks);
                    var uploadingFilesResponse = await uploadFilesToApp4Legal(model.isContract);
                    if (uploadingFilesResponse.Length > 0)
                        return Content(uploadingFilesResponse);
                    else
                        return Content("Error");
                }
            }
            catch (Exception ex)
            {
                logger.Error("ExportChannelFiles method - Message: " + ex.Message);
            }
            return Content("Error");
        }
        private async Task<string> uploadFilesToApp4Legal(bool isContract)
        {
            try
            {
                //Attachments to send
                var filesPath = Path.Combine(_webHostEnvironment.WebRootPath, "TempFiles", "Export-Manually");
                var filesToSend = Directory.GetFiles(filesPath);
                if (filesToSend.Length > 0)
                {
                    int attemps = 0;
                    while (caseId.Length == 0 && attemps < 5)
                    {
                        await RetriveCaseIdFromChannelId(currentModel);
                        Thread.Sleep(500);
                        attemps++;
                    }
                    if (caseId.Length == 0)
                    {
                        logger.Warn("uploadFilesToApp4Legal - CaseId empty");
                        return "";
                    }
                    else
                    {
                        var url = "";
                        var caseString = "";
                        if (isContract || teamName.Contains("Contract"))
                        {
                            url = LoginController.credentials?.userUrl + @"modules/api/documents/contract_upload_file";
                            caseString = "module_record_id=" + caseId;
                        }
                        else
                        {
                            url = LoginController.credentials?.userUrl + @"modules/api/cases/attachment_add";
                            caseString = "legal_case_id=" + caseId;
                        }
                        var response = Web.sentRequestToPostFormDataANDMultipleFiles(url, filesPath, caseString);
                        string responseString = parseError(response);
                        return responseString;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("uploadFilesToApp4Legal - Message: " + ex.Message);
            }
            return "";
        }
        private static string parseError(string response)
        {
            var responseString = "";
            try
            {
                dynamic parsedResponse = JsonConvert.DeserializeObject(response);
                string errorMsg = "";
                object errorDesc = null;
                if (parsedResponse["error"] != null)
                {
                    logger.Error("parseError - Error Api - Message: " + parsedResponse["error"]);
                    errorMsg = parsedResponse["error"]["message"];
                    if (errorMsg.Contains("Files having below type(s) are not allowed"))
                        errorDesc = parsedResponse["error"]["notAllowedExtensions"];
                }
                if (parsedResponse["success"]["msg"].ToString().Length > 0)
                    responseString = "File(s) uploaded successfully.";
                if (errorMsg.Length > 0)
                    responseString += Environment.NewLine + "Warning: " + errorMsg + " " +
                        ((errorDesc != null) ? JsonConvert.SerializeObject(errorDesc) : "");

            }
            catch (Exception ex)
            {
                responseString = "File(s) uploaded successfully.";
                logger.Error("parseError - Message: " + ex.Message);
            }
            return responseString;
        }
        [HttpPost]
        public async Task<ActionResult> GetChannelFiles(TeamContextModel model)
        {
            var graphClient = _graphServiceClientFactory.GetAuthenticatedGraphClient();
            var graphAppClient = _graphServiceClientFactory.GetAuthenticatedApplicationGraphClient();
            var listoffilesString = "";
            try
            {

                var channelId = model.channelId;
                var teamId = "";
                if (SetupController.teamId.Length == 0)
                {
                    int attempts = 0;
                    while (teamId.Length == 0 && attempts < 5)
                    {
                        var teamData = await GraphService.fetchTeamIdByChannelId(graphClient, graphAppClient, channelId);
                        teamName = teamData["name"];
                        teamId = teamData["id"];
                        if (teamId.Length == 0)
                            logger.Warn("ExportChannelMessages - TeamId empty for the " + (attempts + 1).ToString() + "Attempt");
                        else
                            SetupController.teamId = teamId;
                        attempts++;
                    }
                }
                else
                    teamId = SetupController.teamId;

                var listoffiles = await GraphService.getChannelFiles(graphClient, teamId, model.channelName);
                files = listoffiles;
                listoffilesString = JsonConvert.SerializeObject(listoffiles);
            }
            catch (Exception ex)
            {
                logger.Error("getChannelFiles - Message: " + ex.Message);
            }
            return Content(listoffilesString);
        }
        [HttpGet]
        public string GetServerInformation()
        {
            var serverInfo = new
            {
                domainUrl = Globals.DomainUri,
                redirectUrl = Globals.RedirectUri
            };
            return JsonConvert.SerializeObject(serverInfo);
        }
        [HttpPost]
        public async Task<ActionResult> UpdatePreferencesStatus(string key, string value)
        {
            var graphAppClient = _graphServiceClientFactory.GetAuthenticatedApplicationGraphClient();
            try
            {
                    switch(key)
                    {
                        case "archiveFiles":
                            {
                                var checkedBool = bool.Parse(value);
                                await GraphService.updateOrganizationApp4LegalExtension(graphAppClient, "archiveFiles", checkedBool);
                            }
                            break;
                    }
              }
            catch
            {
                return Content("{\"code\":\"Error\"}");
            }
            return Content("{\"code\":\"Success\"}");
        }
        public async Task<string> getPreferencesStatus()
        {
            var graphAppClient = _graphServiceClientFactory.GetAuthenticatedApplicationGraphClient();
            try
            {
                var isArchiveFilesCheckedList = await GraphService.getDataFromOrganizationExtension(graphAppClient, "archiveFiles");
                bool isChecked = Boolean.Parse(isArchiveFilesCheckedList[0].First().Value.ToString());
                Dictionary<string, object> preferences = new Dictionary<string, object>
                {
                    {"archiveFiles", isChecked }
                };
                return JsonConvert.SerializeObject(preferences);
            }
            catch (Exception ex)
            {
                logger.Error("getArchivedFilesStatus - Message: " + ex.Message);
                return "";
            }
        }
        [HttpGet]
        public async Task<bool> Logout()
        {
            var graphClient = _graphServiceClientFactory.GetAuthenticatedGraphClient();
            try
            {
                bool didSuccessfulyLoggedOut = await GraphService.logOut(graphClient);
                if (didSuccessfulyLoggedOut)
                {
                    var imageFormat = (Globals.imageFormat.Length > 0) ? Globals.imageFormat : "jpg";
                    var path = Path.Combine(_webHostEnvironment.WebRootPath, "Images", "ProfileImage." + imageFormat);
                    if (System.IO.File.Exists(path))
                        System.IO.File.Delete(path);
                }
                return didSuccessfulyLoggedOut;
            }
            catch (Exception ex)
            {
                logger.Error("Logout method - Message: " + ex.Message);
                return true;
            }
        }
        private async Task<string> GetUserDisplayName()
        {
            try
            {
                string jsonCheckApidata = await Web.sendRequestToGETtData(LoginController.credentials.userUrl + @"modules/api/users/check_api");
                JavaScriptSerializer js = new JavaScriptSerializer();
                errors data = js.Deserialize<errors>(jsonCheckApidata);
                if (data.error.Equals(""))
                {
                    string result = Web.serializeJson(jsonCheckApidata);
                    CheckAPISuccess res = js.Deserialize<CheckAPISuccess>(result);

                    if (res.data.username != null)
                        return res.data.profileName;
                }
            }
            catch (Exception ex)
            {
                logger.Error("GetUserDisplayName Method - Message :" + ex.Message);
            }
            return "";
        }
    }
}
